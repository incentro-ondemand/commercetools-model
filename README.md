#Commercetools Model

##Mirakl product model

```
├── [10]  Kleding
│   ├── Attributes
│   │   ├── [Merk, REQUIRED, LIST (2-Play|3Doodler|4M|80DBORIGINAL|999 games|Abbey Darts|Achoka|Acrobat|Active|adidas originals|adidas Performance|Adora|Aerobie|Aeronautica Militare|Airforce|Air Hogs|Ajax|Ambi Toys|American outfitters|America Today|AMIGO|Amleg|Amscan|Anagram|Androni|Angel Sports|Angel Toys|Angry Birds|Anker|Antony Morato|AO76|Aquabeads|Aquaplay|Armani|Armani shoes|Asmodee|Asterix & Obelix|Aurora|AutoStyle|AXI|Babeurre|Baby Alive|Baby Annabell|BABY born|Baby Dutch|Babyface|Baby & Kidz Banz|Babymel|Babyproof|Baby Toys|Bad Boys|Bakugan|Ballon|Bampidano|BanBao|Banpresto|Barbapapa|Barbara Farber|Barbie|Barts|Base Toys|Bauer|Baufix|Bayer|BBlocks|Bburago|Bebeboom|Beco|Beebielove|Be Kool|Beleduc|Bellaire|Bema|Bengh|Benson|BERG|Bergstein|Berjuan|Bess|Bestex|Bestway|Betula|BEX|Beyblade|BIColini|Big|Bigjigs|Bilibo|Billabong|Billieblush|Billybandit|BiOBUDDi|Birkenstock|Bjorn Borg|bla bla bla|Black Dragon|Blåkläder|Bloomingville|Blue Concept|Blueprint Collections|Blue Rebel|Blue System|Blutsbaby|B.Nosy|Bo-Bell|Bo Dean|Boeken|Bomba|Bonikka|Bonnie Doon|Boob Maternity|Boon|Born to be famous|Bor*z|BOSS|Boycot it|Brakkies|Brand New|Brandweerman Sam|Braqeez|Bratz|Brio|Britains|British Knights|Bruder|Brunotti|Bruynzeel|BS Toys|Bubble World|Bubblez|Bullboxer|Bull Boys|Bull´s|Bumper|Bunnies|byAstrup|Cakewalk|Calvin Klein|Caran d'Ache|Carbone|Carioca|Carpoint|Carrera|Cars Jeans|Cartamundi|Cartoon Network|Cartronic|Castorland|Catimini|Cause|Cavallaro|Chaos-and-Order|Chaya|Chicamala|Chicco|Childhome|Chillafish|Chuggington|CKS|Claesen's|Claire|Clementoni|Clic|Clicformers|Clics|Clown|Clown Games|Cobble Hill|Cobi|Collecta|Collonil|Comansi|Converse|Copa|Corolle|Cottonfield|Cougar|Craft Sensations|Crayola|Create It!|Creative|Creative Kids|Creotime|Crocodile Creek|Cruyff classics|Crystal Puzzle|Custo Growing|Czech Games Edition|Days of Wonder|DC Comics|Deltas|Denham|Denny Rose|Depesche|Desigual|Despicable Me|Develab|Dhink|Diakakis|Diamant Toys|Diamond Dotz|Dickies|DIDI|Diesel|Difuzed|Dino|Dirkje|Discraft|Disney|Disney Gold Collection|Dixon|Djeco|DKNY|Dobber|Doerak|Dolce Toys|Dolly Moda|Donic Schildkröt|Dragon Fly|Drystone|Ducati|Ducky Beau|Dummy merk|Eager Beaver|EB shoes|écoiffier|Eddie Pen|Eddy Toys|Edushape|Eeboo|Eichhorn|Eliane et Lena|Elle|Elliot|Emoji|Energie|Engelhart|Engino|Enigma|Enjoy Summer|Esprit|Est1842|Eurodisc|EverEarth|EXIT|Exploding Kittens|Fagus|Falca|FALKE|Famosa|Fashion4U|Fashion Angels|Fat Brain Toys|FC Barcelona|Feber|Fehn|Feuchtmann|Fila|Fimo|Fischertechnik|Fisher-Price|Five Nights at Freddy's|Flexo|Foam Clay|Folat|Freaks|Free and Easy|Fridolin|Fun|Funko|Gaastra|Gabbiano|Galt|Game On Sport|Gant|Gant Shoes|Garcia|Gattino|GB Eye|Gear2Play|Gearbox|Geddes & Gillmore|Geisha|Gel-A-Peel|GeoMag|Geosmart|Geoworld|Geox|Gerardo's Toys|Get & Go|Gibsons|Gigamic|Gioseppo|Giotto|Glamorous|Globber|Globos Nordic|Gobbledygook|Goki|Goliath|Gonher|Goodmark|Goula|Grafix|Gre|Green Toys|Gsus|Guess|Günther|Haba|Hanssop|Hape|Happy2play|Happy Baby|Happy Horse|Happy People|Harrows|Harry Potter|Hasbro|Hatley|Havaianas|Haza Original|Heelarious|Heimess|Heless|Hello Kitty|Helly Hansen|Hip|Holster Australia|Holztiger|Hoooked|Hörby Bruk|HORKA|Hot Wheels|Hound|House of Kids|HQ Kites|Hubelino|Hummel|Hupsakee|Huzzle|Icepeak|Iconx|iDance|Identity Games|iDO|Iello|Igor|IKKS|Illooms|IMC|Imps & Elfs|I'm Toy|Indian Blue Jeans|Infinity Nado|Injusa|Interline|Interstat|Intex|Invento|Ipanema|Italtrike|iWallz|Jack & Jones|Jackpot|Jake Fischer|Janod|Jansen Tilanus|JD Bug|Jeep|Jellycat|Jemini|Jill|Jochie|Joe Color|Joe Color|Johntoy|Joker Entertainment|Jonotoys|Jottum|Jouéco|Joy Toy|Jumbo|Jumini|Jumpking|Jurassic World|Just Beach|Just Games|Kamparo|Kanjers|Kapla|Kaporal|Karl Lagerfeld|Käthe Kruse|KBT|Kickers|Kidboxer|Kiddieland|Kiddo|Kidea|Kidid|Kid O|Kidorable|Kids At Work|Kidscase|Kidzface|Kids Fun|Kids Globe|KIDS ONLY|Kidzart|Kidzroom|Kidzzbelts|Kinderfeets|King|Kipling|kleertjes.com|Klein|K'NEX|Koeka|Koko Noko|KREUL|Krunk denim|Lacoste|La Franc|Lalaloopsy|LaQ|LCEE|Lckr|Learning Resources|Le Big|Le Chic|Le Chic shoes|Le Coq Sportif|LEGO|Lelli Kelly|Lelly|Lena|Let's Play|Levi's|LG-Imports|Libellud|Lief|Lifetime|Lifetime Games|Light Stax|Like Flo|Lili Gaufrette|Lilliputiens|Li'l Woodzeez|Little Buddy|Little Company|Little Dutch|Little Eleven Paris|Little Feet|Little Live Pets|Little Tikes|LMTD|LOL Surprise|Loom Twister|Looney Tunes|Love2wait|Lovely Lea|Ludonaute|Luminou|Lumo Stars|Luna|Lyle & Scott|Lyra|Madd Gear|Magformers|Magic the Gathering|Maileg|Maisto|Maliblue|Malinos|Mama Licious|Mamamemo|Marionette|Marlin|Maruti|Marvel|Marybel|Matchbox|Mattel|Mayoral|McGregor|Meccano|Megableu|Mega Bloks|Meiya & Alvin|Melissa & Doug|Me & My Monkey|Mercedes-Benz|Metal Earth|Me To You|Mexx|MGA|Mignolo|Mijn|Milky Kiss|Milly Mally|Mim-Pi|Mim-pi Shoes|Minecraft|Miniland|Minimize|Mini Mommy|Minions|Ministeck|Miraculous|Miss Grant|Miss Sixty|Mistral|Molo|Monchhichi|Mondo|Monster High|Moodstreet|Moon Glow|Moose Toys|Moschino|Moses|Move|Muchachomalo|Mudpuppy|Multiprint|Munich|Murphy & Nye|My Brand|My Little Baby|My Little Pony|Nail-A-Peel|Name It|Nanostad|Napapijri|Naturino|Navir|Nenuco|NERF|New Balance|New Classic Toys|New Port|nic|Nickelodeon|Nicotoy|Nijdam|Nijntje|Nike|Nikko|Ninco|Ninja|Nintendo|Nobell|Noble Collection|No Excess|Non-License|Nono|NOP|Noppies|Noppies Maternity|North Sails|No Tomatoes|Nova Carta|Num Noms|Merkloos|O'Chill|Oilily|Olang|O'Neill|Orange stars|Osprey|Ostheimer|Our Generation|Outdoor|Outdoor Play|Outfitters Nation|Out of the Blue|Oxxy|Pako|Paladone|Pampers|Panini|Pantofola d'Oro|Paradiso Toys|Parajumpers|Party Stars|Party Time|Patrizia Pepe|Paul Frank|Peaceable Kingdom|Pébéo|Pegaso|Pentel|Pepe Jeans|Peppa Pig|Pequetren|Perfect Petzzz|Peri Ponchos|Petit Bateau|Petit Collage|Petite Jolie|Petit Louie|Petit Sofie Schnoor|Petrol|Pexkids|Philos|Phoenix|Pick & Pack|Pinocchio|Pintoy|Pinypon|PIXIE CREW|Plan B Games|Play-Doh|Playforever|Playfun|Play & go|Playlife|PlayMais|PLAYMOBIL|Playshoes|Playskool|Playtime|Playwood|Please|Plenty Gifts|Plum|Plus-Plus|PMS|Poederbaas|Pointer|Pokémon|Polesie|Polly Pocket|Polo Ralph Lauren|Polo Ralph Lauren shoes|PolyM|Power Rangers|Powerslide|Prism|Procos|Professor Puzzle|Project Mc2|Proline|ProPlus|Protest|PSV|Puma|Pusheen|Pustefix|QPlay|Queen mum|Quercetti|Quick|Quiksilver|Quut|R95th|Rags Industry|Raizzed|Rampage|Rastar|Ravensburger|Ravensburger|Razor|Ready2Robot|Recent Toys|Red Code|Red Rag|Reebok|Reef|Relaunch|Replay&Sons|Repos Production|Retour Jeans|Retour Shoes|Retro Roller|Revell|Rhombus|Ria Menorca|RIO Roller|River Woods|Roces|Rollplay|Rolly Toys|Rondinella|Rookie|Rookies|Room Seven|Rory's Story Cubes|Rose & Romeo|Rough Shoes|Roxy|Rubbabu|Rubber Duck|Rubo Toys|Ruby Brown|Rucanor|Rumbl|RunRunToys|Russell Athletic|Salty Dog|Sambro|Sanetta|Sanrio|Schjerning|Schleich|Schmidt|Schylling|Science 4 You|Street Called Madison|Scotch & Soda|Scratch|Screechers Wild|SES|Sesamstraat|Sevenoneseven|Sevi|SFR|Shiwi|Shoe Republic|Shoesme|Sigikid|Siku|Silk Clay|Silverlit|Silvian Heach|Simply for Kids|Sinner|Skechers|Skooter|Slackers|Slamm|Slammer|Sluban|Smartmax|SmarTrike|Snapper Rock|SOAK|Soccer Jeans Company|S.Oliver|Soy Luna|Space Cowboys|Space Scooter|Speedo|Spiderkites|Spider-Man|Spielstabil|Spin Master|SportX|Stabilo|Stamp|State Of Art Rookies|Steiff|Step2|Stephen Joseph Gifts|Stiga|Stone Island|Storksak|StreetSurfing|Studio 100|Studio Maison|Style Me Up!|Stylex|Suki|Summer fun|Summerplay|Summertime|Sunkid|Sunny|Superga|Superman|Supermom|Superrebel|Supertrash|Super Wings|Swim Essentials|SwimWays|Sylvanian Families|Tactic|Taille 0|Tanner|Tatiri|Teletubbies|Tempish|Ten Cate|Tender Toys|Test merk - t.b.v prod|Teva|The Dutch Design Bakery|The Future Is Ours|The Offbits|The Secret Life of Pets|The White Brand|ThinkFun|Thomas de Trein|Tiamo|Tidlo|Tiger Tribe|Tikiri|Timberland|Timberland Shoes|TOM|Tomina|Tommy Hilfiger|Tommy Hilfiger shoes|Tomy|Toppoint|Topwrite Kids|Totum|Toyrific|Toys Pure|Toystate|Traditional Garden Games|Transformers|Travis Design|Trefl|Tretorn|Trudi|Tumble 'N Dry|Tutti Frutti|Twickto|Twinlife|Twizz|Ty|TYGO & vito|Ubisoft|UGG|Unico|Unicorn|Unique|Unisa|Universal|University Games|Van Dijk Toys|Vans|Varta|V-Cube|VDM|Vilebrequin|Vingino|Vingino Eyewear|Vingino shoes|Volare|VTech|Wader|Waimea|Warmbat|Warmies|Waterworld|Waterzone|Wave Racers|Waytoplay|WE Fashion|Weible Knet|Welliebellies|Welly|White Goblin Games|Wicke|Wicked|Wiking|Wild|Wildfish|Wild Republic|Wild Shoes|Winning Moves|Wizards of the Coast|Wobbel|Woezel & Pip|Wooden City|Woolrich|Worlds Apart|Wrebbit|WWF|XIN YU Toys|Xootz|Xplorys|XQ Max|XS Feet|Yello|Yellow Cab|Yep Yep|Yookidoo|Zapf Creation|ZEBRA|Zecchino D oro|ZEE & ZO|Zero2Three|Z-Man Games|Zoomer|Zootjes|Zuru)]  Merk
│   │   ├── [Variant groepnummer, REQUIRED, TEXT]  Variant groepnummer
│   │   ├── [Materialen, OPTIONAL, LIST_MULTIPLE_VALUES (acryl|alpaca|anders|angora|bamboe|chlorofibre|cupro|dons|elastan|hennep|inox|kasjmier|katoen|lamswol|leer|linnen|lurex|lycra|lyocell|merino|mesh|metaal|modal|mohair|nubuck|nylon|papier|polyamide|polyester|polypropyleen|polyurethaan|polyvinylchloride|ramie|rayon|resin|rubber|stro|suède|tactel|viscose|wax|wol|zijde|bont)]  Materialen
│   │   ├── [Basiskleur, REQUIRED, LIST (Blauw|Groen|Rood|Zwart|Wit|Grijs|Roze|Offwhite|Oranje|Zilver|Bruin|Beige|Paars|Geel|Goud|Turquoise|Multicolor)]  Basiskleur
│   │   ├── [Geslacht, REQUIRED, LIST (Jongen|Meisje|Unisex|Baby)]  Geslacht
│   │   ├── [Leverancierskleur, REQUIRED, TEXT]  Leverancierskleur
│   │   ├── [Collectie, RECOMMENDED, LIST (2013|2014|2015|2016|2017|2018|2019|2020|basics|1999|2021)]  Collectie
│   │   ├── [Seizoen, REQUIRED, LIST (zomer|winter|all season|herfst)]  Seizoen
│   │   ├── [Materiaalomschrijving, RECOMMENDED, TEXT]  Materiaalomschrijving
│   │   ├── [Test attribuut Harmen Fashion, OPTIONAL, TEXT]  Test attribuut Harmen Fashion
│   │   ├── [Test attribuut twee Harmen Fashion, OPTIONAL, TEXT]  Test attribuut twee Harmen Fashion
│   │   ├── [Sport, OPTIONAL, LIST_MULTIPLE_VALUES (Voetbal|Tennis|Hockey|Skieën|Darts|Outdoor|Running|Fitness|Basketbal)]  Sport
│   │   └── [Duurzaamheid, OPTIONAL, LIST (Global Organic Textile Standard 2|Organic Content Standard|Responsible Wool Standard2|Leather Working Group2|Forest Stewardship Council2|Fair Trade cotton2|Better Cotton Initiative2|OEKO-TEX Standard 100 2|Gemaakt van gerecycled materiaal2|Gemaakt van biologisch katoen2|Waarde bij de code2)]  Duurzaamheid
│   └── Children
│       └── [100]  Kinderen
│           ├── Attributes
│           │   ├── [Maat (Kinderkleding), REQUIRED, LIST (44|50|56|62|68|74|80|86|92|98|104|110|116|122|128|134|140|146|152|158|164|170|176|182|188|38-44|50-56|62-68|74-80|86-92|98-104|110-116|122-128|134-140|146-152|158-164|170-176|182-188|44-50|56-62|68-74|80-86|92-98|104-110|116-122|128-134|140-146|152-158|164-170|176-182|200)]  Maat (Kinderkleding)
│           │   └── [Leveranciersmaat, RECOMMENDED, TEXT]  Leveranciersmaat
│           └── Children
│               ├── [1000]  Tops
│               │   ├── Attributes
│               │   │   └── [Mouwlengte, RECOMMENDED, LIST (lange mouw|korte mouw|mouwloos|3/4 mouw|7)]  Mouwlengte
│               │   └── Children
│               │       ├── [10000]  T-shirt
│               │       │   ├── Attributes
│               │       │   │   └── [Halsvorm, RECOMMENDED, LIST (V-Hals|Ronde hals|Col|Kraag)]  Halsvorm
│               │       │   └── Children
│               │       │       ├── [100000]  T-shirt
│               │       │       └── [100001]  Poloshirt
│               │       ├── [10001]  Sweater & Trui & Vest
│               │       │   └── Children
│               │       │       ├── [100010]  Vest
│               │       │       ├── [100011]  Trui
│               │       │       ├── [100012]  Sweater
│               │       │       ├── [100013]  Ski trui
│               │       │       ├── [100014]  Ski vest
│               │       │       ├── [100015]  Bolero
│               │       │       └── [100016]  Vest
│               │       ├── [10002]  Blouse & Tuniek & Overhemd
│               │       │   └── Children
│               │       │       ├── [100020]  Overhemd
│               │       │       ├── [100021]  Blouse
│               │       │       ├── [100022]  Tuniek
│               │       │       └── [100023]  Overhemd
│               │       ├── [10003]  Jurk
│               │       │   ├── Attributes
│               │       │   │   └── [Halsvorm, RECOMMENDED, LIST (V-Hals|Ronde hals|Col|Kraag)]  Halsvorm
│               │       │   └── Children
│               │       │       ├── [100030]  Jurk
│               │       │       ├── [100031]  Jurk lange mouw
│               │       │       ├── [100032]  Jurk korte mouw
│               │       │       ├── [100033]  Jurk zonder mouw
│               │       │       └── [100034]  Overgooier
│               │       ├── [10004]  Jas
│               │       │   └── Children
│               │       │       ├── [100040]  Denim jack
│               │       │       ├── [100041]  Jas
│               │       │       ├── [100042]  Bodywarmer
│               │       │       ├── [100043]  Regenjas
│               │       │       ├── [100044]  Ski-jas
│               │       │       ├── [100045]  Trainingjas
│               │       │       ├── [100046]  Regenjas
│               │       │       └── [100047]  Ski-jas
│               │       └── [10005]  Colbert & Gilet
│               │           └── Children
│               │               ├── [100050]  Colbert & Gillet
│               │               ├── [100051]  Blazer
│               │               ├── [100052]  Jasje
│               │               ├── [100053]  Gilet
│               │               ├── [100054]  Colbert
│               │               └── [100055]  Colbert
│               ├── [1001]  Onderstukken
│               │   └── Children
│               │       ├── [10010]  Broek
│               │       │   ├── Attributes
│               │       │   │   └── [Pasvorm, OPTIONAL, LIST (Slim fit|Skinny fit|Regular fit|Tapered fit|Flared|Bootcut|Banana fit|Super Skinny|Boyfriend)]  Pasvorm
│               │       │   └── Children
│               │       │       ├── [100100]  Jumpsuit
│               │       │       ├── [100101]  Lange broek
│               │       │       ├── [100102]  Korte broek
│               │       │       ├── [100103]  Joggingbroek
│               │       │       ├── [100104]  Regenbroek
│               │       │       ├── [100105]  Legging
│               │       │       ├── [100106]  Skibroek
│               │       │       ├── [100107]  Trainingsbroek
│               │       │       └── [100108]  Truinbroek
│               │       ├── [10011]  Jeans
│               │       │   ├── Attributes
│               │       │   │   └── [Pasvorm, OPTIONAL, LIST (Slim fit|Skinny fit|Regular fit|Tapered fit|Flared|Bootcut|Banana fit|Super Skinny|Boyfriend)]  Pasvorm
│               │       │   └── Children
│               │       │       ├── [100110]  Jeans
│               │       │       ├── [100112]  Bermuda
│               │       │       └── [100113]  Korte broek
│               │       ├── [10012]  Rok
│               │       │   └── Children
│               │       │       └── [100120]  Rok
│               │       └── [10013]  Beenmode
│               │           ├── Attributes
│               │           │   └── [Maat (Beenmode), REQUIRED, LIST (27-30|31-34|35-38|39-42)]  Maat (Beenmode)
│               │           └── Children
│               │               ├── [100130]  Maillot
│               │               ├── [100131]  Skisokken
│               │               ├── [100132]  Panty
│               │               └── [100133]  Sokken
│               └── [1002]  Combos
│                   ├── Attributes
│                   │   └── [Mouwlengte, OPTIONAL, LIST (lange mouw|korte mouw|mouwloos|3/4 mouw|7)]  Mouwlengte
│                   └── Children
│                       ├── [10020]  Ondergoed & Romper
│                       │   └── Children
│                       │       ├── [100200]  Hipster
│                       │       ├── [100201]  Boxer
│                       │       ├── [100202]  Slip
│                       │       ├── [100203]  Hemd
│                       │       │   └── Attributes
│                       │       │       └── [Halsvorm, OPTIONAL, LIST (V-Hals|Ronde hals|Col|Kraag)]  Halsvorm
│                       │       ├── [100204]  Romper
│                       │       ├── [100205]  Onesie
│                       │       ├── [100206]  Topje
│                       │       ├── [100207]  Thermo ondergoed
│                       │       ├── [100208]  BH
│                       │       │   └── Attributes
│                       │       │       └── [Maat, REQUIRED, LIST (AA60|AA65|AA70|AA75|A65|A70|A75|A80|B70|B75|B80|B85|C75|C80|C85|C90|D75|D80|D85|E80|E85)]  Maat
│                       │       └── [100209]  Boxpak
│                       ├── [10021]  Nachtkleding
│                       │   └── Children
│                       │       ├── [100210]  Pyjama top
│                       │       ├── [100211]  Pyjama
│                       │       ├── [100212]  Pyjamabroek
│                       │       ├── [100213]  Nachthemd
│                       │       ├── [100214]  Badjas
│                       │       └── [100215]  Badjas
│                       ├── [10022]  Badkleding
│                       │   └── Children
│                       │       ├── [100220]  Bikini top
│                       │       ├── [100221]  Badpak
│                       │       ├── [100222]  Bikini
│                       │       ├── [100223]  Zwembroek
│                       │       ├── [100224]  Swimsuit
│                       │       ├── [100225]  Swimshirt
│                       │       ├── [100226]  Bikini broekje
│                       │       └── [100227]  Zwemshort
│                       ├── [10023]  Combo
│                       │   └── Children
│                       │       ├── [100230]  Trainingspak
│                       │       ├── [100231]  Joggingpak
│                       │       ├── [100232]  Regenpak
│                       │       ├── [100233]  Skipak
│                       │       ├── [100234]  Overall
│                       │       └── [100235]  Kledingset
│                       ├── [10024]  Verkleedkleding
│                       │   └── Children
│                       │       ├── [100240]  Verkleedpak
│                       │       ├── [100241]  Verkleedjurk
│                       │       ├── [100242]  Verkleedrok
│                       │       ├── [100243]  Verkleedbroek
│                       │       ├── [100244]  Verkleedtop
│                       │       ├── [100245]  Verkleedjas
│                       │       ├── [100246]  Lederhosen
│                       │       ├── [100247]  Verkleed onesie
│                       │       ├── [100248]  Verkleed schort
│                       │       └── [100249]  Verkleed tuinbroek
│                       └── [10025]  Verkleedkleding 2
│                           └── Children
│                               ├── [100250]  Verkleed poncho
│                               ├── [100251]  Verkleed cape
│                               ├── [100252]  Dirndl
│                               ├── [100253]  Foampak
│                               └── [100254]  Dierenpak
├── [12]  Schoenen
│   ├── Attributes
│   │   ├── [Merk, REQUIRED, LIST (2-Play|3Doodler|4M|80DBORIGINAL|999 games|Abbey Darts|Achoka|Acrobat|Active|adidas originals|adidas Performance|Adora|Aerobie|Aeronautica Militare|Airforce|Air Hogs|Ajax|Ambi Toys|American outfitters|America Today|AMIGO|Amleg|Amscan|Anagram|Androni|Angel Sports|Angel Toys|Angry Birds|Anker|Antony Morato|AO76|Aquabeads|Aquaplay|Armani|Armani shoes|Asmodee|Asterix & Obelix|Aurora|AutoStyle|AXI|Babeurre|Baby Alive|Baby Annabell|BABY born|Baby Dutch|Babyface|Baby & Kidz Banz|Babymel|Babyproof|Baby Toys|Bad Boys|Bakugan|Ballon|Bampidano|BanBao|Banpresto|Barbapapa|Barbara Farber|Barbie|Barts|Base Toys|Bauer|Baufix|Bayer|BBlocks|Bburago|Bebeboom|Beco|Beebielove|Be Kool|Beleduc|Bellaire|Bema|Bengh|Benson|BERG|Bergstein|Berjuan|Bess|Bestex|Bestway|Betula|BEX|Beyblade|BIColini|Big|Bigjigs|Bilibo|Billabong|Billieblush|Billybandit|BiOBUDDi|Birkenstock|Bjorn Borg|bla bla bla|Black Dragon|Blåkläder|Bloomingville|Blue Concept|Blueprint Collections|Blue Rebel|Blue System|Blutsbaby|B.Nosy|Bo-Bell|Bo Dean|Boeken|Bomba|Bonikka|Bonnie Doon|Boob Maternity|Boon|Born to be famous|Bor*z|BOSS|Boycot it|Brakkies|Brand New|Brandweerman Sam|Braqeez|Bratz|Brio|Britains|British Knights|Bruder|Brunotti|Bruynzeel|BS Toys|Bubble World|Bubblez|Bullboxer|Bull Boys|Bull´s|Bumper|Bunnies|byAstrup|Cakewalk|Calvin Klein|Caran d'Ache|Carbone|Carioca|Carpoint|Carrera|Cars Jeans|Cartamundi|Cartoon Network|Cartronic|Castorland|Catimini|Cause|Cavallaro|Chaos-and-Order|Chaya|Chicamala|Chicco|Childhome|Chillafish|Chuggington|CKS|Claesen's|Claire|Clementoni|Clic|Clicformers|Clics|Clown|Clown Games|Cobble Hill|Cobi|Collecta|Collonil|Comansi|Converse|Copa|Corolle|Cottonfield|Cougar|Craft Sensations|Crayola|Create It!|Creative|Creative Kids|Creotime|Crocodile Creek|Cruyff classics|Crystal Puzzle|Custo Growing|Czech Games Edition|Days of Wonder|DC Comics|Deltas|Denham|Denny Rose|Depesche|Desigual|Despicable Me|Develab|Dhink|Diakakis|Diamant Toys|Diamond Dotz|Dickies|DIDI|Diesel|Difuzed|Dino|Dirkje|Discraft|Disney|Disney Gold Collection|Dixon|Djeco|DKNY|Dobber|Doerak|Dolce Toys|Dolly Moda|Donic Schildkröt|Dragon Fly|Drystone|Ducati|Ducky Beau|Dummy merk|Eager Beaver|EB shoes|écoiffier|Eddie Pen|Eddy Toys|Edushape|Eeboo|Eichhorn|Eliane et Lena|Elle|Elliot|Emoji|Energie|Engelhart|Engino|Enigma|Enjoy Summer|Esprit|Est1842|Eurodisc|EverEarth|EXIT|Exploding Kittens|Fagus|Falca|FALKE|Famosa|Fashion4U|Fashion Angels|Fat Brain Toys|FC Barcelona|Feber|Fehn|Feuchtmann|Fila|Fimo|Fischertechnik|Fisher-Price|Five Nights at Freddy's|Flexo|Foam Clay|Folat|Freaks|Free and Easy|Fridolin|Fun|Funko|Gaastra|Gabbiano|Galt|Game On Sport|Gant|Gant Shoes|Garcia|Gattino|GB Eye|Gear2Play|Gearbox|Geddes & Gillmore|Geisha|Gel-A-Peel|GeoMag|Geosmart|Geoworld|Geox|Gerardo's Toys|Get & Go|Gibsons|Gigamic|Gioseppo|Giotto|Glamorous|Globber|Globos Nordic|Gobbledygook|Goki|Goliath|Gonher|Goodmark|Goula|Grafix|Gre|Green Toys|Gsus|Guess|Günther|Haba|Hanssop|Hape|Happy2play|Happy Baby|Happy Horse|Happy People|Harrows|Harry Potter|Hasbro|Hatley|Havaianas|Haza Original|Heelarious|Heimess|Heless|Hello Kitty|Helly Hansen|Hip|Holster Australia|Holztiger|Hoooked|Hörby Bruk|HORKA|Hot Wheels|Hound|House of Kids|HQ Kites|Hubelino|Hummel|Hupsakee|Huzzle|Icepeak|Iconx|iDance|Identity Games|iDO|Iello|Igor|IKKS|Illooms|IMC|Imps & Elfs|I'm Toy|Indian Blue Jeans|Infinity Nado|Injusa|Interline|Interstat|Intex|Invento|Ipanema|Italtrike|iWallz|Jack & Jones|Jackpot|Jake Fischer|Janod|Jansen Tilanus|JD Bug|Jeep|Jellycat|Jemini|Jill|Jochie|Joe Color|Joe Color|Johntoy|Joker Entertainment|Jonotoys|Jottum|Jouéco|Joy Toy|Jumbo|Jumini|Jumpking|Jurassic World|Just Beach|Just Games|Kamparo|Kanjers|Kapla|Kaporal|Karl Lagerfeld|Käthe Kruse|KBT|Kickers|Kidboxer|Kiddieland|Kiddo|Kidea|Kidid|Kid O|Kidorable|Kids At Work|Kidscase|Kidzface|Kids Fun|Kids Globe|KIDS ONLY|Kidzart|Kidzroom|Kidzzbelts|Kinderfeets|King|Kipling|kleertjes.com|Klein|K'NEX|Koeka|Koko Noko|KREUL|Krunk denim|Lacoste|La Franc|Lalaloopsy|LaQ|LCEE|Lckr|Learning Resources|Le Big|Le Chic|Le Chic shoes|Le Coq Sportif|LEGO|Lelli Kelly|Lelly|Lena|Let's Play|Levi's|LG-Imports|Libellud|Lief|Lifetime|Lifetime Games|Light Stax|Like Flo|Lili Gaufrette|Lilliputiens|Li'l Woodzeez|Little Buddy|Little Company|Little Dutch|Little Eleven Paris|Little Feet|Little Live Pets|Little Tikes|LMTD|LOL Surprise|Loom Twister|Looney Tunes|Love2wait|Lovely Lea|Ludonaute|Luminou|Lumo Stars|Luna|Lyle & Scott|Lyra|Madd Gear|Magformers|Magic the Gathering|Maileg|Maisto|Maliblue|Malinos|Mama Licious|Mamamemo|Marionette|Marlin|Maruti|Marvel|Marybel|Matchbox|Mattel|Mayoral|McGregor|Meccano|Megableu|Mega Bloks|Meiya & Alvin|Melissa & Doug|Me & My Monkey|Mercedes-Benz|Metal Earth|Me To You|Mexx|MGA|Mignolo|Mijn|Milky Kiss|Milly Mally|Mim-Pi|Mim-pi Shoes|Minecraft|Miniland|Minimize|Mini Mommy|Minions|Ministeck|Miraculous|Miss Grant|Miss Sixty|Mistral|Molo|Monchhichi|Mondo|Monster High|Moodstreet|Moon Glow|Moose Toys|Moschino|Moses|Move|Muchachomalo|Mudpuppy|Multiprint|Munich|Murphy & Nye|My Brand|My Little Baby|My Little Pony|Nail-A-Peel|Name It|Nanostad|Napapijri|Naturino|Navir|Nenuco|NERF|New Balance|New Classic Toys|New Port|nic|Nickelodeon|Nicotoy|Nijdam|Nijntje|Nike|Nikko|Ninco|Ninja|Nintendo|Nobell|Noble Collection|No Excess|Non-License|Nono|NOP|Noppies|Noppies Maternity|North Sails|No Tomatoes|Nova Carta|Num Noms|Merkloos|O'Chill|Oilily|Olang|O'Neill|Orange stars|Osprey|Ostheimer|Our Generation|Outdoor|Outdoor Play|Outfitters Nation|Out of the Blue|Oxxy|Pako|Paladone|Pampers|Panini|Pantofola d'Oro|Paradiso Toys|Parajumpers|Party Stars|Party Time|Patrizia Pepe|Paul Frank|Peaceable Kingdom|Pébéo|Pegaso|Pentel|Pepe Jeans|Peppa Pig|Pequetren|Perfect Petzzz|Peri Ponchos|Petit Bateau|Petit Collage|Petite Jolie|Petit Louie|Petit Sofie Schnoor|Petrol|Pexkids|Philos|Phoenix|Pick & Pack|Pinocchio|Pintoy|Pinypon|PIXIE CREW|Plan B Games|Play-Doh|Playforever|Playfun|Play & go|Playlife|PlayMais|PLAYMOBIL|Playshoes|Playskool|Playtime|Playwood|Please|Plenty Gifts|Plum|Plus-Plus|PMS|Poederbaas|Pointer|Pokémon|Polesie|Polly Pocket|Polo Ralph Lauren|Polo Ralph Lauren shoes|PolyM|Power Rangers|Powerslide|Prism|Procos|Professor Puzzle|Project Mc2|Proline|ProPlus|Protest|PSV|Puma|Pusheen|Pustefix|QPlay|Queen mum|Quercetti|Quick|Quiksilver|Quut|R95th|Rags Industry|Raizzed|Rampage|Rastar|Ravensburger|Ravensburger|Razor|Ready2Robot|Recent Toys|Red Code|Red Rag|Reebok|Reef|Relaunch|Replay&Sons|Repos Production|Retour Jeans|Retour Shoes|Retro Roller|Revell|Rhombus|Ria Menorca|RIO Roller|River Woods|Roces|Rollplay|Rolly Toys|Rondinella|Rookie|Rookies|Room Seven|Rory's Story Cubes|Rose & Romeo|Rough Shoes|Roxy|Rubbabu|Rubber Duck|Rubo Toys|Ruby Brown|Rucanor|Rumbl|RunRunToys|Russell Athletic|Salty Dog|Sambro|Sanetta|Sanrio|Schjerning|Schleich|Schmidt|Schylling|Science 4 You|Street Called Madison|Scotch & Soda|Scratch|Screechers Wild|SES|Sesamstraat|Sevenoneseven|Sevi|SFR|Shiwi|Shoe Republic|Shoesme|Sigikid|Siku|Silk Clay|Silverlit|Silvian Heach|Simply for Kids|Sinner|Skechers|Skooter|Slackers|Slamm|Slammer|Sluban|Smartmax|SmarTrike|Snapper Rock|SOAK|Soccer Jeans Company|S.Oliver|Soy Luna|Space Cowboys|Space Scooter|Speedo|Spiderkites|Spider-Man|Spielstabil|Spin Master|SportX|Stabilo|Stamp|State Of Art Rookies|Steiff|Step2|Stephen Joseph Gifts|Stiga|Stone Island|Storksak|StreetSurfing|Studio 100|Studio Maison|Style Me Up!|Stylex|Suki|Summer fun|Summerplay|Summertime|Sunkid|Sunny|Superga|Superman|Supermom|Superrebel|Supertrash|Super Wings|Swim Essentials|SwimWays|Sylvanian Families|Tactic|Taille 0|Tanner|Tatiri|Teletubbies|Tempish|Ten Cate|Tender Toys|Test merk - t.b.v prod|Teva|The Dutch Design Bakery|The Future Is Ours|The Offbits|The Secret Life of Pets|The White Brand|ThinkFun|Thomas de Trein|Tiamo|Tidlo|Tiger Tribe|Tikiri|Timberland|Timberland Shoes|TOM|Tomina|Tommy Hilfiger|Tommy Hilfiger shoes|Tomy|Toppoint|Topwrite Kids|Totum|Toyrific|Toys Pure|Toystate|Traditional Garden Games|Transformers|Travis Design|Trefl|Tretorn|Trudi|Tumble 'N Dry|Tutti Frutti|Twickto|Twinlife|Twizz|Ty|TYGO & vito|Ubisoft|UGG|Unico|Unicorn|Unique|Unisa|Universal|University Games|Van Dijk Toys|Vans|Varta|V-Cube|VDM|Vilebrequin|Vingino|Vingino Eyewear|Vingino shoes|Volare|VTech|Wader|Waimea|Warmbat|Warmies|Waterworld|Waterzone|Wave Racers|Waytoplay|WE Fashion|Weible Knet|Welliebellies|Welly|White Goblin Games|Wicke|Wicked|Wiking|Wild|Wildfish|Wild Republic|Wild Shoes|Winning Moves|Wizards of the Coast|Wobbel|Woezel & Pip|Wooden City|Woolrich|Worlds Apart|Wrebbit|WWF|XIN YU Toys|Xootz|Xplorys|XQ Max|XS Feet|Yello|Yellow Cab|Yep Yep|Yookidoo|Zapf Creation|ZEBRA|Zecchino D oro|ZEE & ZO|Zero2Three|Z-Man Games|Zoomer|Zootjes|Zuru)]  Merk
│   │   ├── [Maat, REQUIRED, LIST (15|16|16,5|17|17,5|18|18,5|19|19,5|20|20,5|21|21,5|22|22,5|23|23,5|24|24,5|25|25,5|26|26,5|27|27,5|28|28,5|29|29,5|30|30,5|31|31,5|32|32,5|33|33,5|34|34,5|35|35,5|36|36,5|37|37,5|38|38,5|39|39,5|40|40,5|41|41,5|42|42,5|43|43,5|44|44,5|45|45,5|46|15-16|17-18|19-20|21-22|23-24|25-26|27-28|29-30|31-32|33-34|35-36|37-38|39-40|41-42|43-44|45-46|16-17|18-19|20-21|22-23|24-25|26-27|28-29|30-31|32-33|34-35|36-37|38-39|40-41|42-43|44-45|50)]  Maat
│   │   ├── [Basiskleur, REQUIRED, LIST (Blauw|Groen|Rood|Zwart|Wit|Grijs|Roze|Offwhite|Oranje|Zilver|Bruin|Beige|Paars|Geel|Goud|Turquoise|Multicolor)]  Basiskleur
│   │   ├── [Geslacht, REQUIRED, LIST (Jongen|Meisje|Unisex|Baby)]  Geslacht
│   │   ├── [Collectie, REQUIRED, LIST (2013|2014|2015|2016|2017|2018|2019|2020|basics|1999|2021)]  Collectie
│   │   ├── [Seizoen, REQUIRED, LIST (zomer|winter|all season|herfst)]  Seizoen
│   │   ├── [Variant groepnummer, REQUIRED, TEXT]  Variant groepnummer
│   │   ├── [Leveranciersmaat, RECOMMENDED, TEXT]  Leveranciersmaat
│   │   ├── [Schoenhoogte, OPTIONAL, LIST (laag|hoog|hoger)]  Schoenhoogte
│   │   ├── [Leverancierskleur, REQUIRED, TEXT]  Leverancierskleur
│   │   ├── [Sport, OPTIONAL, LIST_MULTIPLE_VALUES (Voetbal|Tennis|Hockey|Skieën|Darts|Outdoor|Running|Fitness|Basketbal)]  Sport
│   │   └── [Duurzaamheid, OPTIONAL, LIST (Global Organic Textile Standard 2|Organic Content Standard|Responsible Wool Standard2|Leather Working Group2|Forest Stewardship Council2|Fair Trade cotton2|Better Cotton Initiative2|OEKO-TEX Standard 100 2|Gemaakt van gerecycled materiaal2|Gemaakt van biologisch katoen2|Waarde bij de code2)]  Duurzaamheid
│   └── Children
│       └── [120]  Kinderschoenen
│           └── Children
│               ├── [1200]  Schoenen
│               │   └── Children
│               │       └── [12000]  Schoenen algemeen
│               │           └── Children
│               │               ├── [120000]  Schoenen
│               │               ├── [120001]  Waterschoenen
│               │               ├── [120002]  Babyslofjes
│               │               └── [120003]  Waterschoenen
│               ├── [1201]  Sandalen
│               │   └── Children
│               │       └── [12010]  Sandalen
│               │           └── Children
│               │               └── [120100]  Sandalen
│               ├── [1202]  Laarzen
│               │   └── Children
│               │       └── [12020]  Laarzen
│               │           └── Children
│               │               ├── [120200]  Laarzen
│               │               ├── [120201]  Enkellaarzen
│               │               ├── [120202]  Regenlaarzen
│               │               ├── [120203]  Snowboots
│               │               ├── [120204]  Regenlaarzen
│               │               └── [120205]  Snowboots
│               ├── [1203]  Sneakers
│               │   └── Children
│               │       └── [12030]  Sportschoenen
│               │           └── Children
│               │               ├── [120300]  Sneakers
│               │               ├── [120301]  Voetbalschoenen
│               │               └── [120302]  Sneakers hoog
│               ├── [1204]  Ballerinas
│               │   └── Children
│               │       └── [12040]  Ballerinas
│               │           └── Children
│               │               └── [120400]  Ballerinas
│               ├── [1205]  Slippers
│               │   └── Children
│               │       └── [12050]  Slippers
│               │           └── Children
│               │               ├── [120500]  Slippers
│               │               └── [120501]  Teenslippers
│               ├── [1206]  Mocassins
│               │   └── Children
│               │       └── [12060]  Mocassins
│               │           └── Children
│               │               ├── [120600]  Moccassins
│               │               ├── [120601]  Moccassins laag
│               │               └── [120602]  Moccassins hoog
│               ├── [1207]  Babyschoenen
│               │   └── Children
│               │       └── [12070]  Babyschoenen
│               │           └── Children
│               │               ├── [120700]  Babyslofjes
│               │               ├── [120701]  Veterschoenen laag
│               │               └── [120702]  Veterschoenen hoog
│               ├── [1208]  Pantoffels
│               │   └── Children
│               │       └── [12080]  Pantoffels
│               │           └── Children
│               │               ├── [120800]  Pantoffels
│               │               ├── [120801]  Klittenbandschoenen laag
│               │               └── [120802]  Klittenbandschoenen hoog
│               ├── [1209]  Instapschoenen
│               │   └── Children
│               │       └── [12090]  Instapschoenen
│               │           └── Children
│               │               ├── [120900]  Instapschoenen
│               │               └── [120901]  Espadrilles
│               └── [1210]  Verkleedschoenen
│                   └── Children
│                       └── [12100]  Verkleedschoenen
│                           └── Children
│                               └── [121000]  Verkleedschoenen
├── [13]  Accessoires
│   ├── Attributes
│   │   ├── [Merk, REQUIRED, LIST (2-Play|3Doodler|4M|80DBORIGINAL|999 games|Abbey Darts|Achoka|Acrobat|Active|adidas originals|adidas Performance|Adora|Aerobie|Aeronautica Militare|Airforce|Air Hogs|Ajax|Ambi Toys|American outfitters|America Today|AMIGO|Amleg|Amscan|Anagram|Androni|Angel Sports|Angel Toys|Angry Birds|Anker|Antony Morato|AO76|Aquabeads|Aquaplay|Armani|Armani shoes|Asmodee|Asterix & Obelix|Aurora|AutoStyle|AXI|Babeurre|Baby Alive|Baby Annabell|BABY born|Baby Dutch|Babyface|Baby & Kidz Banz|Babymel|Babyproof|Baby Toys|Bad Boys|Bakugan|Ballon|Bampidano|BanBao|Banpresto|Barbapapa|Barbara Farber|Barbie|Barts|Base Toys|Bauer|Baufix|Bayer|BBlocks|Bburago|Bebeboom|Beco|Beebielove|Be Kool|Beleduc|Bellaire|Bema|Bengh|Benson|BERG|Bergstein|Berjuan|Bess|Bestex|Bestway|Betula|BEX|Beyblade|BIColini|Big|Bigjigs|Bilibo|Billabong|Billieblush|Billybandit|BiOBUDDi|Birkenstock|Bjorn Borg|bla bla bla|Black Dragon|Blåkläder|Bloomingville|Blue Concept|Blueprint Collections|Blue Rebel|Blue System|Blutsbaby|B.Nosy|Bo-Bell|Bo Dean|Boeken|Bomba|Bonikka|Bonnie Doon|Boob Maternity|Boon|Born to be famous|Bor*z|BOSS|Boycot it|Brakkies|Brand New|Brandweerman Sam|Braqeez|Bratz|Brio|Britains|British Knights|Bruder|Brunotti|Bruynzeel|BS Toys|Bubble World|Bubblez|Bullboxer|Bull Boys|Bull´s|Bumper|Bunnies|byAstrup|Cakewalk|Calvin Klein|Caran d'Ache|Carbone|Carioca|Carpoint|Carrera|Cars Jeans|Cartamundi|Cartoon Network|Cartronic|Castorland|Catimini|Cause|Cavallaro|Chaos-and-Order|Chaya|Chicamala|Chicco|Childhome|Chillafish|Chuggington|CKS|Claesen's|Claire|Clementoni|Clic|Clicformers|Clics|Clown|Clown Games|Cobble Hill|Cobi|Collecta|Collonil|Comansi|Converse|Copa|Corolle|Cottonfield|Cougar|Craft Sensations|Crayola|Create It!|Creative|Creative Kids|Creotime|Crocodile Creek|Cruyff classics|Crystal Puzzle|Custo Growing|Czech Games Edition|Days of Wonder|DC Comics|Deltas|Denham|Denny Rose|Depesche|Desigual|Despicable Me|Develab|Dhink|Diakakis|Diamant Toys|Diamond Dotz|Dickies|DIDI|Diesel|Difuzed|Dino|Dirkje|Discraft|Disney|Disney Gold Collection|Dixon|Djeco|DKNY|Dobber|Doerak|Dolce Toys|Dolly Moda|Donic Schildkröt|Dragon Fly|Drystone|Ducati|Ducky Beau|Dummy merk|Eager Beaver|EB shoes|écoiffier|Eddie Pen|Eddy Toys|Edushape|Eeboo|Eichhorn|Eliane et Lena|Elle|Elliot|Emoji|Energie|Engelhart|Engino|Enigma|Enjoy Summer|Esprit|Est1842|Eurodisc|EverEarth|EXIT|Exploding Kittens|Fagus|Falca|FALKE|Famosa|Fashion4U|Fashion Angels|Fat Brain Toys|FC Barcelona|Feber|Fehn|Feuchtmann|Fila|Fimo|Fischertechnik|Fisher-Price|Five Nights at Freddy's|Flexo|Foam Clay|Folat|Freaks|Free and Easy|Fridolin|Fun|Funko|Gaastra|Gabbiano|Galt|Game On Sport|Gant|Gant Shoes|Garcia|Gattino|GB Eye|Gear2Play|Gearbox|Geddes & Gillmore|Geisha|Gel-A-Peel|GeoMag|Geosmart|Geoworld|Geox|Gerardo's Toys|Get & Go|Gibsons|Gigamic|Gioseppo|Giotto|Glamorous|Globber|Globos Nordic|Gobbledygook|Goki|Goliath|Gonher|Goodmark|Goula|Grafix|Gre|Green Toys|Gsus|Guess|Günther|Haba|Hanssop|Hape|Happy2play|Happy Baby|Happy Horse|Happy People|Harrows|Harry Potter|Hasbro|Hatley|Havaianas|Haza Original|Heelarious|Heimess|Heless|Hello Kitty|Helly Hansen|Hip|Holster Australia|Holztiger|Hoooked|Hörby Bruk|HORKA|Hot Wheels|Hound|House of Kids|HQ Kites|Hubelino|Hummel|Hupsakee|Huzzle|Icepeak|Iconx|iDance|Identity Games|iDO|Iello|Igor|IKKS|Illooms|IMC|Imps & Elfs|I'm Toy|Indian Blue Jeans|Infinity Nado|Injusa|Interline|Interstat|Intex|Invento|Ipanema|Italtrike|iWallz|Jack & Jones|Jackpot|Jake Fischer|Janod|Jansen Tilanus|JD Bug|Jeep|Jellycat|Jemini|Jill|Jochie|Joe Color|Joe Color|Johntoy|Joker Entertainment|Jonotoys|Jottum|Jouéco|Joy Toy|Jumbo|Jumini|Jumpking|Jurassic World|Just Beach|Just Games|Kamparo|Kanjers|Kapla|Kaporal|Karl Lagerfeld|Käthe Kruse|KBT|Kickers|Kidboxer|Kiddieland|Kiddo|Kidea|Kidid|Kid O|Kidorable|Kids At Work|Kidscase|Kidzface|Kids Fun|Kids Globe|KIDS ONLY|Kidzart|Kidzroom|Kidzzbelts|Kinderfeets|King|Kipling|kleertjes.com|Klein|K'NEX|Koeka|Koko Noko|KREUL|Krunk denim|Lacoste|La Franc|Lalaloopsy|LaQ|LCEE|Lckr|Learning Resources|Le Big|Le Chic|Le Chic shoes|Le Coq Sportif|LEGO|Lelli Kelly|Lelly|Lena|Let's Play|Levi's|LG-Imports|Libellud|Lief|Lifetime|Lifetime Games|Light Stax|Like Flo|Lili Gaufrette|Lilliputiens|Li'l Woodzeez|Little Buddy|Little Company|Little Dutch|Little Eleven Paris|Little Feet|Little Live Pets|Little Tikes|LMTD|LOL Surprise|Loom Twister|Looney Tunes|Love2wait|Lovely Lea|Ludonaute|Luminou|Lumo Stars|Luna|Lyle & Scott|Lyra|Madd Gear|Magformers|Magic the Gathering|Maileg|Maisto|Maliblue|Malinos|Mama Licious|Mamamemo|Marionette|Marlin|Maruti|Marvel|Marybel|Matchbox|Mattel|Mayoral|McGregor|Meccano|Megableu|Mega Bloks|Meiya & Alvin|Melissa & Doug|Me & My Monkey|Mercedes-Benz|Metal Earth|Me To You|Mexx|MGA|Mignolo|Mijn|Milky Kiss|Milly Mally|Mim-Pi|Mim-pi Shoes|Minecraft|Miniland|Minimize|Mini Mommy|Minions|Ministeck|Miraculous|Miss Grant|Miss Sixty|Mistral|Molo|Monchhichi|Mondo|Monster High|Moodstreet|Moon Glow|Moose Toys|Moschino|Moses|Move|Muchachomalo|Mudpuppy|Multiprint|Munich|Murphy & Nye|My Brand|My Little Baby|My Little Pony|Nail-A-Peel|Name It|Nanostad|Napapijri|Naturino|Navir|Nenuco|NERF|New Balance|New Classic Toys|New Port|nic|Nickelodeon|Nicotoy|Nijdam|Nijntje|Nike|Nikko|Ninco|Ninja|Nintendo|Nobell|Noble Collection|No Excess|Non-License|Nono|NOP|Noppies|Noppies Maternity|North Sails|No Tomatoes|Nova Carta|Num Noms|Merkloos|O'Chill|Oilily|Olang|O'Neill|Orange stars|Osprey|Ostheimer|Our Generation|Outdoor|Outdoor Play|Outfitters Nation|Out of the Blue|Oxxy|Pako|Paladone|Pampers|Panini|Pantofola d'Oro|Paradiso Toys|Parajumpers|Party Stars|Party Time|Patrizia Pepe|Paul Frank|Peaceable Kingdom|Pébéo|Pegaso|Pentel|Pepe Jeans|Peppa Pig|Pequetren|Perfect Petzzz|Peri Ponchos|Petit Bateau|Petit Collage|Petite Jolie|Petit Louie|Petit Sofie Schnoor|Petrol|Pexkids|Philos|Phoenix|Pick & Pack|Pinocchio|Pintoy|Pinypon|PIXIE CREW|Plan B Games|Play-Doh|Playforever|Playfun|Play & go|Playlife|PlayMais|PLAYMOBIL|Playshoes|Playskool|Playtime|Playwood|Please|Plenty Gifts|Plum|Plus-Plus|PMS|Poederbaas|Pointer|Pokémon|Polesie|Polly Pocket|Polo Ralph Lauren|Polo Ralph Lauren shoes|PolyM|Power Rangers|Powerslide|Prism|Procos|Professor Puzzle|Project Mc2|Proline|ProPlus|Protest|PSV|Puma|Pusheen|Pustefix|QPlay|Queen mum|Quercetti|Quick|Quiksilver|Quut|R95th|Rags Industry|Raizzed|Rampage|Rastar|Ravensburger|Ravensburger|Razor|Ready2Robot|Recent Toys|Red Code|Red Rag|Reebok|Reef|Relaunch|Replay&Sons|Repos Production|Retour Jeans|Retour Shoes|Retro Roller|Revell|Rhombus|Ria Menorca|RIO Roller|River Woods|Roces|Rollplay|Rolly Toys|Rondinella|Rookie|Rookies|Room Seven|Rory's Story Cubes|Rose & Romeo|Rough Shoes|Roxy|Rubbabu|Rubber Duck|Rubo Toys|Ruby Brown|Rucanor|Rumbl|RunRunToys|Russell Athletic|Salty Dog|Sambro|Sanetta|Sanrio|Schjerning|Schleich|Schmidt|Schylling|Science 4 You|Street Called Madison|Scotch & Soda|Scratch|Screechers Wild|SES|Sesamstraat|Sevenoneseven|Sevi|SFR|Shiwi|Shoe Republic|Shoesme|Sigikid|Siku|Silk Clay|Silverlit|Silvian Heach|Simply for Kids|Sinner|Skechers|Skooter|Slackers|Slamm|Slammer|Sluban|Smartmax|SmarTrike|Snapper Rock|SOAK|Soccer Jeans Company|S.Oliver|Soy Luna|Space Cowboys|Space Scooter|Speedo|Spiderkites|Spider-Man|Spielstabil|Spin Master|SportX|Stabilo|Stamp|State Of Art Rookies|Steiff|Step2|Stephen Joseph Gifts|Stiga|Stone Island|Storksak|StreetSurfing|Studio 100|Studio Maison|Style Me Up!|Stylex|Suki|Summer fun|Summerplay|Summertime|Sunkid|Sunny|Superga|Superman|Supermom|Superrebel|Supertrash|Super Wings|Swim Essentials|SwimWays|Sylvanian Families|Tactic|Taille 0|Tanner|Tatiri|Teletubbies|Tempish|Ten Cate|Tender Toys|Test merk - t.b.v prod|Teva|The Dutch Design Bakery|The Future Is Ours|The Offbits|The Secret Life of Pets|The White Brand|ThinkFun|Thomas de Trein|Tiamo|Tidlo|Tiger Tribe|Tikiri|Timberland|Timberland Shoes|TOM|Tomina|Tommy Hilfiger|Tommy Hilfiger shoes|Tomy|Toppoint|Topwrite Kids|Totum|Toyrific|Toys Pure|Toystate|Traditional Garden Games|Transformers|Travis Design|Trefl|Tretorn|Trudi|Tumble 'N Dry|Tutti Frutti|Twickto|Twinlife|Twizz|Ty|TYGO & vito|Ubisoft|UGG|Unico|Unicorn|Unique|Unisa|Universal|University Games|Van Dijk Toys|Vans|Varta|V-Cube|VDM|Vilebrequin|Vingino|Vingino Eyewear|Vingino shoes|Volare|VTech|Wader|Waimea|Warmbat|Warmies|Waterworld|Waterzone|Wave Racers|Waytoplay|WE Fashion|Weible Knet|Welliebellies|Welly|White Goblin Games|Wicke|Wicked|Wiking|Wild|Wildfish|Wild Republic|Wild Shoes|Winning Moves|Wizards of the Coast|Wobbel|Woezel & Pip|Wooden City|Woolrich|Worlds Apart|Wrebbit|WWF|XIN YU Toys|Xootz|Xplorys|XQ Max|XS Feet|Yello|Yellow Cab|Yep Yep|Yookidoo|Zapf Creation|ZEBRA|Zecchino D oro|ZEE & ZO|Zero2Three|Z-Man Games|Zoomer|Zootjes|Zuru)]  Merk
│   │   ├── [Basiskleur, REQUIRED, LIST (Blauw|Groen|Rood|Zwart|Wit|Grijs|Roze|Offwhite|Oranje|Zilver|Bruin|Beige|Paars|Geel|Goud|Turquoise|Multicolor)]  Basiskleur
│   │   ├── [Leverancierskleur, REQUIRED, TEXT]  Leverancierskleur
│   │   ├── [Geslacht, REQUIRED, LIST (Jongen|Meisje|Unisex|Baby)]  Geslacht
│   │   ├── [Collectie, OPTIONAL, LIST (2013|2014|2015|2016|2017|2018|2019|2020|basics|1999|2021)]  Collectie
│   │   ├── [Seizoen, REQUIRED, LIST (zomer|winter|all season|herfst)]  Seizoen
│   │   ├── [Variant groepnummer, REQUIRED, TEXT]  Variant groepnummer
│   │   ├── [Materiaalomschrijving, RECOMMENDED, TEXT]  Materiaalomschrijving
│   │   ├── [Leveranciersmaat, RECOMMENDED, TEXT]  Leveranciersmaat
│   │   ├── [Materialen, RECOMMENDED, LIST_MULTIPLE_VALUES (acryl|alpaca|anders|angora|bamboe|chlorofibre|cupro|dons|elastan|hennep|inox|kasjmier|katoen|lamswol|leer|linnen|lurex|lycra|lyocell|merino|mesh|metaal|modal|mohair|nubuck|nylon|papier|polyamide|polyester|polypropyleen|polyurethaan|polyvinylchloride|ramie|rayon|resin|rubber|stro|suède|tactel|viscose|wax|wol|zijde|bont)]  Materialen
│   │   ├── [Maat, REQUIRED, LIST (One Size Kids/Teens|One Size Baby)]  Maat
│   │   ├── [Sport, OPTIONAL, LIST_MULTIPLE_VALUES (Voetbal|Tennis|Hockey|Skieën|Darts|Outdoor|Running|Fitness|Basketbal)]  Sport
│   │   └── [Duurzaamheid, OPTIONAL, LIST (Global Organic Textile Standard 2|Organic Content Standard|Responsible Wool Standard2|Leather Working Group2|Forest Stewardship Council2|Fair Trade cotton2|Better Cotton Initiative2|OEKO-TEX Standard 100 2|Gemaakt van gerecycled materiaal2|Gemaakt van biologisch katoen2|Waarde bij de code2)]  Duurzaamheid
│   └── Children
│       └── [130]  Kinderen
│           └── Children
│               ├── [1300]  Kinder accessoires
│               │   └── Children
│               │       ├── [13000]  Accessoires
│               │       │   └── Children
│               │       │       ├── [130000]  Babymuts
│               │       │       │   └── Attributes
│               │       │       │       └── [Maat, REQUIRED, LIST (40cm|45cm|47cm|50cm|53cm|56cm|One size baby|One size kids)]  Maat
│               │       │       ├── [130002]  Pet
│               │       │       │   └── Attributes
│               │       │       │       └── [Maat, REQUIRED, LIST (40cm|45cm|47cm|50cm|53cm|56cm|One size baby|One size kids)]  Maat
│               │       │       ├── [130003]  Hoedje
│               │       │       │   └── Attributes
│               │       │       │       └── [Maat, REQUIRED, LIST (40cm|45cm|47cm|50cm|53cm|56cm|One size baby|One size kids)]  Maat
│               │       │       ├── [130004]  Skihandschoenen
│               │       │       ├── [130005]  Handschoenen
│               │       │       ├── [130006]  Wanten
│               │       │       ├── [130007]  Riem
│               │       │       │   └── Attributes
│               │       │       │       └── [Maat, REQUIRED, LIST (55cm|65cm|75cm)]  Maat
│               │       │       ├── [130008]  Muts
│               │       │       │   └── Attributes
│               │       │       │       └── [Maat, REQUIRED, LIST (40cm|45cm|47cm|50cm|53cm|56cm|One size baby|One size kids)]  Maat
│               │       │       └── [130009]  Sjaal
│               │       ├── [13001]  Schoenonderhoud
│               │       │   └── Children
│               │       │       ├── [130010]  Inlegzooltjes
│               │       │       ├── [130011]  Veters
│               │       │       ├── [130012]  Schoenonderhoud
│               │       │       └── [130013]  Veters
│               │       ├── [13002]  Sieraden
│               │       │   └── Children
│               │       │       ├── [130020]  Horloge
│               │       │       ├── [130021]  Ketting
│               │       │       ├── [130022]  Armband
│               │       │       └── [130023]  Armband
│               │       ├── [13003]  Brillen
│               │       │   └── Children
│               │       │       ├── [130030]  Skibril
│               │       │       ├── [130031]  Zonnebril
│               │       │       ├── [130032]  Zwembril
│               │       │       └── [130033]  Zwembril
│               │       ├── [13004]  Tas & Koffer
│               │       │   └── Children
│               │       │       ├── [130040]  Trolley
│               │       │       ├── [130041]  Rugzak
│               │       │       ├── [130042]  Koffer
│               │       │       ├── [130043]  Zwemtas
│               │       │       ├── [130044]  Sporttas
│               │       │       ├── [130045]  Handtas
│               │       │       ├── [130046]  Schoudertas
│               │       │       ├── [130047]  Weekendtas
│               │       │       ├── [130048]  Shopper
│               │       │       └── [130049]  Luiertas
│               │       ├── [13005]  Haaraccessoires
│               │       │   └── Children
│               │       │       ├── [130050]  Haarband
│               │       │       └── [130051]  Bandana
│               │       ├── [13006]  Helmen
│               │       │   ├── Attributes
│               │       │   │   └── [Maat (Hoofddeksels), REQUIRED, LIST ()]  Maat (Hoofddeksels)
│               │       │   └── Children
│               │       │       └── [130060]  Skihelm
│               │       ├── [13007]  Ballen
│               │       │   └── Children
│               │       │       └── [130070]  Voetbal
│               │       ├── [13008]  Overige accessoires
│               │       │   └── Children
│               │       │       ├── [130080]  Paraplu
│               │       │       ├── [130081]  Voetbalaccessoires
│               │       │       ├── [130082]  Ballon
│               │       │       └── [130083]  Etui
│               │       └── [13009]  Mode accessoires
│               │           └── Children
│               │               ├── [130091]  Sjaaltje
│               │               ├── [130092]  Vlinderstrik
│               │               ├── [130093]  Stropdas
│               │               └── [130094]  Bretels
│               └── [1301]  Verkleed accessoires
│                   └── Children
│                       ├── [13010]  Verkleed accessoires
│                       │   └── Children
│                       │       ├── [130100]  Boa
│                       │       ├── [130101]  Verkleed bretels
│                       │       ├── [130102]  Verkleed bril
│                       │       ├── [130103]  Verkleed handschoen
│                       │       ├── [130104]  Verkleed riem
│                       │       ├── [130105]  Verkleed strik
│                       │       ├── [130106]  Verkleed stropdas
│                       │       ├── [130107]  Verkleed sjaal
│                       │       ├── [130108]  Verkleed sjerp
│                       │       └── [130109]  Verkleed tas
│                       ├── [13011]  Verkleed kousen en panty's
│                       │   └── Children
│                       │       ├── [130110]  Verkleedkousen
│                       │       ├── [130111]  Verkleed panty
│                       │       ├── [130112]  Verkleed legging
│                       │       ├── [130113]  Verkleed maillot
│                       │       └── [130114]  Verkleed beenwarmers
│                       └── [13012]  Verkleed haaraccessoires
│                           └── Children
│                               ├── [130120]  Verkleed hoed
│                               ├── [130121]  Verkleed diadeem
│                               ├── [130122]  Pruik
│                               ├── [130123]  Verkleed haaraccessoire
│                               ├── [130124]  Nep baard
│                               ├── [130125]  Nep snor
│                               └── [130126]  Pruiknet
├── [14]  Bedding & Lifestyle
│   ├── Attributes
│   │   ├── [Merk, REQUIRED, LIST (2-Play|3Doodler|4M|80DBORIGINAL|999 games|Abbey Darts|Achoka|Acrobat|Active|adidas originals|adidas Performance|Adora|Aerobie|Aeronautica Militare|Airforce|Air Hogs|Ajax|Ambi Toys|American outfitters|America Today|AMIGO|Amleg|Amscan|Anagram|Androni|Angel Sports|Angel Toys|Angry Birds|Anker|Antony Morato|AO76|Aquabeads|Aquaplay|Armani|Armani shoes|Asmodee|Asterix & Obelix|Aurora|AutoStyle|AXI|Babeurre|Baby Alive|Baby Annabell|BABY born|Baby Dutch|Babyface|Baby & Kidz Banz|Babymel|Babyproof|Baby Toys|Bad Boys|Bakugan|Ballon|Bampidano|BanBao|Banpresto|Barbapapa|Barbara Farber|Barbie|Barts|Base Toys|Bauer|Baufix|Bayer|BBlocks|Bburago|Bebeboom|Beco|Beebielove|Be Kool|Beleduc|Bellaire|Bema|Bengh|Benson|BERG|Bergstein|Berjuan|Bess|Bestex|Bestway|Betula|BEX|Beyblade|BIColini|Big|Bigjigs|Bilibo|Billabong|Billieblush|Billybandit|BiOBUDDi|Birkenstock|Bjorn Borg|bla bla bla|Black Dragon|Blåkläder|Bloomingville|Blue Concept|Blueprint Collections|Blue Rebel|Blue System|Blutsbaby|B.Nosy|Bo-Bell|Bo Dean|Boeken|Bomba|Bonikka|Bonnie Doon|Boob Maternity|Boon|Born to be famous|Bor*z|BOSS|Boycot it|Brakkies|Brand New|Brandweerman Sam|Braqeez|Bratz|Brio|Britains|British Knights|Bruder|Brunotti|Bruynzeel|BS Toys|Bubble World|Bubblez|Bullboxer|Bull Boys|Bull´s|Bumper|Bunnies|byAstrup|Cakewalk|Calvin Klein|Caran d'Ache|Carbone|Carioca|Carpoint|Carrera|Cars Jeans|Cartamundi|Cartoon Network|Cartronic|Castorland|Catimini|Cause|Cavallaro|Chaos-and-Order|Chaya|Chicamala|Chicco|Childhome|Chillafish|Chuggington|CKS|Claesen's|Claire|Clementoni|Clic|Clicformers|Clics|Clown|Clown Games|Cobble Hill|Cobi|Collecta|Collonil|Comansi|Converse|Copa|Corolle|Cottonfield|Cougar|Craft Sensations|Crayola|Create It!|Creative|Creative Kids|Creotime|Crocodile Creek|Cruyff classics|Crystal Puzzle|Custo Growing|Czech Games Edition|Days of Wonder|DC Comics|Deltas|Denham|Denny Rose|Depesche|Desigual|Despicable Me|Develab|Dhink|Diakakis|Diamant Toys|Diamond Dotz|Dickies|DIDI|Diesel|Difuzed|Dino|Dirkje|Discraft|Disney|Disney Gold Collection|Dixon|Djeco|DKNY|Dobber|Doerak|Dolce Toys|Dolly Moda|Donic Schildkröt|Dragon Fly|Drystone|Ducati|Ducky Beau|Dummy merk|Eager Beaver|EB shoes|écoiffier|Eddie Pen|Eddy Toys|Edushape|Eeboo|Eichhorn|Eliane et Lena|Elle|Elliot|Emoji|Energie|Engelhart|Engino|Enigma|Enjoy Summer|Esprit|Est1842|Eurodisc|EverEarth|EXIT|Exploding Kittens|Fagus|Falca|FALKE|Famosa|Fashion4U|Fashion Angels|Fat Brain Toys|FC Barcelona|Feber|Fehn|Feuchtmann|Fila|Fimo|Fischertechnik|Fisher-Price|Five Nights at Freddy's|Flexo|Foam Clay|Folat|Freaks|Free and Easy|Fridolin|Fun|Funko|Gaastra|Gabbiano|Galt|Game On Sport|Gant|Gant Shoes|Garcia|Gattino|GB Eye|Gear2Play|Gearbox|Geddes & Gillmore|Geisha|Gel-A-Peel|GeoMag|Geosmart|Geoworld|Geox|Gerardo's Toys|Get & Go|Gibsons|Gigamic|Gioseppo|Giotto|Glamorous|Globber|Globos Nordic|Gobbledygook|Goki|Goliath|Gonher|Goodmark|Goula|Grafix|Gre|Green Toys|Gsus|Guess|Günther|Haba|Hanssop|Hape|Happy2play|Happy Baby|Happy Horse|Happy People|Harrows|Harry Potter|Hasbro|Hatley|Havaianas|Haza Original|Heelarious|Heimess|Heless|Hello Kitty|Helly Hansen|Hip|Holster Australia|Holztiger|Hoooked|Hörby Bruk|HORKA|Hot Wheels|Hound|House of Kids|HQ Kites|Hubelino|Hummel|Hupsakee|Huzzle|Icepeak|Iconx|iDance|Identity Games|iDO|Iello|Igor|IKKS|Illooms|IMC|Imps & Elfs|I'm Toy|Indian Blue Jeans|Infinity Nado|Injusa|Interline|Interstat|Intex|Invento|Ipanema|Italtrike|iWallz|Jack & Jones|Jackpot|Jake Fischer|Janod|Jansen Tilanus|JD Bug|Jeep|Jellycat|Jemini|Jill|Jochie|Joe Color|Joe Color|Johntoy|Joker Entertainment|Jonotoys|Jottum|Jouéco|Joy Toy|Jumbo|Jumini|Jumpking|Jurassic World|Just Beach|Just Games|Kamparo|Kanjers|Kapla|Kaporal|Karl Lagerfeld|Käthe Kruse|KBT|Kickers|Kidboxer|Kiddieland|Kiddo|Kidea|Kidid|Kid O|Kidorable|Kids At Work|Kidscase|Kidzface|Kids Fun|Kids Globe|KIDS ONLY|Kidzart|Kidzroom|Kidzzbelts|Kinderfeets|King|Kipling|kleertjes.com|Klein|K'NEX|Koeka|Koko Noko|KREUL|Krunk denim|Lacoste|La Franc|Lalaloopsy|LaQ|LCEE|Lckr|Learning Resources|Le Big|Le Chic|Le Chic shoes|Le Coq Sportif|LEGO|Lelli Kelly|Lelly|Lena|Let's Play|Levi's|LG-Imports|Libellud|Lief|Lifetime|Lifetime Games|Light Stax|Like Flo|Lili Gaufrette|Lilliputiens|Li'l Woodzeez|Little Buddy|Little Company|Little Dutch|Little Eleven Paris|Little Feet|Little Live Pets|Little Tikes|LMTD|LOL Surprise|Loom Twister|Looney Tunes|Love2wait|Lovely Lea|Ludonaute|Luminou|Lumo Stars|Luna|Lyle & Scott|Lyra|Madd Gear|Magformers|Magic the Gathering|Maileg|Maisto|Maliblue|Malinos|Mama Licious|Mamamemo|Marionette|Marlin|Maruti|Marvel|Marybel|Matchbox|Mattel|Mayoral|McGregor|Meccano|Megableu|Mega Bloks|Meiya & Alvin|Melissa & Doug|Me & My Monkey|Mercedes-Benz|Metal Earth|Me To You|Mexx|MGA|Mignolo|Mijn|Milky Kiss|Milly Mally|Mim-Pi|Mim-pi Shoes|Minecraft|Miniland|Minimize|Mini Mommy|Minions|Ministeck|Miraculous|Miss Grant|Miss Sixty|Mistral|Molo|Monchhichi|Mondo|Monster High|Moodstreet|Moon Glow|Moose Toys|Moschino|Moses|Move|Muchachomalo|Mudpuppy|Multiprint|Munich|Murphy & Nye|My Brand|My Little Baby|My Little Pony|Nail-A-Peel|Name It|Nanostad|Napapijri|Naturino|Navir|Nenuco|NERF|New Balance|New Classic Toys|New Port|nic|Nickelodeon|Nicotoy|Nijdam|Nijntje|Nike|Nikko|Ninco|Ninja|Nintendo|Nobell|Noble Collection|No Excess|Non-License|Nono|NOP|Noppies|Noppies Maternity|North Sails|No Tomatoes|Nova Carta|Num Noms|Merkloos|O'Chill|Oilily|Olang|O'Neill|Orange stars|Osprey|Ostheimer|Our Generation|Outdoor|Outdoor Play|Outfitters Nation|Out of the Blue|Oxxy|Pako|Paladone|Pampers|Panini|Pantofola d'Oro|Paradiso Toys|Parajumpers|Party Stars|Party Time|Patrizia Pepe|Paul Frank|Peaceable Kingdom|Pébéo|Pegaso|Pentel|Pepe Jeans|Peppa Pig|Pequetren|Perfect Petzzz|Peri Ponchos|Petit Bateau|Petit Collage|Petite Jolie|Petit Louie|Petit Sofie Schnoor|Petrol|Pexkids|Philos|Phoenix|Pick & Pack|Pinocchio|Pintoy|Pinypon|PIXIE CREW|Plan B Games|Play-Doh|Playforever|Playfun|Play & go|Playlife|PlayMais|PLAYMOBIL|Playshoes|Playskool|Playtime|Playwood|Please|Plenty Gifts|Plum|Plus-Plus|PMS|Poederbaas|Pointer|Pokémon|Polesie|Polly Pocket|Polo Ralph Lauren|Polo Ralph Lauren shoes|PolyM|Power Rangers|Powerslide|Prism|Procos|Professor Puzzle|Project Mc2|Proline|ProPlus|Protest|PSV|Puma|Pusheen|Pustefix|QPlay|Queen mum|Quercetti|Quick|Quiksilver|Quut|R95th|Rags Industry|Raizzed|Rampage|Rastar|Ravensburger|Ravensburger|Razor|Ready2Robot|Recent Toys|Red Code|Red Rag|Reebok|Reef|Relaunch|Replay&Sons|Repos Production|Retour Jeans|Retour Shoes|Retro Roller|Revell|Rhombus|Ria Menorca|RIO Roller|River Woods|Roces|Rollplay|Rolly Toys|Rondinella|Rookie|Rookies|Room Seven|Rory's Story Cubes|Rose & Romeo|Rough Shoes|Roxy|Rubbabu|Rubber Duck|Rubo Toys|Ruby Brown|Rucanor|Rumbl|RunRunToys|Russell Athletic|Salty Dog|Sambro|Sanetta|Sanrio|Schjerning|Schleich|Schmidt|Schylling|Science 4 You|Street Called Madison|Scotch & Soda|Scratch|Screechers Wild|SES|Sesamstraat|Sevenoneseven|Sevi|SFR|Shiwi|Shoe Republic|Shoesme|Sigikid|Siku|Silk Clay|Silverlit|Silvian Heach|Simply for Kids|Sinner|Skechers|Skooter|Slackers|Slamm|Slammer|Sluban|Smartmax|SmarTrike|Snapper Rock|SOAK|Soccer Jeans Company|S.Oliver|Soy Luna|Space Cowboys|Space Scooter|Speedo|Spiderkites|Spider-Man|Spielstabil|Spin Master|SportX|Stabilo|Stamp|State Of Art Rookies|Steiff|Step2|Stephen Joseph Gifts|Stiga|Stone Island|Storksak|StreetSurfing|Studio 100|Studio Maison|Style Me Up!|Stylex|Suki|Summer fun|Summerplay|Summertime|Sunkid|Sunny|Superga|Superman|Supermom|Superrebel|Supertrash|Super Wings|Swim Essentials|SwimWays|Sylvanian Families|Tactic|Taille 0|Tanner|Tatiri|Teletubbies|Tempish|Ten Cate|Tender Toys|Test merk - t.b.v prod|Teva|The Dutch Design Bakery|The Future Is Ours|The Offbits|The Secret Life of Pets|The White Brand|ThinkFun|Thomas de Trein|Tiamo|Tidlo|Tiger Tribe|Tikiri|Timberland|Timberland Shoes|TOM|Tomina|Tommy Hilfiger|Tommy Hilfiger shoes|Tomy|Toppoint|Topwrite Kids|Totum|Toyrific|Toys Pure|Toystate|Traditional Garden Games|Transformers|Travis Design|Trefl|Tretorn|Trudi|Tumble 'N Dry|Tutti Frutti|Twickto|Twinlife|Twizz|Ty|TYGO & vito|Ubisoft|UGG|Unico|Unicorn|Unique|Unisa|Universal|University Games|Van Dijk Toys|Vans|Varta|V-Cube|VDM|Vilebrequin|Vingino|Vingino Eyewear|Vingino shoes|Volare|VTech|Wader|Waimea|Warmbat|Warmies|Waterworld|Waterzone|Wave Racers|Waytoplay|WE Fashion|Weible Knet|Welliebellies|Welly|White Goblin Games|Wicke|Wicked|Wiking|Wild|Wildfish|Wild Republic|Wild Shoes|Winning Moves|Wizards of the Coast|Wobbel|Woezel & Pip|Wooden City|Woolrich|Worlds Apart|Wrebbit|WWF|XIN YU Toys|Xootz|Xplorys|XQ Max|XS Feet|Yello|Yellow Cab|Yep Yep|Yookidoo|Zapf Creation|ZEBRA|Zecchino D oro|ZEE & ZO|Zero2Three|Z-Man Games|Zoomer|Zootjes|Zuru)]  Merk
│   │   ├── [Basiskleur, REQUIRED, LIST (Blauw|Groen|Rood|Zwart|Wit|Grijs|Roze|Offwhite|Oranje|Zilver|Bruin|Beige|Paars|Geel|Goud|Turquoise|Multicolor)]  Basiskleur
│   │   ├── [Leverancierskleur, REQUIRED, TEXT]  Leverancierskleur
│   │   ├── [Geslacht, REQUIRED, LIST (Jongen|Meisje|Unisex|Baby)]  Geslacht
│   │   ├── [Collectie, REQUIRED, LIST (2013|2014|2015|2016|2017|2018|2019|2020|basics|1999|2021)]  Collectie
│   │   ├── [Seizoen, REQUIRED, LIST (zomer|winter|all season|herfst)]  Seizoen
│   │   ├── [Variant groepnummer, REQUIRED, TEXT]  Variant groepnummer
│   │   ├── [Maat, REQUIRED, LIST (One Size Kids/Teens|One Size Baby)]  Maat
│   │   ├── [Leveranciersmaat, RECOMMENDED, TEXT]  Leveranciersmaat
│   │   ├── [Materiaalomschrijving, RECOMMENDED, TEXT]  Materiaalomschrijving
│   │   ├── [Materialen, RECOMMENDED, LIST_MULTIPLE_VALUES (acryl|alpaca|anders|angora|bamboe|chlorofibre|cupro|dons|elastan|hennep|inox|kasjmier|katoen|lamswol|leer|linnen|lurex|lycra|lyocell|merino|mesh|metaal|modal|mohair|nubuck|nylon|papier|polyamide|polyester|polypropyleen|polyurethaan|polyvinylchloride|ramie|rayon|resin|rubber|stro|suède|tactel|viscose|wax|wol|zijde|bont)]  Materialen
│   │   └── [Duurzaamheid, OPTIONAL, LIST (Global Organic Textile Standard 2|Organic Content Standard|Responsible Wool Standard2|Leather Working Group2|Forest Stewardship Council2|Fair Trade cotton2|Better Cotton Initiative2|OEKO-TEX Standard 100 2|Gemaakt van gerecycled materiaal2|Gemaakt van biologisch katoen2|Waarde bij de code2)]  Duurzaamheid
│   └── Children
│       └── [140]  Kinderen
│           └── Children
│               └── [1400]  Kinder bedding & lifestyle
│                   └── Children
│                       ├── [14000]  Bed
│                       │   └── Children
│                       │       ├── [140001]  Dekbedovertrek
│                       │       │   └── Attributes
│                       │       │       ├── [Instopstrook, OPTIONAL, LIST (Aangestikt|Dubbel doorlopend|Flessenhals|Klittenbamd|Rits- of knoopsluiting|Klittenband)]  Instopstrook
│                       │       │       ├── [Anti allergie, OPTIONAL, LIST (Ja|Nee)]  Anti allergie
│                       │       │       ├── [Geschikt voor, REQUIRED, LIST (Ledikant|Juniorbed|Wieg|Box)]  Geschikt voor
│                       │       │       └── [Maat, REQUIRED, LIST (40x80|40x90|50x65|50x70|50x100|50x110|50x120|55x110|55x120|60x120|60x130|60x140|60x150|70x130|70x140|70x150|70x160|70x190|70x200|75x100|80x190|80x200|90x190|90x200|100x135|100x150|100x135|100x150)]  Maat
│                       │       ├── [140002]  Deken
│                       │       │   └── Attributes
│                       │       │       ├── [Anti allergie, OPTIONAL, LIST (Ja|Nee)]  Anti allergie
│                       │       │       ├── [Geschikt voor, REQUIRED, LIST (Ledikant|Juniorbed|Wieg|Box)]  Geschikt voor
│                       │       │       └── [Maat, REQUIRED, LIST (40x80|40x90|50x65|50x70|50x100|50x110|50x120|55x110|55x120|60x120|60x130|60x140|60x150|70x130|70x140|70x150|70x160|70x190|70x200|75x100|80x190|80x200|90x190|90x200|100x135|100x150|100x135|100x150)]  Maat
│                       │       ├── [140003]  Aankleedkussenhoes
│                       │       │   └── Attributes
│                       │       │       └── [Maat, REQUIRED, LIST (40x80|40x90|50x65|50x70|50x100|50x110|50x120|55x110|55x120|60x120|60x130|60x140|60x150|70x130|70x140|70x150|70x160|70x190|70x200|75x100|80x190|80x200|90x190|90x200|100x135|100x150|100x135|100x150)]  Maat
│                       │       ├── [140004]  Laken
│                       │       │   └── Attributes
│                       │       │       ├── [Anti allergie, OPTIONAL, LIST (Ja|Nee)]  Anti allergie
│                       │       │       ├── [Geschikt voor, REQUIRED, LIST (Ledikant|Juniorbed|Wieg|Box)]  Geschikt voor
│                       │       │       └── [Maat, REQUIRED, LIST (40x80|40x90|50x65|50x70|50x100|50x110|50x120|55x110|55x120|60x120|60x130|60x140|60x150|70x130|70x140|70x150|70x160|70x190|70x200|75x100|80x190|80x200|90x190|90x200|100x135|100x150|100x135|100x150)]  Maat
│                       │       └── [140005]  Slaapzak
│                       │           └── Attributes
│                       │               ├── [Mouwlengte, OPTIONAL, LIST (lange mouw|korte mouw|mouwloos|3/4 mouw|7)]  Mouwlengte
│                       │               ├── [Seizoen, OPTIONAL, LIST (zomer|winter|all season|herfst)]  Seizoen
│                       │               ├── [Maat, REQUIRED, LIST (44|50|56|62|68|74|80|86|92|98|104|110|116|122|128|134|140|146|152|158|164|170|176|182|188|38-44|50-56|62-68|74-80|86-92|98-104|110-116|122-128|134-140|146-152|158-164|170-176|182-188|44-50|56-62|68-74|80-86|92-98|104-110|116-122|128-134|140-146|152-158|164-170|176-182|200)]  Maat
│                       │               └── [TOG Waarde, OPTIONAL, DECIMAL]  TOG Waarde
│                       └── [14001]  Eten en Drinken
│                           └── Children
│                               └── [140013]  Slabbetje
└── [17]  Speelgoed
    ├── Attributes
    │   ├── [Merk, OPTIONAL, LIST (2-Play|3Doodler|4M|80DBORIGINAL|999 games|Abbey Darts|Achoka|Acrobat|Active|adidas originals|adidas Performance|Adora|Aerobie|Aeronautica Militare|Airforce|Air Hogs|Ajax|Ambi Toys|American outfitters|America Today|AMIGO|Amleg|Amscan|Anagram|Androni|Angel Sports|Angel Toys|Angry Birds|Anker|Antony Morato|AO76|Aquabeads|Aquaplay|Armani|Armani shoes|Asmodee|Asterix & Obelix|Aurora|AutoStyle|AXI|Babeurre|Baby Alive|Baby Annabell|BABY born|Baby Dutch|Babyface|Baby & Kidz Banz|Babymel|Babyproof|Baby Toys|Bad Boys|Bakugan|Ballon|Bampidano|BanBao|Banpresto|Barbapapa|Barbara Farber|Barbie|Barts|Base Toys|Bauer|Baufix|Bayer|BBlocks|Bburago|Bebeboom|Beco|Beebielove|Be Kool|Beleduc|Bellaire|Bema|Bengh|Benson|BERG|Bergstein|Berjuan|Bess|Bestex|Bestway|Betula|BEX|Beyblade|BIColini|Big|Bigjigs|Bilibo|Billabong|Billieblush|Billybandit|BiOBUDDi|Birkenstock|Bjorn Borg|bla bla bla|Black Dragon|Blåkläder|Bloomingville|Blue Concept|Blueprint Collections|Blue Rebel|Blue System|Blutsbaby|B.Nosy|Bo-Bell|Bo Dean|Boeken|Bomba|Bonikka|Bonnie Doon|Boob Maternity|Boon|Born to be famous|Bor*z|BOSS|Boycot it|Brakkies|Brand New|Brandweerman Sam|Braqeez|Bratz|Brio|Britains|British Knights|Bruder|Brunotti|Bruynzeel|BS Toys|Bubble World|Bubblez|Bullboxer|Bull Boys|Bull´s|Bumper|Bunnies|byAstrup|Cakewalk|Calvin Klein|Caran d'Ache|Carbone|Carioca|Carpoint|Carrera|Cars Jeans|Cartamundi|Cartoon Network|Cartronic|Castorland|Catimini|Cause|Cavallaro|Chaos-and-Order|Chaya|Chicamala|Chicco|Childhome|Chillafish|Chuggington|CKS|Claesen's|Claire|Clementoni|Clic|Clicformers|Clics|Clown|Clown Games|Cobble Hill|Cobi|Collecta|Collonil|Comansi|Converse|Copa|Corolle|Cottonfield|Cougar|Craft Sensations|Crayola|Create It!|Creative|Creative Kids|Creotime|Crocodile Creek|Cruyff classics|Crystal Puzzle|Custo Growing|Czech Games Edition|Days of Wonder|DC Comics|Deltas|Denham|Denny Rose|Depesche|Desigual|Despicable Me|Develab|Dhink|Diakakis|Diamant Toys|Diamond Dotz|Dickies|DIDI|Diesel|Difuzed|Dino|Dirkje|Discraft|Disney|Disney Gold Collection|Dixon|Djeco|DKNY|Dobber|Doerak|Dolce Toys|Dolly Moda|Donic Schildkröt|Dragon Fly|Drystone|Ducati|Ducky Beau|Dummy merk|Eager Beaver|EB shoes|écoiffier|Eddie Pen|Eddy Toys|Edushape|Eeboo|Eichhorn|Eliane et Lena|Elle|Elliot|Emoji|Energie|Engelhart|Engino|Enigma|Enjoy Summer|Esprit|Est1842|Eurodisc|EverEarth|EXIT|Exploding Kittens|Fagus|Falca|FALKE|Famosa|Fashion4U|Fashion Angels|Fat Brain Toys|FC Barcelona|Feber|Fehn|Feuchtmann|Fila|Fimo|Fischertechnik|Fisher-Price|Five Nights at Freddy's|Flexo|Foam Clay|Folat|Freaks|Free and Easy|Fridolin|Fun|Funko|Gaastra|Gabbiano|Galt|Game On Sport|Gant|Gant Shoes|Garcia|Gattino|GB Eye|Gear2Play|Gearbox|Geddes & Gillmore|Geisha|Gel-A-Peel|GeoMag|Geosmart|Geoworld|Geox|Gerardo's Toys|Get & Go|Gibsons|Gigamic|Gioseppo|Giotto|Glamorous|Globber|Globos Nordic|Gobbledygook|Goki|Goliath|Gonher|Goodmark|Goula|Grafix|Gre|Green Toys|Gsus|Guess|Günther|Haba|Hanssop|Hape|Happy2play|Happy Baby|Happy Horse|Happy People|Harrows|Harry Potter|Hasbro|Hatley|Havaianas|Haza Original|Heelarious|Heimess|Heless|Hello Kitty|Helly Hansen|Hip|Holster Australia|Holztiger|Hoooked|Hörby Bruk|HORKA|Hot Wheels|Hound|House of Kids|HQ Kites|Hubelino|Hummel|Hupsakee|Huzzle|Icepeak|Iconx|iDance|Identity Games|iDO|Iello|Igor|IKKS|Illooms|IMC|Imps & Elfs|I'm Toy|Indian Blue Jeans|Infinity Nado|Injusa|Interline|Interstat|Intex|Invento|Ipanema|Italtrike|iWallz|Jack & Jones|Jackpot|Jake Fischer|Janod|Jansen Tilanus|JD Bug|Jeep|Jellycat|Jemini|Jill|Jochie|Joe Color|Joe Color|Johntoy|Joker Entertainment|Jonotoys|Jottum|Jouéco|Joy Toy|Jumbo|Jumini|Jumpking|Jurassic World|Just Beach|Just Games|Kamparo|Kanjers|Kapla|Kaporal|Karl Lagerfeld|Käthe Kruse|KBT|Kickers|Kidboxer|Kiddieland|Kiddo|Kidea|Kidid|Kid O|Kidorable|Kids At Work|Kidscase|Kidzface|Kids Fun|Kids Globe|KIDS ONLY|Kidzart|Kidzroom|Kidzzbelts|Kinderfeets|King|Kipling|kleertjes.com|Klein|K'NEX|Koeka|Koko Noko|KREUL|Krunk denim|Lacoste|La Franc|Lalaloopsy|LaQ|LCEE|Lckr|Learning Resources|Le Big|Le Chic|Le Chic shoes|Le Coq Sportif|LEGO|Lelli Kelly|Lelly|Lena|Let's Play|Levi's|LG-Imports|Libellud|Lief|Lifetime|Lifetime Games|Light Stax|Like Flo|Lili Gaufrette|Lilliputiens|Li'l Woodzeez|Little Buddy|Little Company|Little Dutch|Little Eleven Paris|Little Feet|Little Live Pets|Little Tikes|LMTD|LOL Surprise|Loom Twister|Looney Tunes|Love2wait|Lovely Lea|Ludonaute|Luminou|Lumo Stars|Luna|Lyle & Scott|Lyra|Madd Gear|Magformers|Magic the Gathering|Maileg|Maisto|Maliblue|Malinos|Mama Licious|Mamamemo|Marionette|Marlin|Maruti|Marvel|Marybel|Matchbox|Mattel|Mayoral|McGregor|Meccano|Megableu|Mega Bloks|Meiya & Alvin|Melissa & Doug|Me & My Monkey|Mercedes-Benz|Metal Earth|Me To You|Mexx|MGA|Mignolo|Mijn|Milky Kiss|Milly Mally|Mim-Pi|Mim-pi Shoes|Minecraft|Miniland|Minimize|Mini Mommy|Minions|Ministeck|Miraculous|Miss Grant|Miss Sixty|Mistral|Molo|Monchhichi|Mondo|Monster High|Moodstreet|Moon Glow|Moose Toys|Moschino|Moses|Move|Muchachomalo|Mudpuppy|Multiprint|Munich|Murphy & Nye|My Brand|My Little Baby|My Little Pony|Nail-A-Peel|Name It|Nanostad|Napapijri|Naturino|Navir|Nenuco|NERF|New Balance|New Classic Toys|New Port|nic|Nickelodeon|Nicotoy|Nijdam|Nijntje|Nike|Nikko|Ninco|Ninja|Nintendo|Nobell|Noble Collection|No Excess|Non-License|Nono|NOP|Noppies|Noppies Maternity|North Sails|No Tomatoes|Nova Carta|Num Noms|Merkloos|O'Chill|Oilily|Olang|O'Neill|Orange stars|Osprey|Ostheimer|Our Generation|Outdoor|Outdoor Play|Outfitters Nation|Out of the Blue|Oxxy|Pako|Paladone|Pampers|Panini|Pantofola d'Oro|Paradiso Toys|Parajumpers|Party Stars|Party Time|Patrizia Pepe|Paul Frank|Peaceable Kingdom|Pébéo|Pegaso|Pentel|Pepe Jeans|Peppa Pig|Pequetren|Perfect Petzzz|Peri Ponchos|Petit Bateau|Petit Collage|Petite Jolie|Petit Louie|Petit Sofie Schnoor|Petrol|Pexkids|Philos|Phoenix|Pick & Pack|Pinocchio|Pintoy|Pinypon|PIXIE CREW|Plan B Games|Play-Doh|Playforever|Playfun|Play & go|Playlife|PlayMais|PLAYMOBIL|Playshoes|Playskool|Playtime|Playwood|Please|Plenty Gifts|Plum|Plus-Plus|PMS|Poederbaas|Pointer|Pokémon|Polesie|Polly Pocket|Polo Ralph Lauren|Polo Ralph Lauren shoes|PolyM|Power Rangers|Powerslide|Prism|Procos|Professor Puzzle|Project Mc2|Proline|ProPlus|Protest|PSV|Puma|Pusheen|Pustefix|QPlay|Queen mum|Quercetti|Quick|Quiksilver|Quut|R95th|Rags Industry|Raizzed|Rampage|Rastar|Ravensburger|Ravensburger|Razor|Ready2Robot|Recent Toys|Red Code|Red Rag|Reebok|Reef|Relaunch|Replay&Sons|Repos Production|Retour Jeans|Retour Shoes|Retro Roller|Revell|Rhombus|Ria Menorca|RIO Roller|River Woods|Roces|Rollplay|Rolly Toys|Rondinella|Rookie|Rookies|Room Seven|Rory's Story Cubes|Rose & Romeo|Rough Shoes|Roxy|Rubbabu|Rubber Duck|Rubo Toys|Ruby Brown|Rucanor|Rumbl|RunRunToys|Russell Athletic|Salty Dog|Sambro|Sanetta|Sanrio|Schjerning|Schleich|Schmidt|Schylling|Science 4 You|Street Called Madison|Scotch & Soda|Scratch|Screechers Wild|SES|Sesamstraat|Sevenoneseven|Sevi|SFR|Shiwi|Shoe Republic|Shoesme|Sigikid|Siku|Silk Clay|Silverlit|Silvian Heach|Simply for Kids|Sinner|Skechers|Skooter|Slackers|Slamm|Slammer|Sluban|Smartmax|SmarTrike|Snapper Rock|SOAK|Soccer Jeans Company|S.Oliver|Soy Luna|Space Cowboys|Space Scooter|Speedo|Spiderkites|Spider-Man|Spielstabil|Spin Master|SportX|Stabilo|Stamp|State Of Art Rookies|Steiff|Step2|Stephen Joseph Gifts|Stiga|Stone Island|Storksak|StreetSurfing|Studio 100|Studio Maison|Style Me Up!|Stylex|Suki|Summer fun|Summerplay|Summertime|Sunkid|Sunny|Superga|Superman|Supermom|Superrebel|Supertrash|Super Wings|Swim Essentials|SwimWays|Sylvanian Families|Tactic|Taille 0|Tanner|Tatiri|Teletubbies|Tempish|Ten Cate|Tender Toys|Test merk - t.b.v prod|Teva|The Dutch Design Bakery|The Future Is Ours|The Offbits|The Secret Life of Pets|The White Brand|ThinkFun|Thomas de Trein|Tiamo|Tidlo|Tiger Tribe|Tikiri|Timberland|Timberland Shoes|TOM|Tomina|Tommy Hilfiger|Tommy Hilfiger shoes|Tomy|Toppoint|Topwrite Kids|Totum|Toyrific|Toys Pure|Toystate|Traditional Garden Games|Transformers|Travis Design|Trefl|Tretorn|Trudi|Tumble 'N Dry|Tutti Frutti|Twickto|Twinlife|Twizz|Ty|TYGO & vito|Ubisoft|UGG|Unico|Unicorn|Unique|Unisa|Universal|University Games|Van Dijk Toys|Vans|Varta|V-Cube|VDM|Vilebrequin|Vingino|Vingino Eyewear|Vingino shoes|Volare|VTech|Wader|Waimea|Warmbat|Warmies|Waterworld|Waterzone|Wave Racers|Waytoplay|WE Fashion|Weible Knet|Welliebellies|Welly|White Goblin Games|Wicke|Wicked|Wiking|Wild|Wildfish|Wild Republic|Wild Shoes|Winning Moves|Wizards of the Coast|Wobbel|Woezel & Pip|Wooden City|Woolrich|Worlds Apart|Wrebbit|WWF|XIN YU Toys|Xootz|Xplorys|XQ Max|XS Feet|Yello|Yellow Cab|Yep Yep|Yookidoo|Zapf Creation|ZEBRA|Zecchino D oro|ZEE & ZO|Zero2Three|Z-Man Games|Zoomer|Zootjes|Zuru)]  Merk
    │   ├── [Basiskleur, OPTIONAL, LIST (Blauw|Groen|Rood|Zwart|Wit|Grijs|Roze|Offwhite|Oranje|Zilver|Bruin|Beige|Paars|Geel|Goud|Turquoise|Multicolor)]  Basiskleur
    │   ├── [Leverancierskleur, OPTIONAL, TEXT]  Leverancierskleur
    │   ├── [Geslacht, OPTIONAL, LIST (Jongen|Meisje|Unisex|Baby)]  Geslacht
    │   ├── [Materialen, OPTIONAL, LIST_MULTIPLE_VALUES (Hout|Pluche|Karton|Plastic|EVA|Stof|Metaal|ABS|Acacia hout|Acryl|Acrylaat|Acrylverf|Aluminium|Badstof|Bamboe|Berkenhout|Beukenhout|Bio plastic|Biologisch katoen|Blik|Board|Canvas|Coating|Eikenhout|Elastaan|Elastiek|Essenhout|Fiber|Fleece|Foam|Gel|Glas|Glasvezel|Hardhout|Houtvezels|IJzer|Imitatieleer|Jersey|Katoen|Katoensatijn|Keramiek|Klei|Kunstleer|Kunststof|Kunstvezel|Kurk|Leer|Linnen|MDF|Melamine|Microvezel|Modal|Multiplex|Neodymium|Nylon|Papier|PEVA plastic|Piepschuim|Polyamide|Polycarbonaat|Polyester|Polyetheen|Polyethersulfon|Polykatoen|Polymeer|Polypropyleen|Polyurethaan|Porselein|PVC|Raffia|Rubber|RVS verchroomd|RVS|Satijn|Schapenwol|Schuim|Silicium|Siliconen|Sisal|Spaanplaat|Spandex|Staal|Steen|Stro|Synthetisch steen|Synthetisch|Textiel|Thermoplast|Tin|TPU|Velours|Verchroomd metaal|Verzilverd|Vilt|Vinyl|Viscose|Was|Wol|Zijde|Riet|Rotan)]  Materialen
    │   ├── [Gewicht, OPTIONAL, INTEGER]  Gewicht
    │   ├── [Opmerking, OPTIONAL, TEXT]  Opmerking
    │   ├── [Image URL, OPTIONAL, TEXT]  Image URL
    │   ├── [Minimum leeftijd in maanden, OPTIONAL, INTEGER]  Minimum leeftijd in maanden
    │   ├── [Maximum leeftijd in maanden, OPTIONAL, INTEGER]  Maximum leeftijd in maanden
    │   ├── [Minimum leeftijd in jaren, RECOMMENDED, INTEGER]  Minimum leeftijd in jaren
    │   ├── [Maximum leeftijd in jaren, RECOMMENDED, INTEGER]  Maximum leeftijd in jaren
    │   ├── [Artikelnummer leverancier, RECOMMENDED, TEXT]  Artikelnummer leverancier
    │   ├── [Serie, RECOMMENDED, LIST (Banbao boerderij|Banbao Duncan treasure|Banbao Hi-tech|Banbao Leger|Banbao Safari|Banbao Snoopy|Banbao Trendy City|Banbao Turbo Power|BiOBUDDI leren|Clementoni Clemmy Plus|Clementoni mechanica laboratorium|Cobi Small Army|Disney Princess|ENGINO ECO|ENGINO INVENTOR|Fisher-Price Wonder Makers|Geomag Kor|Geomag Magicube|Geomag Mechanics|Geomag Panels|Geomag Pro-L|Kid K"NEX|K"NEX Achtbanen|K"NEX Bouwsets|K"NEX Education|Laser Pegs Creatures|Laser Pegs Mission Mars|Laser Pegs Monster Rally|LEGO Alien Conquest|LEGO Angry Birds|LEGO Architecture|LEGO Art|LEGO Atlantis|LEGO Basic|LEGO Batman|LEGO Batman Movie|LEGO Bionicle|LEGO Boost|LEGO Brickheadz|LEGO Cars|LEGO Castle|LEGO Chima|LEGO City|LEGO Classic|LEGO Creator|LEGO Creator Expert|LEGO DC Super Hero Girls|LEGO Disney|LEGO DUPLO|LEGO Elves|LEGO Friends|LEGO Galaxy Squad|LEGO Harry Potter|LEGO Hero Factory|LEGO Hidden Site|LEGO Ideas|LEGO Juniors|LEGO Jurassic World|LEGO Kingdoms|LEGO Lone Range|LEGO Lord Of The Rings|LEGO Marvel Avengers|LEGO Mindstorms|LEGO Minecraft|LEGO Minifigures|LEGO Mixels|LEGO Monster Fighters|LEGO NEXO KNIGHTS|LEGO NINJAGO|LEGO Ninja Turtles|LEGO Overwatch|LEGO Pirates|LEGO Pirates Of the Caribbean|LEGO Racers|LEGO Scooby-Doo|LEGO Space Police|LEGO Speed Champions|LEGO Spider-Man|LEGO Star Wars|LEGO Stranger Things|LEGO Super Heroes|LEGO Super Mario|LEGO Technic|LEGO The Hobbit|LEGO The Movie|LEGO The Movie 2|LEGO Toy Story|LEGO Trolls|LEGO Ultra Agents|Magformers Basic|Magformers Behicle|Magformers My First|Matador Ki|Matador Klassik|Mega Bloks Barbie|Mega Bloks Call Of Duty|Mega Bloks First Builders|Mega Bloks Hot Wheels|Mega Bloks Minions|Mega Bloks World of Warcraft|Ninja Turtles|Quercetti Fantacolor|Quercetti Georello|Real Madrid|SmartMax My First|Super Wings|VTech Bla-Bla-Blocks|World of Warcraft)]  Serie
    │   ├── [Sport, OPTIONAL, LIST_MULTIPLE_VALUES (Voetbal|Tennis|Hockey|Skieën|Darts|Outdoor|Running|Fitness|Basketbal)]  Sport
    │   ├── [Duurzaamheid, OPTIONAL, LIST (Global Organic Textile Standard 2|Organic Content Standard|Responsible Wool Standard2|Leather Working Group2|Forest Stewardship Council2|Fair Trade cotton2|Better Cotton Initiative2|OEKO-TEX Standard 100 2|Gemaakt van gerecycled materiaal2|Gemaakt van biologisch katoen2|Waarde bij de code2)]  Duurzaamheid
    │   ├── [Jaar van uitgave, OPTIONAL, LIST (2021|2020|2019|2018|2017|2016|2015|2014|2013|2012|2011|2010|2009|2008|2007|2006|2005|2004|2003|2002|2001|2000|1999|1998|1997|1996|1995|1994|1993|1992|1991|1990)]  Jaar van uitgave
    │   └── [Minimum leeftijd in maanden, OPTIONAL, INTEGER]  Minimum leeftijd in maanden
    └── Children
        ├── [170]  Baby & Peuter
        │   └── Children
        │       ├── [1700]  Baby/Peuter Stimuleringsspeelgoed (elektrisch)
        │       │   ├── Attributes
        │       │   │   ├── [Lichteffecten, RECOMMENDED, LIST (Ja|Nee)]  Lichteffecten
        │       │   │   ├── [Liedjes/melodietjes, RECOMMENDED, LIST (Ja|Nee)]  Liedjes/melodietjes
        │       │   │   ├── [Volumeregeling, RECOMMENDED, LIST (Ja|Nee)]  Volumeregeling
        │       │   │   ├── [Timer, RECOMMENDED, LIST (Ja|Nee)]  Timer
        │       │   │   ├── [Natuurgeluiden, RECOMMENDED, LIST (Ja|Nee)]  Natuurgeluiden
        │       │   │   ├── [Huilsensor, RECOMMENDED, LIST (Ja|Nee)]  Huilsensor
        │       │   │   ├── [Afstandsbediening, RECOMMENDED, LIST (Ja|Nee)]  Afstandsbediening
        │       │   │   ├── [Voorleesverhaaltje, RECOMMENDED, LIST (Ja|Nee)]  Voorleesverhaaltje
        │       │   │   ├── [Bluetooth, RECOMMENDED, LIST (Ja|Nee)]  Bluetooth
        │       │   │   └── [Personage, OPTIONAL, LIST (101 dalmatiërs|Aladdin|Angry Birds|Ant-Man|Aquaman|Assassin's Creed|Assepoester|Asterix|Baby Annabell|Bakugan|Barbapapa|Barbie|Batman|Belle en het Beest|Big Hero 6|Black Widow|Blaze|Bob de Bouwer|Brandweerman Sam|Buurman & Buurman|Bratz|Bumba|Woezel en Pip|DC Super Hero Girls|De Kleine Zeemeermin|Disney|Disney Cars|Disney Fairies|Disney Frozen|Disney Princess|Donald Duck|Doornroosje|Dora the Explorer|Dr. Who|Dragon Ball Z|Elena van Avalor|Echantimals|Fallout|FIFA|Finding Dory|Fortnite|Game Of Thrones|God of War|Guardians of the Galaxy|Halo|Harley Quinn|Hannah Montana|Harry Potter|Hatchimals|Hello Kitty|Hoe Tem je een Draak|Hulk|Iron Man|James Bond|Jurassic World|K3|Kerst|Kikker|Kuifje|Kung Fu Panda|Lalaloopsy|Lion King|Little People|Looney Tunes|Lord of the Rings|Madagascar|Marvel|Masha en de Beer|Maya de Bij|Minecraft|Mickey Mouse|Minions|Minnie Mouse|Monster High|Moxie Girlz|My Little Pony|Nijntje|PAW Patrol|Peppa Pig|Peter Pan|Pickachu|Planes|Pokémon|Power Rangers|Prinses Sofia|Prinsessia|Rapunzel|Robin Hood|Roodkapje|Sesamstraat|Shimmer and Shine|Smurfen|Sneeuwwitje|Snoopy|Sofia het Prinsesje|Sonic|Spiderman|Spongebob Squarepants|Star Trek|Star Wars|Super Mario|Sunny Day|Supergirl|Superman|The Avengers|The Big Bang Theory|The Good Dinosaur|The Hulk|The Incredibles|The Joker|The Secret Live of Pets|The Simpsons|Thomas de Trein|Thor|Toy Story|Transformers|Tinkerbell|Trolls|Violetta|Winnie de Poeh|Winx|Wonder Woman|X-men|Zelda|Zootropolis|Astra|Bello|Chase|Dizzy|Donnie|Flip|Grand Albert|Jerome|Jett|Mira|Paul|Poppa WHeels|Todd|PJ Masks|Southpark|Teletubbies)]  Personage
        │       │   └── Children
        │       │       └── [170000]  Babyprojector
        │       │           └── Attributes
        │       │               └── [Maakt geluid, REQUIRED, LIST (Ja|Nee)]  Maakt geluid
        │       └── [1701]  Baby/Peuter Stimuleringsspeelgoed (niet-elektrisch)
        │           └── Children
        │               ├── [170100]  Activity-Center
        │               │   └── Attributes
        │               │       ├── [Type activity center, RECOMMENDED, LIST (Speelset|Paneel|Box|Kubus|Tafel|Rol|Toren)]  Type activity center
        │               │       └── [Maakt geluid, REQUIRED, LIST (Ja|Nee)]  Maakt geluid
        │               ├── [170101]  Activity-Gym
        │               │   └── Attributes
        │               │       ├── [Maakt geluid, REQUIRED, LIST (Ja|Nee)]  Maakt geluid
        │               │       ├── [Inclusief speelkleed, REQUIRED, LIST (Ja|Nee)]  Inclusief speelkleed
        │               │       └── [Aantal speelelementen, REQUIRED, INTEGER]  Aantal speelelementen
        │               ├── [170102]  Babyboekje
        │               ├── [170103]  Babymobile
        │               ├── [170104]  Knikkerbaan
        │               │   └── Attributes
        │               │       └── [Aantal onderdelen, RECOMMENDED, INTEGER]  Aantal onderdelen
        │               ├── [170105]  Looptrainer
        │               ├── [170106]  Speelkleed
        │               │   └── Attributes
        │               │       ├── [Lengte product in cm, REQUIRED, INTEGER]  Lengte product in cm
        │               │       └── [Breedte product in cm, REQUIRED, INTEGER]  Breedte product in cm
        │               ├── [170107]  Rammelaar
        │               │   └── Attributes
        │               │       ├── [Voedingstype, OPTIONAL, LIST_MULTIPLE_VALUES (Adapter/lichtnet|Batterij|Netstroom|Accu|USB)]  Voedingstype
        │               │       └── [Diersoort, OPTIONAL, LIST (Aap|Alpaca|Beer|Bever|Bij|Buideldier|Cavia|Das|Dinosaurus|Dolfijn|Draak|Eekhoorn|Eend|Eenhoorn|Egel|Ezel|Fantasiedier|Flamingo|Gans|Geit|Giraffe|Gordeldier|Haai|Haas|Hamster|Hert|Hond|Hyena|Ijsbeer|Inktvis|Insect|Jaguar|Kameel|Kameleon|Kangoeroe|Kat|Kikker|Kip|Koala|Koe|Konijn|Krokodil|Lama|Leeuw|Lieveheersbeestje|Luiaard|Luipaard|Mammoet|Miereneter|Mol|Muis|Neushoorn|Nijlpaard|Octopus|Olifant|Ooievaar|Orka|Otter|Paard|Panda|Panter|Papegaai|Pauw|Pinguin|Rat|Rendier|Rog|Rups|Schaap|Schildpad|Slak|Slang|Spin|Stinkdier|Stokstaartje|Struisvogel|Tapir|Tijger|Uil|Varken|Vis|Vleermuis|Vlinder|Vogel|Vos|Walvis|Wasbeer|Wezel|Wolf|Zebra|Zeedier|Zeehond|Zeepaardje|Zwijn|Zwaan)]  Diersoort
        │               ├── [170108]  Hamerbank
        │               ├── [170109]  Boxspiraal
        │               ├── [170110]  Trekfiguur
        │               │   └── Attributes
        │               │       └── [Diersoort, OPTIONAL, LIST (Aap|Alpaca|Beer|Bever|Bij|Buideldier|Cavia|Das|Dinosaurus|Dolfijn|Draak|Eekhoorn|Eend|Eenhoorn|Egel|Ezel|Fantasiedier|Flamingo|Gans|Geit|Giraffe|Gordeldier|Haai|Haas|Hamster|Hert|Hond|Hyena|Ijsbeer|Inktvis|Insect|Jaguar|Kameel|Kameleon|Kangoeroe|Kat|Kikker|Kip|Koala|Koe|Konijn|Krokodil|Lama|Leeuw|Lieveheersbeestje|Luiaard|Luipaard|Mammoet|Miereneter|Mol|Muis|Neushoorn|Nijlpaard|Octopus|Olifant|Ooievaar|Orka|Otter|Paard|Panda|Panter|Papegaai|Pauw|Pinguin|Rat|Rendier|Rog|Rups|Schaap|Schildpad|Slak|Slang|Spin|Stinkdier|Stokstaartje|Struisvogel|Tapir|Tijger|Uil|Varken|Vis|Vleermuis|Vlinder|Vogel|Vos|Walvis|Wasbeer|Wezel|Wolf|Zebra|Zeedier|Zeehond|Zeepaardje|Zwijn|Zwaan)]  Diersoort
        │               ├── [170111]  Kralenspiraal
        │               └── [170120]  Babydump
        ├── [171]  Bouwen
        │   └── Children
        │       └── [1710]  Blokken/Constructiespeelgoed
        │           ├── Attributes
        │           │   └── [Setnummer, OPTIONAL, INTEGER]  Setnummer
        │           └── Children
        │               ├── [171001]  Blokken
        │               │   └── Attributes
        │               │       ├── [Aantal stuks in verpakking, REQUIRED, INTEGER]  Aantal stuks in verpakking
        │               │       └── [Type blokken, REQUIRED, LIST (Blokkenset|Stapeltoren|Speelblok|Blokkenkar)]  Type blokken
        │               ├── [171002]  Constructiespeelgoed bouwplaat
        │               │   └── Attributes
        │               │       └── [Speelgoedthema, RECOMMENDED, LIST (Bouw|Brandweer|Circus|Cowboys en Indianen|Dieren|Dierentuin|Dinosaurussen|Draken|Fantasy|Film; TV en Muziek|Kermis|Kerst|Leger|Manege|Mode|Piraten|Politie|Prinsessen|Ridders|Robots|Ruimtevaart|Safari|School|Science fiction|Sport|Stadsleven|Vakantie|Ziekenhuis|Boerderij)]  Speelgoedthema
        │               ├── [171003]  Constructiespeelgoed bouwset
        │               │   └── Attributes
        │               │       ├── [Aantal onderdelen, RECOMMENDED, INTEGER]  Aantal onderdelen
        │               │       ├── [Speelgoedthema, RECOMMENDED, LIST (Bouw|Brandweer|Circus|Cowboys en Indianen|Dieren|Dierentuin|Dinosaurussen|Draken|Fantasy|Film; TV en Muziek|Kermis|Kerst|Leger|Manege|Mode|Piraten|Politie|Prinsessen|Ridders|Robots|Ruimtevaart|Safari|School|Science fiction|Sport|Stadsleven|Vakantie|Ziekenhuis|Boerderij)]  Speelgoedthema
        │               │       └── [Aantal figuren, OPTIONAL, INTEGER]  Aantal figuren
        │               ├── [171004]  Constructiespeelgoed bouwstenen
        │               │   └── Attributes
        │               │       └── [Aantal onderdelen, RECOMMENDED, INTEGER]  Aantal onderdelen
        │               └── [171009]  Regressie 14-09-20
        │                   └── Attributes
        │                       └── [Regressie 14-09, REQUIRED, TEXT]  Regressie 14-09
        ├── [172]  Buitenspeelgoed & Waterspeelgoed
        │   └── Children
        │       ├── [1720]  Buiten Speeltoestellen
        │       │   └── Children
        │       │       ├── [172000]  Duikelstang
        │       │       ├── [172001]  Glijbaan
        │       │       │   └── Attributes
        │       │       │       ├── [Lengte product in cm, REQUIRED, INTEGER]  Lengte product in cm
        │       │       │       └── [Type speeltoestel, REQUIRED, LIST (Aanbouw toestel|Volledig toestel)]  Type speeltoestel
        │       │       ├── [172002]  Klimrek
        │       │       ├── [172003]  Klimwand
        │       │       ├── [172004]  Jacuzzi
        │       │       ├── [172005]  Schommel
        │       │       │   └── Attributes
        │       │       │       ├── [Maximale belasting in kg, RECOMMENDED, INTEGER]  Maximale belasting in kg
        │       │       │       └── [Type schommelzitje, REQUIRED, LIST (Plankschommel|Nestschommel|Duoschommel|Peuterschommel|Babyschommel|Flexibele schommel|Meegroeischommel)]  Type schommelzitje
        │       │       ├── [172006]  Speelhuisje
        │       │       │   └── Attributes
        │       │       │       ├── [Glijbaan, OPTIONAL, LIST (Ja|Nee)]  Glijbaan
        │       │       │       ├── [Zandbak, OPTIONAL, LIST (Ja|Nee)]  Zandbak
        │       │       │       ├── [Meubels, OPTIONAL, LIST (Ja|Nee)]  Meubels
        │       │       │       ├── [Deurbel, OPTIONAL, LIST (Ja|Nee)]  Deurbel
        │       │       │       ├── [Schommel, OPTIONAL, LIST (Ja|Nee)]  Schommel
        │       │       │       └── [Plaatsing, OPTIONAL, LIST (Op de grond|Op palen|In-ground)]  Plaatsing
        │       │       ├── [172007]  Jacuzzifilter
        │       │       ├── [172008]  Speeltent
        │       │       │   └── Attributes
        │       │       │       ├── [Gesloten aan elke kant, RECOMMENDED, LIST (Ja|Nee)]  Gesloten aan elke kant
        │       │       │       ├── [Inclusief bodem, RECOMMENDED, LIST (Ja|Nee)]  Inclusief bodem
        │       │       │       └── [Type speeltent, REQUIRED, LIST (Tipi tent|Staande kindertent|Tafeltent|Speelhuistent|Ballenbaktent|Kindertent|Hangende tent)]  Type speeltent
        │       │       ├── [172009]  Speeltoestellencombinatie
        │       │       │   └── Attributes
        │       │       │       ├── [Glijbaan, OPTIONAL, LIST (Ja|Nee)]  Glijbaan
        │       │       │       ├── [Zandbak, OPTIONAL, LIST (Ja|Nee)]  Zandbak
        │       │       │       ├── [Schommel, OPTIONAL, LIST (Ja|Nee)]  Schommel
        │       │       │       ├── [Klimwand, OPTIONAL, LIST (Ja|Nee)]  Klimwand
        │       │       │       ├── [Klimtouw, OPTIONAL, LIST (Ja|Nee)]  Klimtouw
        │       │       │       ├── [Klimnet, OPTIONAL, LIST (Ja|Nee)]  Klimnet
        │       │       │       ├── [Loopbrug, OPTIONAL, LIST (Ja|Nee)]  Loopbrug
        │       │       │       ├── [Bandweerpaal, OPTIONAL, LIST (Ja|Nee)]  Bandweerpaal
        │       │       │       ├── [Rekstok, OPTIONAL, LIST (Ja|Nee)]  Rekstok
        │       │       │       ├── [Picknicktafel, OPTIONAL, LIST (Ja|Nee)]  Picknicktafel
        │       │       │       ├── [Lengte product in cm, REQUIRED, INTEGER]  Lengte product in cm
        │       │       │       ├── [Breedte product in cm, REQUIRED, INTEGER]  Breedte product in cm
        │       │       │       └── [Hoogte product in cm, REQUIRED, INTEGER]  Hoogte product in cm
        │       │       ├── [172010]  Speeltunnel
        │       │       ├── [172011]  Springkussen
        │       │       │   └── Attributes
        │       │       │       └── [Inclusief springkussenblower, OPTIONAL, LIST (Ja|Nee)]  Inclusief springkussenblower
        │       │       ├── [172012]  Trampoline
        │       │       │   └── Attributes
        │       │       │       ├── [Diameter in cm, REQUIRED, INTEGER]  Diameter in cm
        │       │       │       ├── [Vorm, RECOMMENDED, LIST (Rond|Rechthoek|Ovaal|Zeshoek|Vierkant)]  Vorm
        │       │       │       ├── [Maximale belasting in kg, REQUIRED, INTEGER]  Maximale belasting in kg
        │       │       │       └── [Plaatsing, OPTIONAL, LIST (Op de grond|Op palen|In-ground)]  Plaatsing
        │       │       ├── [172013]  Trapeze
        │       │       ├── [172014]  Wip
        │       │       ├── [172015]  Watertafel
        │       │       │   └── Attributes
        │       │       │       ├── [Lengte product in cm, REQUIRED, INTEGER]  Lengte product in cm
        │       │       │       ├── [Breedte product in cm, REQUIRED, INTEGER]  Breedte product in cm
        │       │       │       └── [Hoogte product in cm, REQUIRED, INTEGER]  Hoogte product in cm
        │       │       ├── [172016]  Zandbak
        │       │       ├── [172017]  Zwembad
        │       │       │   └── Attributes
        │       │       │       ├── [Maximale belasting in KG, REQUIRED, INTEGER]  Maximale belasting in KG
        │       │       │       ├── [Filterpomp, RECOMMENDED, LIST (Ja|Nee)]  Filterpomp
        │       │       │       ├── [Ladder, RECOMMENDED, LIST (Ja|Nee)]  Ladder
        │       │       │       ├── [Afdekzeil, RECOMMENDED, LIST (Ja|Nee)]  Afdekzeil
        │       │       │       ├── [Maximale belasting in kg, REQUIRED, INTEGER]  Maximale belasting in kg
        │       │       │       ├── [Filterpomp, RECOMMENDED, LIST (Ja|Nee)]  Filterpomp
        │       │       │       ├── [Ladder, RECOMMENDED, LIST (Ja|Nee)]  Ladder
        │       │       │       ├── [Afdekzeil, RECOMMENDED, LIST (Ja|Nee)]  Afdekzeil
        │       │       │       ├── [Grondzeil, RECOMMENDED, INTEGER]  Grondzeil
        │       │       │       └── [Doelgroep, REQUIRED, LIST (Familiebad|Kinderbad|Speelzwembad)]  Doelgroep
        │       │       ├── [172018]  Zwembad afdekzeil
        │       │       ├── [172019]  Zwembad grondzeil
        │       │       ├── [172020]  Zwembadfilterpomp
        │       │       │   └── Attributes
        │       │       │       └── [Voltage, RECOMMENDED, INTEGER]  Voltage
        │       │       ├── [172021]  Zwembadladder
        │       │       ├── [172022]  Zwembadonderhoud
        │       │       ├── [172023]  Zwembadreinigingsmiddel
        │       │       ├── [172024]  Zandbakzand
        │       │       │   └── Attributes
        │       │       │       ├── [Deksel, REQUIRED, LIST (Ja|Nee)]  Deksel
        │       │       │       ├── [Bodem, REQUIRED, LIST (Ja|Nee)]  Bodem
        │       │       │       └── [Zonwering, REQUIRED, LIST (Ja|Nee)]  Zonwering
        │       │       ├── [172025]  Kweektafel
        │       │       └── [172026]  Zandtafel
        │       ├── [1721]  Buiten/Tuin Speelgoed
        │       │   └── Children
        │       │       ├── [172100]  Springtouw (speelgoed)
        │       │       │   └── Attributes
        │       │       │       └── [Lengte product in cm, RECOMMENDED, INTEGER]  Lengte product in cm
        │       │       ├── [172101]  Ballenbak
        │       │       ├── [172102]  Ballenbakballen
        │       │       ├── [172103]  Beachballset
        │       │       ├── [172104]  Boemerang
        │       │       ├── [172105]  Buitenspeelgoedverkeersset
        │       │       ├── [172106]  Buitenspeelpakket
        │       │       ├── [172107]  Croquetspel
        │       │       ├── [172108]  Diabolo
        │       │       ├── [172109]  Duikspeelgoed
        │       │       ├── [172110]  Elastiekpistool
        │       │       ├── [172111]  Foamraket
        │       │       ├── [172112]  Frisbee
        │       │       ├── [172113]  Hinkelspel
        │       │       ├── [172114]  Jeu-de-boules
        │       │       ├── [172115]  Jongleervoorwerp
        │       │       ├── [172116]  Kegelspel
        │       │       ├── [172117]  Knikkerpot
        │       │       ├── [172118]  Knikkers
        │       │       ├── [172119]  Longboard
        │       │       ├── [172120]  Loopklossen
        │       │       ├── [172121]  Speelgoed pijl-en-boog
        │       │       ├── [172122]  Rolschaatsen
        │       │       │   └── Attributes
        │       │       │       └── [Type rolschaats, REQUIRED, LIST (Volledige rolschaatsen|Opzetrolschaatsen)]  Type rolschaats
        │       │       ├── [172123]  Schietspeelgoedmunitie
        │       │       ├── [172124]  Skimboard
        │       │       ├── [172125]  Skippybal
        │       │       ├── [172126]  Speelgoedbal
        │       │       ├── [172127]  Speelgoedblaster
        │       │       ├── [172128]  Speelgoedgieter
        │       │       ├── [172129]  Speelgoedgolfset
        │       │       ├── [172130]  Speelgoedpistool
        │       │       ├── [172131]  Speelgoedschepnet
        │       │       ├── [172132]  Speelgoedwatersproeier
        │       │       ├── [172133]  Springstok
        │       │       ├── [172134]  Stelten
        │       │       ├── [172135]  Stoepkrijt
        │       │       ├── [172136]  Stuiterbal
        │       │       ├── [172137]  Touwtrektouw
        │       │       │   └── Attributes
        │       │       │       └── [Lengte product in cm, RECOMMENDED, INTEGER]  Lengte product in cm
        │       │       ├── [172138]  Trekbal
        │       │       ├── [172139]  Vang- en werpspel
        │       │       ├── [172140]  Vlindernet
        │       │       ├── [172141]  Waterbaanaccessoire
        │       │       ├── [172142]  Waterballon
        │       │       ├── [172143]  Waterglijmat
        │       │       ├── [172144]  Waveboard
        │       │       ├── [172145]  Zakloopset
        │       │       ├── [172146]  Zand- en Watermolen
        │       │       ├── [172147]  Zandspeelset
        │       │       ├── [172148]  Zwembadspel
        │       │       ├── [172149]  Zwemtrainer
        │       │       ├── [172150]  Zwemvleugels
        │       │       ├── [172151]  Speelgoedkatapult
        │       │       ├── [172152]  Skateboard
        │       │       └── [172153]  Balanceboard
        │       ├── [1722]  Speelgoed/Speeltoestellen voor Buiten – Overig
        │       │   └── Children
        │       │       ├── [172201]  Basketbalstandaard
        │       │       ├── [172202]  Schommelhaak
        │       │       ├── [172203]  Schommeltouw
        │       │       ├── [172204]  Schommelverbindingsstuk
        │       │       ├── [172205]  Sleeaccessoire
        │       │       ├── [172206]  Sneeuwbalmaker
        │       │       ├── [172207]  Trampolinebeschermhoes
        │       │       │   └── Attributes
        │       │       │       ├── [Diameter in cm, REQUIRED, INTEGER]  Diameter in cm
        │       │       │       └── [Diameter in cm, REQUIRED, INTEGER]  Diameter in cm
        │       │       ├── [172208]  Trampoline-elastiek
        │       │       ├── [172209]  Trampolineframenet
        │       │       ├── [172210]  Trampoline-inbouwkit
        │       │       ├── [172211]  Trampolineladder
        │       │       │   └── Attributes
        │       │       │       └── [Lengte product in cm, REQUIRED, INTEGER]  Lengte product in cm
        │       │       ├── [172212]  Trampolineopbergvak
        │       │       ├── [172213]  Trampolinerand
        │       │       ├── [172214]  Trampolinespringmat
        │       │       ├── [172215]  Trampolinetent
        │       │       ├── [172216]  Trampolineveer
        │       │       │   └── Attributes
        │       │       │       └── [Lengte product in cm, REQUIRED, INTEGER]  Lengte product in cm
        │       │       ├── [172217]  Trampolineveiligheidsnet
        │       │       │   └── Attributes
        │       │       │       └── [Net met buizenframe, REQUIRED, LIST (Ja|Nee)]  Net met buizenframe
        │       │       ├── [172218]  Trampolineverankeringsset
        │       │       └── [172219]  Turnringen
        │       ├── [1723]  Fietsen
        │       │   └── Children
        │       │       ├── [172300]  Kinderfiets
        │       │       │   └── Attributes
        │       │       │       ├── [Wielmaat in cm, REQUIRED, INTEGER]  Wielmaat in cm
        │       │       │       ├── [Aantal versnellingen, REQUIRED, INTEGER]  Aantal versnellingen
        │       │       │       ├── [Wielmaat in inch, REQUIRED, INTEGER]  Wielmaat in inch
        │       │       │       ├── [Aantal versnellingen, REQUIRED, INTEGER]  Aantal versnellingen
        │       │       │       ├── [Type rem achter, REQUIRED, LIST (Terugtraprem|V-brake|Rollerbrake|Mechanische schijfrem|Trommelrem|Schijfremmen|Cantilever|Hydraulische velgrem|Hydraulische schrijfrem)]  Type rem achter
        │       │       │       ├── [Type rem voor, REQUIRED, LIST (Terugtraprem|V-brake|Rollerbrake|Mechanische schijfrem|Trommelrem|Schijfremmen|Cantilever|Hydraulische velgrem|Hydraulische schrijfrem)]  Type rem voor
        │       │       │       └── [Type aandrijving, OPTIONAL, LIST (Elektrisch|Benzine|Nitro)]  Type aandrijving
        │       │       ├── [172301]  Fietshelm
        │       │       │   └── Attributes
        │       │       │       ├── [Verstelbaar, RECOMMENDED, LIST (Ja|Nee)]  Verstelbaar
        │       │       │       ├── [met MIPS, RECOMMENDED, LIST (Ja|Nee)]  met MIPS
        │       │       │       ├── [Hoofdomtrek in cm, REQUIRED, INTEGER]  Hoofdomtrek in cm
        │       │       │       └── [Geschikt voor type fiets, RECOMMENDED, LIST_MULTIPLE_VALUES (Racefiets|Mountainbike|Kinderfiets|Stadsfiets|BMX|Recreatief|MTB)]  Geschikt voor type fiets
        │       │       ├── [172302]  Fietsverlichting
        │       │       │   └── Attributes
        │       │       │       ├── [Aantal lumen, RECOMMENDED, INTEGER]  Aantal lumen
        │       │       │       └── [Type fietsverlichting, REQUIRED, LIST (Achterlicht|Koplamp|Velichtingsset|Voorlicht|Lampje|Bevestigingsstrip|Stuurhouder|Accu|Voorlichthouder|Helmhouder|Achterlichthouder)]  Type fietsverlichting
        │       │       └── [172303]  Fietsaccessoires
        │       ├── [1724]  Waterspeelgoed voor Bad/Zwembad/Tuin
        │       │   └── Children
        │       │       ├── [172400]  Badeendje
        │       │       ├── [172401]  Badspeelfiguur
        │       │       ├── [172402]  Badspeelgoed
        │       │       ├── [172403]  Bellenblaas
        │       │       ├── [172404]  Luchtbed (waterspeelgoed)
        │       │       │   └── Attributes
        │       │       │       ├── [Lengte product in cm, REQUIRED, INTEGER]  Lengte product in cm
        │       │       │       └── [Breedte product in cm, REQUIRED, INTEGER]  Breedte product in cm
        │       │       ├── [172405]  Opblaasboot
        │       │       ├── [172406]  Opblaasfiguur
        │       │       │   └── Attributes
        │       │       │       ├── [Maximale belasting in kg, RECOMMENDED, INTEGER]  Maximale belasting in kg
        │       │       │       └── [Type opblaasfiguur, RECOMMENDED, LIST (Dier|Figuur|Voedsel|Unicorn|Voertuig|Zwaan|Meubel|Krokodil|Eiland|Instrument|Volleybal)]  Type opblaasfiguur
        │       │       ├── [172407]  Strandbal
        │       │       ├── [172408]  Waterbaan
        │       │       │   └── Attributes
        │       │       │       └── [Aantal speelelementen, REQUIRED, INTEGER]  Aantal speelelementen
        │       │       ├── [172409]  Waterpistool
        │       │       │   └── Attributes
        │       │       │       └── [Inhoud waterreservoir in ml, RECOMMENDED, INTEGER]  Inhoud waterreservoir in ml
        │       │       ├── [172410]  Zwemband
        │       │       │   └── Attributes
        │       │       │       └── [Type opblaasfiguur, OPTIONAL, LIST (Dier|Figuur|Voedsel|Unicorn|Voertuig|Zwaan|Meubel|Krokodil|Eiland|Instrument|Volleybal)]  Type opblaasfiguur
        │       │       ├── [172411]  Zwembandjes
        │       │       ├── [172412]  Waterglijbaan
        │       │       │   └── Attributes
        │       │       │       ├── [Lengte product in cm, REQUIRED, INTEGER]  Lengte product in cm
        │       │       │       └── [Type speeltoestel, REQUIRED, LIST (Aanbouw toestel|Volledig toestel)]  Type speeltoestel
        │       │       └── [172413]  Water speeltegels
        │       └── [1725]  Sportspeelgoed
        │           └── Children
        │               ├── [172500]  Voetbaldoel
        │               ├── [172501]  Hockeydoel
        │               ├── [172502]  Voetbal
        │               ├── [172503]  Rebounder
        │               ├── [172504]  Pannaveld
        │               ├── [172505]  Backstop sportnet
        │               ├── [172506]  Multisportnet
        │               └── [172507]  Volleybal
        │                   └── Attributes
        │                       └── [Type opblaasfiguur, OPTIONAL, LIST (Dier|Figuur|Voedsel|Unicorn|Voertuig|Zwaan|Meubel|Krokodil|Eiland|Instrument|Volleybal)]  Type opblaasfiguur
        ├── [173]  Creatief Speelgoed & Educatief speelgoed
        │   └── Children
        │       ├── [1730]  Communicatiespeelgoed (elektrisch)
        │       │   ├── Attributes
        │       │   │   └── [Leerthema, RECOMMENDED, LIST (Taal|Leren lezen|Techniek|Leren rekenen|Leren schrijven|Aardrijkskunde|Natuurkunde|Biologie|Scheikunde|Anatomie|Geschiedenis|Muziek|Algemene kennis|Cognitieve ontwikkeling|Motoriek|Programmeren|Ruimtelijk inzicht|Klokkijken|Archeologie)]  Leerthema
        │       │   └── Children
        │       │       ├── [173001]  Kindercomputer
        │       │       ├── [173002]  Kindercamera
        │       │       │   └── Attributes
        │       │       │       └── [Aantal megapixels, RECOMMENDED, INTEGER]  Aantal megapixels
        │       │       ├── [173003]  Kindertablet
        │       │       ├── [173004]  Leercomputer
        │       │       ├── [173005]  Leercomputeraccessoire
        │       │       ├── [173006]  Leercomputergame
        │       │       ├── [173007]  Kindercameratas
        │       │       ├── [173008]  Speelgoedmobieltje
        │       │       ├── [173009]  Speelgoedwalkietalkie
        │       │       │   └── Attributes
        │       │       │       ├── [Bereik in meters, REQUIRED, INTEGER]  Bereik in meters
        │       │       │       ├── [Aantal meegeleverde handsets, REQUIRED, INTEGER]  Aantal meegeleverde handsets
        │       │       │       ├── [Batterij nodig, REQUIRED, LIST (Ja|Nee)]  Batterij nodig
        │       │       │       └── [Batterij meegeleverd, REQUIRED, LIST (Ja|Nee)]  Batterij meegeleverd
        │       │       └── [173010]  Babytelefoon
        │       ├── [1731]  Speelgoedtekenborden – Accessoires
        │       │   └── Children
        │       │       ├── [173100]  Magneetbord
        │       │       ├── [173101]  Whiteboard
        │       │       │   └── Attributes
        │       │       │       ├── [Lengte product in cm, REQUIRED, INTEGER]  Lengte product in cm
        │       │       │       └── [Breedte product in cm, REQUIRED, INTEGER]  Breedte product in cm
        │       │       ├── [173102]  Whiteboard stift
        │       │       ├── [173103]  Krijtbord
        │       │       │   └── Attributes
        │       │       │       ├── [Lengte product in cm, REQUIRED, INTEGER]  Lengte product in cm
        │       │       │       └── [Breedte product in cm, REQUIRED, INTEGER]  Breedte product in cm
        │       │       └── [173104]  Tekenbord
        │       ├── [1732]  Tollen/Jojo's
        │       │   └── Children
        │       │       ├── [173201]  Jojo
        │       │       └── [173202]  Tol
        │       ├── [1733]  Duw/Trek Speelgoed (niet-elektrisch)
        │       │   └── Children
        │       │       └── [173300]  Loopwagen
        │       ├── [1734]  Ontwikkelings/Educatief Speelgoed – Assortimenten
        │       │   ├── Attributes
        │       │   │   └── [Leerthema, REQUIRED, LIST (Taal|Leren lezen|Techniek|Leren rekenen|Leren schrijven|Aardrijkskunde|Natuurkunde|Biologie|Scheikunde|Anatomie|Geschiedenis|Muziek|Algemene kennis|Cognitieve ontwikkeling|Motoriek|Programmeren|Ruimtelijk inzicht|Klokkijken|Archeologie)]  Leerthema
        │       │   └── Children
        │       │       ├── [173400]  Educatief spel
        │       │       │   └── Attributes
        │       │       │       ├── [Gemiddelde speeltijd in min, REQUIRED, INTEGER]  Gemiddelde speeltijd in min
        │       │       │       ├── [Minimum aantal spelers, REQUIRED, INTEGER]  Minimum aantal spelers
        │       │       │       ├── [Maximum aantal spelers, REQUIRED, INTEGER]  Maximum aantal spelers
        │       │       │       └── [Editie, REQUIRED, LIST (Standaard editie|Luxe uitgave|Uitbreiding|Licentie uitgave|Pocketspel|Internationale versie)]  Editie
        │       │       ├── [173401]  Experimenteerset
        │       │       ├── [173402]  Leerklok
        │       │       │   └── Attributes
        │       │       │       └── [Type Leerklok, REQUIRED, LIST (Analoge klok|Kalenderklok|Interactieve klok|Digitale klok)]  Type Leerklok
        │       │       ├── [173403]  Leersysteem
        │       │       │   └── Attributes
        │       │       │       └── [Maakt geluid, REQUIRED, LIST (Ja|Nee)]  Maakt geluid
        │       │       └── [173404]  Leersysteemuitbreiding
        │       ├── [1735]  Wetenschappelijk Speelgoed (niet-elektrisch)
        │       │   ├── Attributes
        │       │   │   └── [Leerthema, RECOMMENDED, LIST (Taal|Leren lezen|Techniek|Leren rekenen|Leren schrijven|Aardrijkskunde|Natuurkunde|Biologie|Scheikunde|Anatomie|Geschiedenis|Muziek|Algemene kennis|Cognitieve ontwikkeling|Motoriek|Programmeren|Ruimtelijk inzicht|Klokkijken|Archeologie)]  Leerthema
        │       │   └── Children
        │       │       ├── [173500]  Goocheldoos
        │       │       ├── [173501]  Telraam
        │       │       ├── [173502]  Vormenstoof
        │       │       ├── [173503]  Wereldbol
        │       │       │   └── Attributes
        │       │       │       └── [Diameter in cm, REQUIRED, INTEGER]  Diameter in cm
        │       │       ├── [173504]  Wereldkaart
        │       │       │   └── Attributes
        │       │       │       ├── [Lengte product in cm, REQUIRED, INTEGER]  Lengte product in cm
        │       │       │       ├── [Breedte product in cm, REQUIRED, INTEGER]  Breedte product in cm
        │       │       │       ├── [Inclusief lijst, REQUIRED, LIST (Ja|Nee)]  Inclusief lijst
        │       │       │       └── [Type wereldkaart, REQUIRED, LIST (Scratch map|Poster|Prikbord)]  Type wereldkaart
        │       │       ├── [173505]  Kindermicroscoop
        │       │       ├── [173506]  Kindertelescoop
        │       │       │   └── Attributes
        │       │       │       └── [Type telescoop, REQUIRED, LIST (Lenzentelescoop|Spiegeltelescoop)]  Type telescoop
        │       │       └── [173507]  Kinderverrekijker
        │       ├── [1736]  Knutselartikelen
        │       │   └── Children
        │       │       ├── [173600]  Schaar
        │       │       │   └── Attributes
        │       │       │       └── [Rechts of linkshandig, REQUIRED, LIST (Rechtshandig|Linkshandig)]  Rechts of linkshandig
        │       │       ├── [173601]  Hobbylijm
        │       │       │   └── Attributes
        │       │       │       └── [Inhoud in ml, REQUIRED, INTEGER]  Inhoud in ml
        │       │       ├── [173602]  Plakband
        │       │       │   └── Attributes
        │       │       │       └── [Aantal stuks in verpakking, REQUIRED, INTEGER]  Aantal stuks in verpakking
        │       │       ├── [173603]  Knutselpakket
        │       │       ├── [173604]  Hobbypapier
        │       │       ├── [173605]  Hobbykarton
        │       │       ├── [173606]  Speelzand
        │       │       ├── [173607]  Speelmais
        │       │       ├── [173608]  Slijm
        │       │       │   └── Attributes
        │       │       │       ├── [Inhoud in gram, REQUIRED, INTEGER]  Inhoud in gram
        │       │       │       ├── [Magisch slijm, OPTIONAL, LIST (Ja|Nee)]  Magisch slijm
        │       │       │       └── [Type slijm, REQUIRED, LIST (Kant en klaar slijm|Zelf slijm maken)]  Type slijm
        │       │       ├── [173609]  Gips
        │       │       ├── [173610]  Scrapbookpakket
        │       │       ├── [173611]  Papierpons
        │       │       ├── [173612]  Stempelset
        │       │       ├── [173613]  Stempel
        │       │       ├── [173614]  Sticker
        │       │       ├── [173615]  Washi tape
        │       │       ├── [173616]  Hobbykralen
        │       │       │   └── Attributes
        │       │       │       └── [Type kraal, REQUIRED, LIST (Houten kralen|Zoetwaterparels|Acryl kralen|Glaskralen|Natuursteen kralen|Facetkralen|Figuurkralen|Glasparels|Tussenkralen|3D  kralen|Fimo kralen)]  Type kraal
        │       │       ├── [173617]  Strijkkralen
        │       │       │   └── Attributes
        │       │       │       └── [Type strijkkraal, REQUIRED, LIST (Normale strijkkralen|Maxi strijkkralen|Mini strijkkralen)]  Type strijkkraal
        │       │       └── [173618]  Waterkralen
        │       ├── [1737]  Schilderbenodigdheden
        │       │   └── Children
        │       │       ├── [173700]  Verf
        │       │       │   └── Attributes
        │       │       │       └── [Type verf, REQUIRED, LIST (Acrylverf|Olieverf|Aquarelverf|Spuitverf|Textielverf|Glas/ Porselein verf|Plakkaatverf|Waterverf|3D liner|Metaalverf|Glitterverf|Verfstift|Ecoline|Schoolbordverf)]  Type verf
        │       │       ├── [173701]  Hobbykwast
        │       │       ├── [173702]  Hobbypenseel
        │       │       ├── [173703]  Schilderpakket
        │       │       ├── [173704]  Schilderdoek
        │       │       ├── [173705]  Schilderspalet
        │       │       ├── [173706]  Schildersezel
        │       │       └── [173707]  Schilderen op nummer
        │       ├── [1738]  Teken-en kleurbenodigdheden
        │       │   └── Children
        │       │       ├── [173800]  Kneedgum
        │       │       ├── [173801]  Kleurboek
        │       │       ├── [173802]  Schetsboek
        │       │       ├── [173803]  Schoolbordkrijt
        │       │       ├── [173804]  Waskrijt
        │       │       ├── [173805]  Tekenhoutskool
        │       │       ├── [173806]  Potlood
        │       │       │   └── Attributes
        │       │       │       └── [Aantal stuks in verpakking, REQUIRED, INTEGER]  Aantal stuks in verpakking
        │       │       ├── [173807]  Kleurpotlood
        │       │       │   └── Attributes
        │       │       │       └── [Potlood hardheid, OPTIONAL, LIST (H|B|HB|F)]  Potlood hardheid
        │       │       ├── [173808]  Puntenslijper
        │       │       ├── [173809]  Hobbysjabloon
        │       │       ├── [173810]  Viltstift
        │       │       │   └── Attributes
        │       │       │       └── [Aantal stuks in verpakking, REQUIRED, INTEGER]  Aantal stuks in verpakking
        │       │       ├── [173811]  Verfstift
        │       │       │   └── Attributes
        │       │       │       └── [Aantal stuks in verpakking, REQUIRED, INTEGER]  Aantal stuks in verpakking
        │       │       ├── [173812]  Highlighter
        │       │       │   └── Attributes
        │       │       │       └── [Aantal stuks in verpakking, REQUIRED, INTEGER]  Aantal stuks in verpakking
        │       │       ├── [173813]  Brushpen
        │       │       │   └── Attributes
        │       │       │       └── [Aantal stuks in verpakking, REQUIRED, INTEGER]  Aantal stuks in verpakking
        │       │       ├── [173814]  Gelpen
        │       │       │   └── Attributes
        │       │       │       └── [Aantal stuks in verpakking, REQUIRED, INTEGER]  Aantal stuks in verpakking
        │       │       ├── [173815]  Kalligrafiepen
        │       │       │   └── Attributes
        │       │       │       └── [Aantal stuks in verpakking, REQUIRED, INTEGER]  Aantal stuks in verpakking
        │       │       ├── [173816]  Schrijfinkt
        │       │       │   └── Attributes
        │       │       │       └── [Inhoud in ml, REQUIRED, INTEGER]  Inhoud in ml
        │       │       ├── [173817]  Tekendoos
        │       │       └── [173818]  Tekenpakket
        │       └── [1739]  Klei
        │           └── Children
        │               ├── [173900]  Boetseerklei
        │               │   └── Attributes
        │               │       └── [Type klei, REQUIRED, LIST (Zelfhardende klei|Bakklei|Schoolklei|Kunststofklei)]  Type klei
        │               ├── [173901]  Boetseergereedschap
        │               ├── [173902]  Boetseerpakket
        │               └── [173903]  Speelklei
        │                   └── Attributes
        │                       └── [Type klei, REQUIRED, LIST (Zelfhardende klei|Bakklei|Schoolklei|Kunststofklei)]  Type klei
        ├── [174]  Muzikaal Speelgoed
        │   ├── Attributes
        │   │   └── [Speelgoedthema, OPTIONAL, LIST (Bouw|Brandweer|Circus|Cowboys en Indianen|Dieren|Dierentuin|Dinosaurussen|Draken|Fantasy|Film; TV en Muziek|Kermis|Kerst|Leger|Manege|Mode|Piraten|Politie|Prinsessen|Ridders|Robots|Ruimtevaart|Safari|School|Science fiction|Sport|Stadsleven|Vakantie|Ziekenhuis|Boerderij)]  Speelgoedthema
        │   └── Children
        │       ├── [1740]  Muzikaal Speelgoed – Overig
        │       │   └── Children
        │       │       ├── [174000]  Bladmuziekstandaard
        │       │       └── [174001]  Muziekdoosje
        │       ├── [1741]  Muzikaal Speelgoed (elektrisch)
        │       │   └── Children
        │       │       ├── [174100]  Speelgoedaccordeon
        │       │       ├── [174101]  Dansmat
        │       │       ├── [174102]  Speelgoedkaraokeset
        │       │       │   └── Attributes
        │       │       │       ├── [Draagbaar, OPTIONAL, LIST (Ja|Nee)]  Draagbaar
        │       │       │       ├── [Lichteffecten, OPTIONAL, LIST (Ja|Nee)]  Lichteffecten
        │       │       │       ├── [Externe muziek aan te sluiten, OPTIONAL, LIST (Ja|Nee)]  Externe muziek aan te sluiten
        │       │       │       ├── [Echo effect, OPTIONAL, LIST (Ja|Nee)]  Echo effect
        │       │       │       ├── [Inclusief karoakeliedjes, OPTIONAL, LIST (Ja|Nee)]  Inclusief karoakeliedjes
        │       │       │       ├── [Geluidseffecten, OPTIONAL, LIST (Ja|Nee)]  Geluidseffecten
        │       │       │       ├── [Bedrade microfoon, OPTIONAL, LIST (Ja|Nee)]  Bedrade microfoon
        │       │       │       ├── [Draadloze microfoon, OPTIONAL, LIST (Ja|Nee)]  Draadloze microfoon
        │       │       │       ├── [Inclusief CD speler, OPTIONAL, LIST (Ja|Nee)]  Inclusief CD speler
        │       │       │       └── [Voedingstype, REQUIRED, LIST_MULTIPLE_VALUES (Adapter/lichtnet|Batterij|Netstroom|Accu|USB)]  Voedingstype
        │       │       ├── [174103]  Speelgoedkeyboard
        │       │       ├── [174104]  Speelgoedmuziekspeler
        │       │       └── [174105]  Speelgoedmicrofoon
        │       │           └── Attributes
        │       │               ├── [Lichteffecten, OPTIONAL, LIST (Ja|Nee)]  Lichteffecten
        │       │               ├── [Echo effect, OPTIONAL, LIST (Ja|Nee)]  Echo effect
        │       │               ├── [Geluidseffecten, OPTIONAL, LIST (Ja|Nee)]  Geluidseffecten
        │       │               ├── [Voorgeprogrammeerde liedjes, OPTIONAL, LIST (Ja|Nee)]  Voorgeprogrammeerde liedjes
        │       │               ├── [Versterking stem, OPTIONAL, LIST (Ja|Nee)]  Versterking stem
        │       │               ├── [Aansluiten op telefoon/ mp3 speler, OPTIONAL, LIST (Ja|Nee)]  Aansluiten op telefoon/ mp3 speler
        │       │               ├── [Aan/uit schakelaar, OPTIONAL, LIST (Ja|Nee)]  Aan/uit schakelaar
        │       │               ├── [Inclusief kabel, OPTIONAL, LIST (Ja|Nee)]  Inclusief kabel
        │       │               └── [Opnamefunctie, OPTIONAL, LIST (Ja|Nee)]  Opnamefunctie
        │       └── [1742]  Muzikaal Speelgoed (niet-elektrisch)
        │           └── Children
        │               ├── [174200]  Speelgoedbanjo
        │               ├── [174201]  Speelgoedbekken
        │               ├── [174202]  Speelgoedblokfluit
        │               ├── [174203]  Speelgoedbongo
        │               ├── [174204]  Speelgoedcastagnetten
        │               ├── [174205]  Speelgoedcello
        │               ├── [174206]  Speelgoeddidgeridoo
        │               ├── [174207]  Speelgoeddjembe
        │               ├── [174208]  Speelgoeddoedelzak
        │               ├── [174209]  Speelgoeddrum
        │               ├── [174210]  Speelgoeddrumbox
        │               ├── [174211]  Speelgoeddrumstel
        │               ├── [174212]  Speelgoeddwarsfluit
        │               ├── [174213]  Speelgoedfagot
        │               ├── [174214]  Speelgoedfluit
        │               ├── [174215]  Speelgoedgitaar
        │               ├── [174216]  Speelgoedgong
        │               ├── [174217]  Speelgoedharp
        │               ├── [174218]  Speelgoedhobo
        │               ├── [174219]  Speelgoedhoorn
        │               ├── [174220]  Speelgoedklarinet
        │               ├── [174221]  Speelgoedmandoline (muziek)
        │               ├── [174222]  Speelgoedmaracassen
        │               ├── [174223]  Speelgoedmelodium
        │               ├── [174224]  Speelgoedmondharmonica
        │               ├── [174225]  Speelgoedmuziekdoos
        │               ├── [174226]  Speelgoedmuziekset
        │               ├── [174227]  Speelgoedpanfluit
        │               ├── [174228]  Speelgoedpauk
        │               ├── [174229]  Speelgoedpiano
        │               ├── [174230]  Speelgoedsamba
        │               ├── [174231]  Speelgoedsaxofoon
        │               ├── [174232]  Speelgoedtamboerijn
        │               ├── [174233]  Speelgoedtrombone
        │               ├── [174234]  Speelgoedtrommel
        │               ├── [174235]  Speelgoedtrompet
        │               ├── [174236]  Speelgoedtuba
        │               ├── [174237]  Speelgoedukulele
        │               ├── [174238]  Speelgoedviool
        │               ├── [174239]  Speelgoedwoodblock
        │               └── [174240]  Speelgoedxylofoon
        ├── [176]  Poppen & Pluchen speelgoed
        │   └── Children
        │       ├── [1760]  Accessoires voor Poppen/Handpoppen/Pluchen Speelgoed – Overig
        │       │   └── Children
        │       │       ├── [176000]  Babypoppendier
        │       │       ├── [176001]  Babypoppenvervoersmiddel
        │       │       ├── [176002]  Babypoppenverzorgingsproduct
        │       │       ├── [176003]  Babypoppenwagen
        │       │       │   └── Attributes
        │       │       │       ├── [Lengte product in cm, REQUIRED, INTEGER]  Lengte product in cm
        │       │       │       ├── [Breedte product in cm, REQUIRED, INTEGER]  Breedte product in cm
        │       │       │       └── [Hoogte product in cm, REQUIRED, INTEGER]  Hoogte product in cm
        │       │       ├── [176004]  Modepoppendier
        │       │       ├── [176005]  Modepoppenvervoersmiddel
        │       │       └── [176006]  Modepoppenverzorgingsproduct
        │       ├── [1761]  Handpoppen/Poppenkastpoppen
        │       │   └── Children
        │       │       ├── [176100]  Handpop
        │       │       └── [176101]  Poppenkastpop
        │       ├── [1762]  Poppen Gebouwen/Omgevingen
        │       │   └── Children
        │       │       └── [176200]  Poppenhuis
        │       │           └── Attributes
        │       │               ├── [Lengte product in cm, REQUIRED, INTEGER]  Lengte product in cm
        │       │               ├── [Breedte product in cm, REQUIRED, INTEGER]  Breedte product in cm
        │       │               ├── [Hoogte product in cm, REQUIRED, INTEGER]  Hoogte product in cm
        │       │               ├── [Inclusief meubels, RECOMMENDED, LIST (Ja|Nee)]  Inclusief meubels
        │       │               ├── [Inclusief poppen, RECOMMENDED, LIST (Ja|Nee)]  Inclusief poppen
        │       │               ├── [Geschikt voor poppenhuispop, RECOMMENDED, LIST (Ja|Nee)]  Geschikt voor poppenhuispop
        │       │               ├── [Geschikt voor Speelfiguur, RECOMMENDED, LIST (Ja|Nee)]  Geschikt voor Speelfiguur
        │       │               ├── [Geschikt voor Modepop, RECOMMENDED, LIST (Ja|Nee)]  Geschikt voor Modepop
        │       │               └── [Geschikt voor Babypop, RECOMMENDED, LIST (Ja|Nee)]  Geschikt voor Babypop
        │       ├── [1763]  Poppen/Speelfiguren (niet-elektrisch)
        │       │   ├── Attributes
        │       │   │   └── [Personage, OPTIONAL, LIST (101 dalmatiërs|Aladdin|Angry Birds|Ant-Man|Aquaman|Assassin's Creed|Assepoester|Asterix|Baby Annabell|Bakugan|Barbapapa|Barbie|Batman|Belle en het Beest|Big Hero 6|Black Widow|Blaze|Bob de Bouwer|Brandweerman Sam|Buurman & Buurman|Bratz|Bumba|Woezel en Pip|DC Super Hero Girls|De Kleine Zeemeermin|Disney|Disney Cars|Disney Fairies|Disney Frozen|Disney Princess|Donald Duck|Doornroosje|Dora the Explorer|Dr. Who|Dragon Ball Z|Elena van Avalor|Echantimals|Fallout|FIFA|Finding Dory|Fortnite|Game Of Thrones|God of War|Guardians of the Galaxy|Halo|Harley Quinn|Hannah Montana|Harry Potter|Hatchimals|Hello Kitty|Hoe Tem je een Draak|Hulk|Iron Man|James Bond|Jurassic World|K3|Kerst|Kikker|Kuifje|Kung Fu Panda|Lalaloopsy|Lion King|Little People|Looney Tunes|Lord of the Rings|Madagascar|Marvel|Masha en de Beer|Maya de Bij|Minecraft|Mickey Mouse|Minions|Minnie Mouse|Monster High|Moxie Girlz|My Little Pony|Nijntje|PAW Patrol|Peppa Pig|Peter Pan|Pickachu|Planes|Pokémon|Power Rangers|Prinses Sofia|Prinsessia|Rapunzel|Robin Hood|Roodkapje|Sesamstraat|Shimmer and Shine|Smurfen|Sneeuwwitje|Snoopy|Sofia het Prinsesje|Sonic|Spiderman|Spongebob Squarepants|Star Trek|Star Wars|Super Mario|Sunny Day|Supergirl|Superman|The Avengers|The Big Bang Theory|The Good Dinosaur|The Hulk|The Incredibles|The Joker|The Secret Live of Pets|The Simpsons|Thomas de Trein|Thor|Toy Story|Transformers|Tinkerbell|Trolls|Violetta|Winnie de Poeh|Winx|Wonder Woman|X-men|Zelda|Zootropolis|Astra|Bello|Chase|Dizzy|Donnie|Flip|Grand Albert|Jerome|Jett|Mira|Paul|Poppa WHeels|Todd|PJ Masks|Southpark|Teletubbies)]  Personage
        │       │   └── Children
        │       │       ├── [176300]  Actiefiguur
        │       │       ├── [176301]  Babypop
        │       │       │   └── Attributes
        │       │       │       ├── [Lengte product in cm, RECOMMENDED, INTEGER]  Lengte product in cm
        │       │       │       ├── [Maakt geluid, OPTIONAL, LIST (Ja|Nee)]  Maakt geluid
        │       │       │       ├── [Met verlichting, OPTIONAL, LIST (Ja|Nee)]  Met verlichting
        │       │       │       └── [Interactief, OPTIONAL, LIST (Ja|Nee)]  Interactief
        │       │       ├── [176302]  Speelgoedkaphoofd
        │       │       ├── [176303]  Modepop
        │       │       │   └── Attributes
        │       │       │       ├── [Maakt geluid, OPTIONAL, LIST (Ja|Nee)]  Maakt geluid
        │       │       │       └── [Met verlichting, OPTIONAL, LIST (Ja|Nee)]  Met verlichting
        │       │       ├── [176304]  Speelfigurenset
        │       │       │   └── Attributes
        │       │       │       └── [Speelgoedthema, RECOMMENDED, LIST (Bouw|Brandweer|Circus|Cowboys en Indianen|Dieren|Dierentuin|Dinosaurussen|Draken|Fantasy|Film; TV en Muziek|Kermis|Kerst|Leger|Manege|Mode|Piraten|Politie|Prinsessen|Ridders|Robots|Ruimtevaart|Safari|School|Science fiction|Sport|Stadsleven|Vakantie|Ziekenhuis|Boerderij)]  Speelgoedthema
        │       │       ├── [176305]  Speelfiguur
        │       │       │   └── Attributes
        │       │       │       └── [Speelgoedthema, RECOMMENDED, LIST (Bouw|Brandweer|Circus|Cowboys en Indianen|Dieren|Dierentuin|Dinosaurussen|Draken|Fantasy|Film; TV en Muziek|Kermis|Kerst|Leger|Manege|Mode|Piraten|Politie|Prinsessen|Ridders|Robots|Ruimtevaart|Safari|School|Science fiction|Sport|Stadsleven|Vakantie|Ziekenhuis|Boerderij)]  Speelgoedthema
        │       │       ├── [176306]  Speelgoedrobot
        │       │       │   └── Attributes
        │       │       │       ├── [Te programmeren, OPTIONAL, LIST (Ja|Nee)]  Te programmeren
        │       │       │       ├── [Inclusief afstandsbediening, OPTIONAL, LIST (Ja|Nee)]  Inclusief afstandsbediening
        │       │       │       └── [Bediening via app, OPTIONAL, LIST (Ja|Nee)]  Bediening via app
        │       │       └── [176307]  Poppenhuispop
        │       ├── [1764]  Poppenkasten
        │       │   └── Children
        │       │       └── [176400]  Poppenkast
        │       ├── [1765]  Poppenkleding
        │       │   ├── Attributes
        │       │   │   └── [Type poppenkleding, REQUIRED, LIST (Kledingset|Jurk|Schoenen|Nachtkleding|Boxpak|Jas|Badkleding|Ondergoed|Romper|Sokken|Joggingpak|T-shirt|Kostuum|Wintersportkleding|Trui|Broek|Badjas|Sportkleding|Maillot|Pet|Rok|Dokterskleding|Bruidsjurk)]  Type poppenkleding
        │       │   └── Children
        │       │       ├── [176500]  Babypoppenkleding
        │       │       └── [176501]  Modepoppenkleding
        │       ├── [1766]  Poppenmeubilair
        │       │   ├── Attributes
        │       │   │   └── [Type poppenmeubel, REQUIRED, LIST (Accessoireset|Wieg|Commode|Kast|Slaapkamerset|Bad|Eetkamerset|Wasruimteset|Kaptafel)]  Type poppenmeubel
        │       │   └── Children
        │       │       ├── [176600]  Babypoppenmeubel
        │       │       ├── [176601]  Modepoppenmeubel
        │       │       ├── [176602]  Poppenbed
        │       │       ├── [176603]  Poppenhuisinrichting
        │       │       ├── [176604]  Poppenmeubel
        │       │       ├── [176605]  Poppenstoel
        │       │       └── [176606]  Poppenwagen
        │       └── [1767]  Knuffels (niet-elektrisch)
        │           ├── Attributes
        │           │   ├── [Maakt geluid, OPTIONAL, LIST (Ja|Nee)]  Maakt geluid
        │           │   ├── [Met verlichting, OPTIONAL, LIST (Ja|Nee)]  Met verlichting
        │           │   ├── [Interactief, OPTIONAL, LIST (Ja|Nee)]  Interactief
        │           │   ├── [Wasbaar, REQUIRED, LIST (Ja|Nee)]  Wasbaar
        │           │   ├── [Lengte product in cm, RECOMMENDED, INTEGER]  Lengte product in cm
        │           │   └── [Personage, OPTIONAL, LIST (101 dalmatiërs|Aladdin|Angry Birds|Ant-Man|Aquaman|Assassin's Creed|Assepoester|Asterix|Baby Annabell|Bakugan|Barbapapa|Barbie|Batman|Belle en het Beest|Big Hero 6|Black Widow|Blaze|Bob de Bouwer|Brandweerman Sam|Buurman & Buurman|Bratz|Bumba|Woezel en Pip|DC Super Hero Girls|De Kleine Zeemeermin|Disney|Disney Cars|Disney Fairies|Disney Frozen|Disney Princess|Donald Duck|Doornroosje|Dora the Explorer|Dr. Who|Dragon Ball Z|Elena van Avalor|Echantimals|Fallout|FIFA|Finding Dory|Fortnite|Game Of Thrones|God of War|Guardians of the Galaxy|Halo|Harley Quinn|Hannah Montana|Harry Potter|Hatchimals|Hello Kitty|Hoe Tem je een Draak|Hulk|Iron Man|James Bond|Jurassic World|K3|Kerst|Kikker|Kuifje|Kung Fu Panda|Lalaloopsy|Lion King|Little People|Looney Tunes|Lord of the Rings|Madagascar|Marvel|Masha en de Beer|Maya de Bij|Minecraft|Mickey Mouse|Minions|Minnie Mouse|Monster High|Moxie Girlz|My Little Pony|Nijntje|PAW Patrol|Peppa Pig|Peter Pan|Pickachu|Planes|Pokémon|Power Rangers|Prinses Sofia|Prinsessia|Rapunzel|Robin Hood|Roodkapje|Sesamstraat|Shimmer and Shine|Smurfen|Sneeuwwitje|Snoopy|Sofia het Prinsesje|Sonic|Spiderman|Spongebob Squarepants|Star Trek|Star Wars|Super Mario|Sunny Day|Supergirl|Superman|The Avengers|The Big Bang Theory|The Good Dinosaur|The Hulk|The Incredibles|The Joker|The Secret Live of Pets|The Simpsons|Thomas de Trein|Thor|Toy Story|Transformers|Tinkerbell|Trolls|Violetta|Winnie de Poeh|Winx|Wonder Woman|X-men|Zelda|Zootropolis|Astra|Bello|Chase|Dizzy|Donnie|Flip|Grand Albert|Jerome|Jett|Mira|Paul|Poppa WHeels|Todd|PJ Masks|Southpark|Teletubbies)]  Personage
        │           └── Children
        │               ├── [176700]  Knuffeldier
        │               │   └── Attributes
        │               │       └── [Diersoort, REQUIRED, LIST (Aap|Alpaca|Beer|Bever|Bij|Buideldier|Cavia|Das|Dinosaurus|Dolfijn|Draak|Eekhoorn|Eend|Eenhoorn|Egel|Ezel|Fantasiedier|Flamingo|Gans|Geit|Giraffe|Gordeldier|Haai|Haas|Hamster|Hert|Hond|Hyena|Ijsbeer|Inktvis|Insect|Jaguar|Kameel|Kameleon|Kangoeroe|Kat|Kikker|Kip|Koala|Koe|Konijn|Krokodil|Lama|Leeuw|Lieveheersbeestje|Luiaard|Luipaard|Mammoet|Miereneter|Mol|Muis|Neushoorn|Nijlpaard|Octopus|Olifant|Ooievaar|Orka|Otter|Paard|Panda|Panter|Papegaai|Pauw|Pinguin|Rat|Rendier|Rog|Rups|Schaap|Schildpad|Slak|Slang|Spin|Stinkdier|Stokstaartje|Struisvogel|Tapir|Tijger|Uil|Varken|Vis|Vleermuis|Vlinder|Vogel|Vos|Walvis|Wasbeer|Wezel|Wolf|Zebra|Zeedier|Zeehond|Zeepaardje|Zwijn|Zwaan)]  Diersoort
        │               ├── [176701]  Knuffeldoek
        │               │   └── Attributes
        │               │       └── [Diersoort, OPTIONAL, LIST (Aap|Alpaca|Beer|Bever|Bij|Buideldier|Cavia|Das|Dinosaurus|Dolfijn|Draak|Eekhoorn|Eend|Eenhoorn|Egel|Ezel|Fantasiedier|Flamingo|Gans|Geit|Giraffe|Gordeldier|Haai|Haas|Hamster|Hert|Hond|Hyena|Ijsbeer|Inktvis|Insect|Jaguar|Kameel|Kameleon|Kangoeroe|Kat|Kikker|Kip|Koala|Koe|Konijn|Krokodil|Lama|Leeuw|Lieveheersbeestje|Luiaard|Luipaard|Mammoet|Miereneter|Mol|Muis|Neushoorn|Nijlpaard|Octopus|Olifant|Ooievaar|Orka|Otter|Paard|Panda|Panter|Papegaai|Pauw|Pinguin|Rat|Rendier|Rog|Rups|Schaap|Schildpad|Slak|Slang|Spin|Stinkdier|Stokstaartje|Struisvogel|Tapir|Tijger|Uil|Varken|Vis|Vleermuis|Vlinder|Vogel|Vos|Walvis|Wasbeer|Wezel|Wolf|Zebra|Zeedier|Zeehond|Zeepaardje|Zwijn|Zwaan)]  Diersoort
        │               └── [176702]  Knuffelpop
        ├── [177]  Spellen & Puzzels
        │   └── Children
        │       ├── [1770]  Bordspelen (niet-elektrisch)
        │       │   ├── Attributes
        │       │   │   ├── [Minimum aantal spelers, REQUIRED, INTEGER]  Minimum aantal spelers
        │       │   │   ├── [Maximum aantal spelers, REQUIRED, INTEGER]  Maximum aantal spelers
        │       │   │   ├── [Gemiddelde speeltijd in min, REQUIRED, INTEGER]  Gemiddelde speeltijd in min
        │       │   │   ├── [Spelsoort, RECOMMENDED, LIST (Stroytelling spel|Damspel|Trading cards|Speelkaarten|Kwartet|Behendingheidsspel|Denkspel|Educatief spel|Partyspel|Abstract spel|Geluksspel|Strategisch spel|Bingo spel|Vraag/antwoord spel|Rollenspel|Trivia Spel|Coöperatief spel|Tablet spel|Puzzelspel|Kansspel|DVD spel|Multispel)]  Spelsoort
        │       │   │   └── [Editie, REQUIRED, LIST (Standaard editie|Luxe uitgave|Uitbreiding|Licentie uitgave|Pocketspel|Internationale versie)]  Editie
        │       │   └── Children
        │       │       ├── [177000]  Actiespel
        │       │       ├── [177001]  Bordspel
        │       │       ├── [177002]  Dobbelspel
        │       │       └── [177003]  Vloerspel
        │       ├── [1771]  Bordspelen/Kaarten/Puzzels – Accessoires/Onderdelen
        │       │   └── Children
        │       │       ├── [177100]  Dobbelbeker
        │       │       ├── [177101]  Puzzellijm
        │       │       │   └── Attributes
        │       │       │       └── [Inhoud in ml, REQUIRED, INTEGER]  Inhoud in ml
        │       │       ├── [177102]  Puzzelmap
        │       │       ├── [177103]  Puzzelmat
        │       │       └── [177104]  Puzzelsorteerbak
        │       ├── [1772]  Kaartspelen (niet-elektrisch)
        │       │   ├── Attributes
        │       │   │   ├── [Gemiddelde speeltijd in min, REQUIRED, INTEGER]  Gemiddelde speeltijd in min
        │       │   │   ├── [Spelsoort, RECOMMENDED, LIST (Stroytelling spel|Damspel|Trading cards|Speelkaarten|Kwartet|Behendingheidsspel|Denkspel|Educatief spel|Partyspel|Abstract spel|Geluksspel|Strategisch spel|Bingo spel|Vraag/antwoord spel|Rollenspel|Trivia Spel|Coöperatief spel|Tablet spel|Puzzelspel|Kansspel|DVD spel|Multispel)]  Spelsoort
        │       │   │   └── [Editie, REQUIRED, LIST (Standaard editie|Luxe uitgave|Uitbreiding|Licentie uitgave|Pocketspel|Internationale versie)]  Editie
        │       │   └── Children
        │       │       └── [177200]  Kaartspel
        │       ├── [1773]  Puzzels
        │       │   ├── Attributes
        │       │   │   ├── [Aantal puzzelstukjes, REQUIRED, INTEGER]  Aantal puzzelstukjes
        │       │   │   ├── [Puzzelthema, REQUIRED, LIST (Dieren|Fantasie|Cartoon|Walt Disney|Steden en bouwwerken|Landschap|Televisie en film|Nostalgie en geschiedenis|Rustiek en landelijk|Kunst en religie|Fotografie|Feestdagen|Bloemen en Planten|Landkaarten)]  Puzzelthema
        │       │   │   └── [Personage, OPTIONAL, LIST (101 dalmatiërs|Aladdin|Angry Birds|Ant-Man|Aquaman|Assassin's Creed|Assepoester|Asterix|Baby Annabell|Bakugan|Barbapapa|Barbie|Batman|Belle en het Beest|Big Hero 6|Black Widow|Blaze|Bob de Bouwer|Brandweerman Sam|Buurman & Buurman|Bratz|Bumba|Woezel en Pip|DC Super Hero Girls|De Kleine Zeemeermin|Disney|Disney Cars|Disney Fairies|Disney Frozen|Disney Princess|Donald Duck|Doornroosje|Dora the Explorer|Dr. Who|Dragon Ball Z|Elena van Avalor|Echantimals|Fallout|FIFA|Finding Dory|Fortnite|Game Of Thrones|God of War|Guardians of the Galaxy|Halo|Harley Quinn|Hannah Montana|Harry Potter|Hatchimals|Hello Kitty|Hoe Tem je een Draak|Hulk|Iron Man|James Bond|Jurassic World|K3|Kerst|Kikker|Kuifje|Kung Fu Panda|Lalaloopsy|Lion King|Little People|Looney Tunes|Lord of the Rings|Madagascar|Marvel|Masha en de Beer|Maya de Bij|Minecraft|Mickey Mouse|Minions|Minnie Mouse|Monster High|Moxie Girlz|My Little Pony|Nijntje|PAW Patrol|Peppa Pig|Peter Pan|Pickachu|Planes|Pokémon|Power Rangers|Prinses Sofia|Prinsessia|Rapunzel|Robin Hood|Roodkapje|Sesamstraat|Shimmer and Shine|Smurfen|Sneeuwwitje|Snoopy|Sofia het Prinsesje|Sonic|Spiderman|Spongebob Squarepants|Star Trek|Star Wars|Super Mario|Sunny Day|Supergirl|Superman|The Avengers|The Big Bang Theory|The Good Dinosaur|The Hulk|The Incredibles|The Joker|The Secret Live of Pets|The Simpsons|Thomas de Trein|Thor|Toy Story|Transformers|Tinkerbell|Trolls|Violetta|Winnie de Poeh|Winx|Wonder Woman|X-men|Zelda|Zootropolis|Astra|Bello|Chase|Dizzy|Donnie|Flip|Grand Albert|Jerome|Jett|Mira|Paul|Poppa WHeels|Todd|PJ Masks|Southpark|Teletubbies)]  Personage
        │       │   └── Children
        │       │       ├── [177300]  3D-puzzel
        │       │       ├── [177301]  Blokpuzzel
        │       │       ├── [177302]  Breinbreker
        │       │       │   └── Attributes
        │       │       │       ├── [Gemiddelde speeltijd in min, REQUIRED, INTEGER]  Gemiddelde speeltijd in min
        │       │       │       └── [Spelsoort, RECOMMENDED, LIST (Stroytelling spel|Damspel|Trading cards|Speelkaarten|Kwartet|Behendingheidsspel|Denkspel|Educatief spel|Partyspel|Abstract spel|Geluksspel|Strategisch spel|Bingo spel|Vraag/antwoord spel|Rollenspel|Trivia Spel|Coöperatief spel|Tablet spel|Puzzelspel|Kansspel|DVD spel|Multispel)]  Spelsoort
        │       │       ├── [177303]  Legpuzzel
        │       │       ├── [177304]  Puzzelbal
        │       │       ├── [177305]  Schuifpuzzel
        │       │       ├── [177306]  Vloerpuzzel
        │       │       └── [177307]  Vormenpuzzel
        │       ├── [1774]  Tafelspelen
        │       │   └── Children
        │       │       ├── [177400]  Airhockeypuck
        │       │       ├── [177401]  Airhockeypusher
        │       │       ├── [177402]  Airhockeytafel
        │       │       ├── [177403]  Multispeltafel
        │       │       ├── [177404]  Pokertafel
        │       │       │   └── Attributes
        │       │       │       ├── [Minimum aantal spelers, REQUIRED, INTEGER]  Minimum aantal spelers
        │       │       │       └── [Maximum aantal spelers, REQUIRED, INTEGER]  Maximum aantal spelers
        │       │       ├── [177405]  Sjoelbak
        │       │       ├── [177406]  Sjoelschijven
        │       │       ├── [177407]  Arcadekast
        │       │       │   └── Attributes
        │       │       │       ├── [Minimum aantal spelers, REQUIRED, INTEGER]  Minimum aantal spelers
        │       │       │       └── [Maximum aantal spelers, REQUIRED, INTEGER]  Maximum aantal spelers
        │       │       ├── [177408]  Speeltafel
        │       │       ├── [177409]  Voetbaltafel
        │       │       ├── [177410]  Voetbaltafelbal
        │       │       ├── [177411]  Voetbaltafelhandvat
        │       │       ├── [177412]  Voetbaltafelpoppetje
        │       │       ├── [177413]  Voetbaltafelstang
        │       │       ├── [177414]  Flipperkast
        │       │       │   └── Attributes
        │       │       │       ├── [Minimum aantal spelers, REQUIRED, INTEGER]  Minimum aantal spelers
        │       │       │       └── [Maximum aantal spelers, REQUIRED, INTEGER]  Maximum aantal spelers
        │       │       ├── [177415]  Pooltafel
        │       │       │   └── Attributes
        │       │       │       ├── [Formaat Speelveld in ft., REQUIRED, INTEGER]  Formaat Speelveld in ft.
        │       │       │       └── [Type pooltafel, OPTIONAL, LIST (Pooltafel|Snookertafel|Biljarttafel)]  Type pooltafel
        │       │       └── [177416]  Tafeltennistafel
        │       │           └── Attributes
        │       │               ├── [Geschikt voor binnen, OPTIONAL, LIST (Ja|Nee)]  Geschikt voor binnen
        │       │               ├── [Geschikt voor buiten, OPTIONAL, LIST (Ja|Nee)]  Geschikt voor buiten
        │       │               └── [Verrijdbaar, REQUIRED, LIST (Ja|Nee)]  Verrijdbaar
        │       └── [1775]  Darten
        │           └── Children
        │               ├── [177500]  Dartbord
        │               │   └── Attributes
        │               │       └── [Type dartbord, REQUIRED, LIST (Sisal|Elektronisch)]  Type dartbord
        │               ├── [177501]  Dartpijlen
        │               │   └── Attributes
        │               │       ├── [Gewicht in gram, REQUIRED, INTEGER]  Gewicht in gram
        │               │       └── [Type dartpijl, REQUIRED, LIST (Steeltip dartpijl|Softtip dartpijl)]  Type dartpijl
        │               ├── [177502]  Surroundring
        │               ├── [177503]  Dartkabinet
        │               └── [177504]  Dartmat
        ├── [178]  Rollenspellen
        │   └── Children
        │       ├── [1780]  Rollenspel – Huishouden/Tuinieren/DHZ Speelgoed
        │       │   └── Children
        │       │       ├── [178000]  Speelgoedboor
        │       │       │   └── Attributes
        │       │       │       ├── [Batterij meegeleverd, REQUIRED, LIST (Ja|Nee)]  Batterij meegeleverd
        │       │       │       ├── [Batterij nodig, REQUIRED, LIST (Ja|Nee)]  Batterij nodig
        │       │       │       ├── [Type batterij, OPTIONAL, TEXT]  Type batterij
        │       │       │       ├── [Aantal batterijen, OPTIONAL, INTEGER]  Aantal batterijen
        │       │       │       └── [Oplaadbaar, OPTIONAL, LIST (Ja|Nee)]  Oplaadbaar
        │       │       ├── [178001]  Speelgoeddroogrek
        │       │       ├── [178002]  Speelgoedgereedschapskoffer
        │       │       ├── [178003]  Speelgoedkaptafel
        │       │       ├── [178004]  Speelgoedkruiwagen
        │       │       ├── [178005]  Speelgoedmake-up
        │       │       ├── [178006]  Speelgoedschoonmaakset
        │       │       ├── [178007]  Speelgoedschroevendraaier
        │       │       │   └── Attributes
        │       │       │       ├── [Batterij meegeleverd, OPTIONAL, LIST (Ja|Nee)]  Batterij meegeleverd
        │       │       │       ├── [Batterij nodig, OPTIONAL, LIST (Ja|Nee)]  Batterij nodig
        │       │       │       ├── [Type batterij, OPTIONAL, TEXT]  Type batterij
        │       │       │       ├── [Aantal batterijen, OPTIONAL, INTEGER]  Aantal batterijen
        │       │       │       └── [Oplaadbaar, OPTIONAL, LIST (Ja|Nee)]  Oplaadbaar
        │       │       ├── [178008]  Speelgoedstofzuiger
        │       │       │   └── Attributes
        │       │       │       ├── [Batterij meegeleverd, OPTIONAL, LIST (Ja|Nee)]  Batterij meegeleverd
        │       │       │       ├── [Batterij nodig, OPTIONAL, LIST (Ja|Nee)]  Batterij nodig
        │       │       │       ├── [Type batterij, OPTIONAL, TEXT]  Type batterij
        │       │       │       ├── [Aantal batterijen, OPTIONAL, INTEGER]  Aantal batterijen
        │       │       │       └── [Oplaadbaar, OPTIONAL, LIST (Ja|Nee)]  Oplaadbaar
        │       │       ├── [178009]  Speelgoedstrijkijzer
        │       │       │   └── Attributes
        │       │       │       ├── [Batterij meegeleverd, OPTIONAL, LIST (Ja|Nee)]  Batterij meegeleverd
        │       │       │       ├── [Batterij nodig, OPTIONAL, LIST (Ja|Nee)]  Batterij nodig
        │       │       │       ├── [Type batterij, OPTIONAL, TEXT]  Type batterij
        │       │       │       ├── [Aantal batterijen, OPTIONAL, INTEGER]  Aantal batterijen
        │       │       │       └── [Oplaadbaar, OPTIONAL, LIST (Ja|Nee)]  Oplaadbaar
        │       │       ├── [178010]  Speelgoedstrijkplank
        │       │       ├── [178011]  Speelgoedwasmachine
        │       │       │   └── Attributes
        │       │       │       ├── [Batterij meegeleverd, OPTIONAL, LIST (Ja|Nee)]  Batterij meegeleverd
        │       │       │       ├── [Batterij nodig, OPTIONAL, LIST (Ja|Nee)]  Batterij nodig
        │       │       │       ├── [Type batterij, OPTIONAL, TEXT]  Type batterij
        │       │       │       ├── [Aantal batterijen, OPTIONAL, INTEGER]  Aantal batterijen
        │       │       │       └── [Oplaadbaar, OPTIONAL, LIST (Ja|Nee)]  Oplaadbaar
        │       │       ├── [178012]  Speelgoedwerkbank
        │       │       ├── [178013]  Speelgoedzaag
        │       │       │   └── Attributes
        │       │       │       ├── [Batterij meegeleverd, OPTIONAL, LIST (Ja|Nee)]  Batterij meegeleverd
        │       │       │       ├── [Batterij nodig, OPTIONAL, LIST (Ja|Nee)]  Batterij nodig
        │       │       │       ├── [Type batterij, OPTIONAL, TEXT]  Type batterij
        │       │       │       ├── [Aantal batterijen, OPTIONAL, INTEGER]  Aantal batterijen
        │       │       │       └── [Oplaadbaar, OPTIONAL, LIST (Ja|Nee)]  Oplaadbaar
        │       │       └── [178014]  Tuinierspeelgoed
        │       ├── [1781]  Rollenspel – Keukenspeelgoed
        │       │   └── Children
        │       │       ├── [178100]  Speelgoedbarbecue
        │       │       │   └── Attributes
        │       │       │       ├── [Batterij meegeleverd, OPTIONAL, LIST (Ja|Nee)]  Batterij meegeleverd
        │       │       │       ├── [Batterij nodig, OPTIONAL, LIST (Ja|Nee)]  Batterij nodig
        │       │       │       ├── [Type batterij, OPTIONAL, TEXT]  Type batterij
        │       │       │       ├── [Aantal batterijen, OPTIONAL, INTEGER]  Aantal batterijen
        │       │       │       └── [Oplaadbaar, OPTIONAL, LIST (Ja|Nee)]  Oplaadbaar
        │       │       ├── [178101]  Speelgoedbroodrooster
        │       │       │   └── Attributes
        │       │       │       ├── [Batterij meegeleverd, OPTIONAL, LIST (Ja|Nee)]  Batterij meegeleverd
        │       │       │       ├── [Batterij nodig, OPTIONAL, LIST (Ja|Nee)]  Batterij nodig
        │       │       │       ├── [Type batterij, OPTIONAL, TEXT]  Type batterij
        │       │       │       ├── [Aantal batterijen, OPTIONAL, INTEGER]  Aantal batterijen
        │       │       │       └── [Oplaadbaar, OPTIONAL, LIST (Ja|Nee)]  Oplaadbaar
        │       │       ├── [178102]  Speelgoedeten en -drinken
        │       │       ├── [178103]  Speelgoedkeukenaccessoireset
        │       │       ├── [178104]  Speelgoedkeukenmachine
        │       │       │   └── Attributes
        │       │       │       ├── [Batterij meegeleverd, OPTIONAL, LIST (Ja|Nee)]  Batterij meegeleverd
        │       │       │       ├── [Batterij nodig, OPTIONAL, LIST (Ja|Nee)]  Batterij nodig
        │       │       │       ├── [Type batterij, OPTIONAL, TEXT]  Type batterij
        │       │       │       ├── [Aantal batterijen, OPTIONAL, INTEGER]  Aantal batterijen
        │       │       │       └── [Oplaadbaar, OPTIONAL, LIST (Ja|Nee)]  Oplaadbaar
        │       │       ├── [178105]  Speelgoedkoelkast
        │       │       │   └── Attributes
        │       │       │       ├── [Batterij meegeleverd, OPTIONAL, LIST (Ja|Nee)]  Batterij meegeleverd
        │       │       │       ├── [Batterij nodig, OPTIONAL, LIST (Ja|Nee)]  Batterij nodig
        │       │       │       ├── [Type batterij, OPTIONAL, TEXT]  Type batterij
        │       │       │       ├── [Aantal batterijen, OPTIONAL, INTEGER]  Aantal batterijen
        │       │       │       └── [Oplaadbaar, OPTIONAL, LIST (Ja|Nee)]  Oplaadbaar
        │       │       ├── [178106]  Speelgoedkoffiezetapparaat
        │       │       │   └── Attributes
        │       │       │       ├── [Batterij meegeleverd, OPTIONAL, LIST (Ja|Nee)]  Batterij meegeleverd
        │       │       │       ├── [Batterij nodig, OPTIONAL, LIST (Ja|Nee)]  Batterij nodig
        │       │       │       ├── [Type batterij, OPTIONAL, TEXT]  Type batterij
        │       │       │       ├── [Aantal batterijen, OPTIONAL, INTEGER]  Aantal batterijen
        │       │       │       └── [Oplaadbaar, OPTIONAL, LIST (Ja|Nee)]  Oplaadbaar
        │       │       ├── [178107]  Speelgoedoven
        │       │       │   └── Attributes
        │       │       │       ├── [Batterij meegeleverd, OPTIONAL, LIST (Ja|Nee)]  Batterij meegeleverd
        │       │       │       ├── [Batterij nodig, OPTIONAL, LIST (Ja|Nee)]  Batterij nodig
        │       │       │       ├── [Type batterij, OPTIONAL, TEXT]  Type batterij
        │       │       │       ├── [Aantal batterijen, OPTIONAL, INTEGER]  Aantal batterijen
        │       │       │       └── [Oplaadbaar, OPTIONAL, LIST (Ja|Nee)]  Oplaadbaar
        │       │       ├── [178108]  Speelgoedpannenset
        │       │       ├── [178109]  Speelgoedpicknickset
        │       │       ├── [178110]  Speelgoedservies
        │       │       ├── [178111]  Speelgoedtheeservies
        │       │       ├── [178112]  Speelgoedvaatwasser
        │       │       │   └── Attributes
        │       │       │       ├── [Batterij meegeleverd, OPTIONAL, LIST (Ja|Nee)]  Batterij meegeleverd
        │       │       │       ├── [Batterij nodig, OPTIONAL, LIST (Ja|Nee)]  Batterij nodig
        │       │       │       ├── [Type batterij, OPTIONAL, TEXT]  Type batterij
        │       │       │       ├── [Aantal batterijen, OPTIONAL, INTEGER]  Aantal batterijen
        │       │       │       └── [Oplaadbaar, OPTIONAL, LIST (Ja|Nee)]  Oplaadbaar
        │       │       ├── [178113]  Speelgoedwaterkoker
        │       │       │   └── Attributes
        │       │       │       ├── [Batterij meegeleverd, OPTIONAL, LIST (Ja|Nee)]  Batterij meegeleverd
        │       │       │       ├── [Batterij nodig, OPTIONAL, LIST (Ja|Nee)]  Batterij nodig
        │       │       │       ├── [Type batterij, OPTIONAL, TEXT]  Type batterij
        │       │       │       ├── [Aantal batterijen, OPTIONAL, INTEGER]  Aantal batterijen
        │       │       │       └── [Oplaadbaar, OPTIONAL, LIST (Ja|Nee)]  Oplaadbaar
        │       │       └── [178114]  Speelkeuken
        │       │           └── Attributes
        │       │               ├── [Maakt geluid, REQUIRED, LIST (Ja|Nee)]  Maakt geluid
        │       │               ├── [Lengte product in cm, REQUIRED, INTEGER]  Lengte product in cm
        │       │               ├── [Breedte product in cm, REQUIRED, INTEGER]  Breedte product in cm
        │       │               └── [Hoogte product in cm, REQUIRED, INTEGER]  Hoogte product in cm
        │       └── [1782]  Rollenspel – Winkel/Kantoor/Bedrijf Speelgoed
        │           └── Children
        │               ├── [178200]  Speelgoedbrandweerset
        │               ├── [178201]  Speelgoeddoktersset
        │               ├── [178202]  Speelgeld
        │               ├── [178203]  Speelgoedgereedschapsset
        │               │   └── Attributes
        │               │       ├── [Batterij meegeleverd, OPTIONAL, LIST (Ja|Nee)]  Batterij meegeleverd
        │               │       ├── [Batterij nodig, OPTIONAL, LIST (Ja|Nee)]  Batterij nodig
        │               │       ├── [Type batterij, OPTIONAL, TEXT]  Type batterij
        │               │       ├── [Aantal batterijen, OPTIONAL, INTEGER]  Aantal batterijen
        │               │       └── [Oplaadbaar, OPTIONAL, LIST (Ja|Nee)]  Oplaadbaar
        │               ├── [178204]  Speelgoedhijskraan
        │               ├── [178205]  Speelgoedkassa
        │               │   └── Attributes
        │               │       ├── [Batterij meegeleverd, OPTIONAL, LIST (Ja|Nee)]  Batterij meegeleverd
        │               │       ├── [Batterij nodig, OPTIONAL, LIST (Ja|Nee)]  Batterij nodig
        │               │       ├── [Type batterij, OPTIONAL, TEXT]  Type batterij
        │               │       ├── [Aantal batterijen, OPTIONAL, INTEGER]  Aantal batterijen
        │               │       ├── [Oplaadbaar, OPTIONAL, LIST (Ja|Nee)]  Oplaadbaar
        │               │       └── [Maakt geluid, OPTIONAL, LIST (Ja|Nee)]  Maakt geluid
        │               ├── [178206]  Speelgoedwinkeltje
        │               │   └── Attributes
        │               │       ├── [Maakt geluid, OPTIONAL, LIST (Ja|Nee)]  Maakt geluid
        │               │       ├── [Batterij meegeleverd, OPTIONAL, LIST (Ja|Nee)]  Batterij meegeleverd
        │               │       ├── [Batterij nodig, OPTIONAL, LIST (Ja|Nee)]  Batterij nodig
        │               │       ├── [Type batterij, OPTIONAL, TEXT]  Type batterij
        │               │       ├── [Aantal batterijen, OPTIONAL, INTEGER]  Aantal batterijen
        │               │       └── [Oplaadbaar, OPTIONAL, LIST (Ja|Nee)]  Oplaadbaar
        │               ├── [178207]  Spionagespeelgoed
        │               ├── [178208]  Speelgoedwinkelwagen
        │               └── [178209]  Speelgoedwinkelmand
        └── [179]  Speelgoed voertuigen en modellen
            └── Children
                ├── [1790]  Auto/Treinset – Onderdelen/Accessoires
                │   └── Children
                │       ├── [179000]  Racebaan
                │       ├── [179001]  Racebaan auto
                │       │   └── Attributes
                │       │       ├── [Batterij meegeleverd, OPTIONAL, LIST (Ja|Nee)]  Batterij meegeleverd
                │       │       ├── [Batterij nodig, OPTIONAL, LIST (Ja|Nee)]  Batterij nodig
                │       │       ├── [Type batterij, OPTIONAL, TEXT]  Type batterij
                │       │       ├── [Aantal batterijen, OPTIONAL, INTEGER]  Aantal batterijen
                │       │       ├── [Controller meegeleverd, REQUIRED, LIST (Ja|Nee)]  Controller meegeleverd
                │       │       └── [Voedingstype, REQUIRED, LIST_MULTIPLE_VALUES (Adapter/lichtnet|Batterij|Netstroom|Accu|USB)]  Voedingstype
                │       ├── [179002]  Treinbaantrein
                │       │   └── Attributes
                │       │       └── [Maakt geluid, OPTIONAL, LIST (Ja|Nee)]  Maakt geluid
                │       ├── [179003]  Racebaanonderdeel
                │       │   └── Attributes
                │       │       └── [Type baanonderdeel, REQUIRED, LIST (Baandeel|Decoratie|Controller|Rondeteller|Vangrails|Sleepcontacten)]  Type baanonderdeel
                │       ├── [179004]  Treinbaan
                │       │   └── Attributes
                │       │       ├── [Lengte product in cm, REQUIRED, INTEGER]  Lengte product in cm
                │       │       ├── [Elektrisch, REQUIRED, LIST (Ja|Nee)]  Elektrisch
                │       │       ├── [Controller meegeleverd, REQUIRED, LIST (Ja|Nee)]  Controller meegeleverd
                │       │       ├── [Batterij meegeleverd, OPTIONAL, LIST (Ja|Nee)]  Batterij meegeleverd
                │       │       ├── [Batterij nodig, OPTIONAL, LIST (Ja|Nee)]  Batterij nodig
                │       │       ├── [Type batterij, OPTIONAL, TEXT]  Type batterij
                │       │       └── [Aantal batterijen, OPTIONAL, INTEGER]  Aantal batterijen
                │       ├── [179005]  Treinbaanonderdeel
                │       │   └── Attributes
                │       │       └── [Type baanonderdeel, REQUIRED, LIST (Baandeel|Decoratie|Controller|Rondeteller|Vangrails|Sleepcontacten)]  Type baanonderdeel
                │       └── [179006]  Verkeerskleed
                ├── [1791]  Speelgoedvoertuigen – Berijdbaar (elektrisch)
                │   └── Children
                │       ├── [179100]  Accuvoertuig
                │       │   └── Attributes
                │       │       ├── [Automerk, RECOMMENDED, LIST (Alfa Romeo|AMC|Aston Martin|Audi|Austin|Autobianchi|Bitter|BMW|Bugatti|Buick|Cadillac|Chevrolet|Chrysler|Citroën|Daewoo|DAF|Daihatsu|Daimler|De Lorean|DeTomaso|Dodge|Donkervoort|Eagle|ERF|Ferrari|Fiat|Ford|Galopper|GMC|Holden|Honda|Hummer|Hyundai|Isuzu|Iveco|Jaguar|Jeep|Kenworth|Kia|Lada|Lamborghini|Lancia|Land Rover|Lexus|Lincoln|Lotus|Mack|MAN|Marcos|Maserati|Maybach|Mazda|Mercedes|Mercury|MG|Mini|Morgan|Nissan|Nobel|Oldsmobile|Opel|Peterbilt|Peugeot|Plymouth|Pontiac|Porsche|Renault|Rolls-Royce|Rover|SAAB|Scania|SEAT|Skoda|Smart|Spyker|Ssangyong|Subaru|Suzuki|Talbot|Toyota|TVR|Volkswagen|Volvo|Westfield|White|Zimmer|Regressie 14-09|Regressie 23-10-2020)]  Automerk
                │       │       ├── [Voltage, REQUIRED, INTEGER]  Voltage
                │       │       ├── [Type voertuig, REQUIRED, LIST (Auto|Quad|Motor|Tractor|Fiets|Bus|Trein|Boot)]  Type voertuig
                │       │       └── [Type band, REQUIRED, LIST (Massieve banden|Luchtbanden)]  Type band
                │       ├── [179101]  Zelfbalancerende scooter
                │       ├── [179102]  Airwheel
                │       └── [179103]  Hoverboard
                │           └── Attributes
                │               ├── [Wielmaat in inch, REQUIRED, INTEGER]  Wielmaat in inch
                │               ├── [Maximale belasting in kg, REQUIRED, INTEGER]  Maximale belasting in kg
                │               └── [max snelheid in km, RECOMMENDED, INTEGER]  max snelheid in km
                ├── [1792]  Speelgoedvoertuigen – Berijdbaar (niet-elektrisch)
                │   └── Children
                │       ├── [179200]  Bolderkar
                │       │   └── Attributes
                │       │       ├── [Opvouwbaar, REQUIRED, LIST (Ja|Nee)]  Opvouwbaar
                │       │       ├── [Maximale belasting in kg, REQUIRED, INTEGER]  Maximale belasting in kg
                │       │       └── [Type band, REQUIRED, LIST (Massieve banden|Luchtbanden)]  Type band
                │       ├── [179201]  Hobbelfiguur
                │       ├── [179202]  Trapautoaanhanger
                │       ├── [179203]  Loopauto
                │       │   └── Attributes
                │       │       ├── [Automerk, OPTIONAL, LIST (Alfa Romeo|AMC|Aston Martin|Audi|Austin|Autobianchi|Bitter|BMW|Bugatti|Buick|Cadillac|Chevrolet|Chrysler|Citroën|Daewoo|DAF|Daihatsu|Daimler|De Lorean|DeTomaso|Dodge|Donkervoort|Eagle|ERF|Ferrari|Fiat|Ford|Galopper|GMC|Holden|Honda|Hummer|Hyundai|Isuzu|Iveco|Jaguar|Jeep|Kenworth|Kia|Lada|Lamborghini|Lancia|Land Rover|Lexus|Lincoln|Lotus|Mack|MAN|Marcos|Maserati|Maybach|Mazda|Mercedes|Mercury|MG|Mini|Morgan|Nissan|Nobel|Oldsmobile|Opel|Peterbilt|Peugeot|Plymouth|Pontiac|Porsche|Renault|Rolls-Royce|Rover|SAAB|Scania|SEAT|Skoda|Smart|Spyker|Ssangyong|Subaru|Suzuki|Talbot|Toyota|TVR|Volkswagen|Volvo|Westfield|White|Zimmer|Regressie 14-09|Regressie 23-10-2020)]  Automerk
                │       │       ├── [Aantal wielen, OPTIONAL, INTEGER]  Aantal wielen
                │       │       ├── [Duwstang, OPTIONAL, LIST (Ja|Nee)]  Duwstang
                │       │       └── [Automerk 2, OPTIONAL, LIST (Alfa Romeo|AMC|Aston Martin|Audi|Austin|Autobianchi|Bitter|BMW|Bugatti|Buick|Cadillac|Chevrolet|Chrysler|Citroën|Daewoo|DAF|Daihatsu|Daimler|De Lorean|DeTomaso|Dodge|Donkervoort|Eagle|ERF|Ferrari|Fiat|Ford|Galopper|GMC|Holden|Honda|Hummer|Hyundai|Isuzu|Iveco|Jaguar|Jeep|Kenworth|Kia|Lada|Lamborghini|Lancia|Land Rover|Lexus|Lincoln|Lotus|Mack|MAN|Marcos|Maserati|Maybach|Mazda|Mercedes|Mercury|MG|Mini|Morgan|Nissan|Nobel|Oldsmobile|Opel|Peterbilt|Peugeot|Plymouth|Pontiac|Porsche|Renault|Rolls-Royce|Rover|SAAB|Scania|SEAT|Skoda|Smart|Spyker|Ssangyong|Subaru|Suzuki|Talbot|Toyota|TVR|Volkswagen|Volvo|Westfield|White|Zimmer|Regressie 14-09|Regressie 23-10-2020)]  Automerk 2
                │       ├── [179204]  Loopfiets
                │       │   └── Attributes
                │       │       ├── [Aantal wielen, OPTIONAL, INTEGER]  Aantal wielen
                │       │       ├── [Minimale zithoogte in cm, REQUIRED, INTEGER]  Minimale zithoogte in cm
                │       │       ├── [Veiligheidshandvatten, OPTIONAL, LIST (Ja|Nee)]  Veiligheidshandvatten
                │       │       ├── [Verstelbaar zadel, OPTIONAL, LIST (Ja|Nee)]  Verstelbaar zadel
                │       │       ├── [Verstelbaar stuur, OPTIONAL, LIST (Ja|Nee)]  Verstelbaar stuur
                │       │       ├── [Stuurbegrenzer, OPTIONAL, LIST (Ja|Nee)]  Stuurbegrenzer
                │       │       ├── [Standaard, OPTIONAL, LIST (Ja|Nee)]  Standaard
                │       │       ├── [Voetenplank, OPTIONAL, LIST (Ja|Nee)]  Voetenplank
                │       │       ├── [Kinbeschermer, OPTIONAL, LIST (Ja|Nee)]  Kinbeschermer
                │       │       ├── [Stuurvergrendeling, OPTIONAL, LIST (Ja|Nee)]  Stuurvergrendeling
                │       │       └── [Type band, REQUIRED, LIST (Massieve banden|Luchtbanden)]  Type band
                │       ├── [179205]  Loopmotor
                │       │   └── Attributes
                │       │       ├── [Minimale zithoogte in cm, REQUIRED, INTEGER]  Minimale zithoogte in cm
                │       │       ├── [Maximale belasting in kg, REQUIRED, INTEGER]  Maximale belasting in kg
                │       │       └── [Type band, REQUIRED, LIST (Massieve banden|Luchtbanden)]  Type band
                │       ├── [179206]  Skelter
                │       │   └── Attributes
                │       │       ├── [Reflectoren, OPTIONAL, LIST (Ja|Nee)]  Reflectoren
                │       │       ├── [verstelbaar zitje, OPTIONAL, LIST (Ja|Nee)]  verstelbaar zitje
                │       │       ├── [verstelbaar stoel, OPTIONAL, LIST (Ja|Nee)]  verstelbaar stoel
                │       │       ├── [Rem, OPTIONAL, LIST (Ja|Nee)]  Rem
                │       │       ├── [Vrijloopautomaat, OPTIONAL, LIST (Ja|Nee)]  Vrijloopautomaat
                │       │       ├── [Verstelbaar stuur, OPTIONAL, LIST (Ja|Nee)]  Verstelbaar stuur
                │       │       ├── [Inclusief aanhanger, OPTIONAL, LIST (Ja|Nee)]  Inclusief aanhanger
                │       │       ├── [Versnellingen, OPTIONAL, LIST (Ja|Nee)]  Versnellingen
                │       │       ├── [Voetrem, OPTIONAL, LIST (Ja|Nee)]  Voetrem
                │       │       ├── [Inclusief duo zitje, OPTIONAL, LIST (Ja|Nee)]  Inclusief duo zitje
                │       │       └── [Type band, REQUIRED, LIST (Massieve banden|Luchtbanden)]  Type band
                │       ├── [179207]  Step
                │       │   └── Attributes
                │       │       ├── [Wielmaat in cm, RECOMMENDED, INTEGER]  Wielmaat in cm
                │       │       ├── [Wielmaat in inch, RECOMMENDED, INTEGER]  Wielmaat in inch
                │       │       ├── [Aantal wielen, REQUIRED, INTEGER]  Aantal wielen
                │       │       ├── [Voetrem, OPTIONAL, LIST (Ja|Nee)]  Voetrem
                │       │       ├── [Verstelbaar stuur, OPTIONAL, LIST (Ja|Nee)]  Verstelbaar stuur
                │       │       ├── [Inklapbaar, OPTIONAL, LIST (Ja|Nee)]  Inklapbaar
                │       │       ├── [Handrem, OPTIONAL, LIST (Ja|Nee)]  Handrem
                │       │       └── [Type band, REQUIRED, LIST (Massieve banden|Luchtbanden)]  Type band
                │       ├── [179208]  Trapauto
                │       │   └── Attributes
                │       │       ├── [Type band, REQUIRED, LIST (Massieve banden|Luchtbanden)]  Type band
                │       │       └── [Automerk 2, OPTIONAL, LIST (Alfa Romeo|AMC|Aston Martin|Audi|Austin|Autobianchi|Bitter|BMW|Bugatti|Buick|Cadillac|Chevrolet|Chrysler|Citroën|Daewoo|DAF|Daihatsu|Daimler|De Lorean|DeTomaso|Dodge|Donkervoort|Eagle|ERF|Ferrari|Fiat|Ford|Galopper|GMC|Holden|Honda|Hummer|Hyundai|Isuzu|Iveco|Jaguar|Jeep|Kenworth|Kia|Lada|Lamborghini|Lancia|Land Rover|Lexus|Lincoln|Lotus|Mack|MAN|Marcos|Maserati|Maybach|Mazda|Mercedes|Mercury|MG|Mini|Morgan|Nissan|Nobel|Oldsmobile|Opel|Peterbilt|Peugeot|Plymouth|Pontiac|Porsche|Renault|Rolls-Royce|Rover|SAAB|Scania|SEAT|Skoda|Smart|Spyker|Ssangyong|Subaru|Suzuki|Talbot|Toyota|TVR|Volkswagen|Volvo|Westfield|White|Zimmer|Regressie 14-09|Regressie 23-10-2020)]  Automerk 2
                │       ├── [179209]  Traptractor
                │       │   └── Attributes
                │       │       └── [Type band, REQUIRED, LIST (Massieve banden|Luchtbanden)]  Type band
                │       └── [179210]  Space Scooter
                │           └── Attributes
                │               ├── [Aantal wielen, REQUIRED, INTEGER]  Aantal wielen
                │               ├── [Voetrem, OPTIONAL, LIST (Ja|Nee)]  Voetrem
                │               ├── [Verstelbaar stuur, OPTIONAL, LIST (Ja|Nee)]  Verstelbaar stuur
                │               ├── [Inklapbaar, OPTIONAL, LIST (Ja|Nee)]  Inklapbaar
                │               ├── [Handrem, OPTIONAL, LIST (Ja|Nee)]  Handrem
                │               └── [Type band, REQUIRED, LIST (Massieve banden|Luchtbanden)]  Type band
                ├── [1793]  Speelgoedvoertuigen – Niet-berijdbaar – Assortimenten
                │   └── Children
                │       └── [179300]  Speelgoedgarage
                ├── [1794]  Speelgoedvoertuigen – Niet-berijdbaar (elektrisch)
                │   └── Children
                │       ├── [179400]  Drone
                │       │   └── Attributes
                │       │       ├── [Live view, OPTIONAL, LIST (Ja|Nee)]  Live view
                │       │       ├── [Bediening via app, OPTIONAL, LIST (Ja|Nee)]  Bediening via app
                │       │       ├── [Return to Home, OPTIONAL, LIST (Ja|Nee)]  Return to Home
                │       │       ├── [Hoogte vasthouden, OPTIONAL, LIST (Ja|Nee)]  Hoogte vasthouden
                │       │       ├── [voorgeprogrameerde trucs, OPTIONAL, LIST (Ja|Nee)]  voorgeprogrameerde trucs
                │       │       ├── [Obstakel ontwijking, OPTIONAL, LIST (Ja|Nee)]  Obstakel ontwijking
                │       │       ├── [Inklapbaar, OPTIONAL, LIST (Ja|Nee)]  Inklapbaar
                │       │       ├── [No Fly Zone, OPTIONAL, LIST (Ja|Nee)]  No Fly Zone
                │       │       ├── [Throw and Go, OPTIONAL, LIST (Ja|Nee)]  Throw and Go
                │       │       ├── [Levensduur batterij in min, REQUIRED, INTEGER]  Levensduur batterij in min
                │       │       ├── [Camera, REQUIRED, LIST (Ja|Nee)]  Camera
                │       │       ├── [Aantal megapixels, RECOMMENDED, INTEGER]  Aantal megapixels
                │       │       ├── [GPS, OPTIONAL, LIST (Ja|Nee)]  GPS
                │       │       ├── [WiFi, OPTIONAL, LIST (Ja|Nee)]  WiFi
                │       │       ├── [Oplaadtijd in min, OPTIONAL, INTEGER]  Oplaadtijd in min
                │       │       ├── [Extern geheugen in gb, OPTIONAL, INTEGER]  Extern geheugen in gb
                │       │       ├── [max snelheid in km, RECOMMENDED, INTEGER]  max snelheid in km
                │       │       ├── [Geschikt voor binnen, REQUIRED, LIST (Ja|Nee)]  Geschikt voor binnen
                │       │       ├── [Geschikt voor buiten, REQUIRED, LIST (Ja|Nee)]  Geschikt voor buiten
                │       │       └── [Videoresolutie, OPTIONAL, LIST (4K|Full HD|HD Ready|VGA)]  Videoresolutie
                │       ├── [179401]  Droneaccessoire
                │       └── [179402]  RC voertuigonderdeel
                ├── [1795]  Speelgoedvoertuigen – Niet-berijdbaar (niet-elektrisch)
                │   ├── Attributes
                │   │   ├── [Maakt geluid, OPTIONAL, LIST (Ja|Nee)]  Maakt geluid
                │   │   ├── [Met verlichting, OPTIONAL, LIST (Ja|Nee)]  Met verlichting
                │   │   ├── [Mechanische onderdelen, OPTIONAL, LIST (Ja|Nee)]  Mechanische onderdelen
                │   │   └── [Elektrisch, REQUIRED, LIST (Ja|Nee)]  Elektrisch
                │   └── Children
                │       ├── [179500]  Speelgoedboot
                │       ├── [179501]  Speelgoedgraafmachine
                │       ├── [179502]  Speelgoedhelikopter
                │       ├── [179503]  Speelgoedkiepwagen
                │       ├── [179504]  Speelgoedkraanwagen
                │       ├── [179505]  Speelgoedtank
                │       ├── [179506]  Speelgoedruimtevoertuig
                │       ├── [179507]  Speelgoedauto
                │       │   └── Attributes
                │       │       └── [Automerk, RECOMMENDED, LIST (Alfa Romeo|AMC|Aston Martin|Audi|Austin|Autobianchi|Bitter|BMW|Bugatti|Buick|Cadillac|Chevrolet|Chrysler|Citroën|Daewoo|DAF|Daihatsu|Daimler|De Lorean|DeTomaso|Dodge|Donkervoort|Eagle|ERF|Ferrari|Fiat|Ford|Galopper|GMC|Holden|Honda|Hummer|Hyundai|Isuzu|Iveco|Jaguar|Jeep|Kenworth|Kia|Lada|Lamborghini|Lancia|Land Rover|Lexus|Lincoln|Lotus|Mack|MAN|Marcos|Maserati|Maybach|Mazda|Mercedes|Mercury|MG|Mini|Morgan|Nissan|Nobel|Oldsmobile|Opel|Peterbilt|Peugeot|Plymouth|Pontiac|Porsche|Renault|Rolls-Royce|Rover|SAAB|Scania|SEAT|Skoda|Smart|Spyker|Ssangyong|Subaru|Suzuki|Talbot|Toyota|TVR|Volkswagen|Volvo|Westfield|White|Zimmer|Regressie 14-09|Regressie 23-10-2020)]  Automerk
                │       ├── [179508]  Speelgoedmotor
                │       ├── [179509]  Speelgoedvliegtuig
                │       ├── [179510]  Speelgoedvrachtwagen
                │       │   └── Attributes
                │       │       └── [Automerk, OPTIONAL, LIST (Alfa Romeo|AMC|Aston Martin|Audi|Austin|Autobianchi|Bitter|BMW|Bugatti|Buick|Cadillac|Chevrolet|Chrysler|Citroën|Daewoo|DAF|Daihatsu|Daimler|De Lorean|DeTomaso|Dodge|Donkervoort|Eagle|ERF|Ferrari|Fiat|Ford|Galopper|GMC|Holden|Honda|Hummer|Hyundai|Isuzu|Iveco|Jaguar|Jeep|Kenworth|Kia|Lada|Lamborghini|Lancia|Land Rover|Lexus|Lincoln|Lotus|Mack|MAN|Marcos|Maserati|Maybach|Mazda|Mercedes|Mercury|MG|Mini|Morgan|Nissan|Nobel|Oldsmobile|Opel|Peterbilt|Peugeot|Plymouth|Pontiac|Porsche|Renault|Rolls-Royce|Rover|SAAB|Scania|SEAT|Skoda|Smart|Spyker|Ssangyong|Subaru|Suzuki|Talbot|Toyota|TVR|Volkswagen|Volvo|Westfield|White|Zimmer|Regressie 14-09|Regressie 23-10-2020)]  Automerk
                │       ├── [179511]  Speelfiguurvoertuig
                │       └── [179512]  Speelgoedtrein
                ├── [1796]  Speelgoed Modelbouw (elektrisch)
                │   ├── Attributes
                │   │   ├── [Geschikt voor binnen, REQUIRED, LIST (Ja|Nee)]  Geschikt voor binnen
                │   │   ├── [Geschikt voor buiten, REQUIRED, LIST (Ja|Nee)]  Geschikt voor buiten
                │   │   ├── [Max. afstand afstandsbediening in Meters, REQUIRED, INTEGER]  Max. afstand afstandsbediening in Meters
                │   │   ├── [max snelheid in km, REQUIRED, INTEGER]  max snelheid in km
                │   │   └── [Type aandrijving, OPTIONAL, LIST (Elektrisch|Benzine|Nitro)]  Type aandrijving
                │   └── Children
                │       ├── [179600]  RC auto
                │       ├── [179601]  RC boot
                │       ├── [179602]  RC helikopter
                │       ├── [179603]  RC vliegtuig
                │       ├── [179604]  RC tank
                │       └── [179605]  RC vrachtwagen
                └── [1797]  Speelgoed Modelbouw (niet-elektrisch)
                    ├── Attributes
                    │   ├── [Inclusief lijm, REQUIRED, LIST (Ja|Nee)]  Inclusief lijm
                    │   ├── [Inclusief verf, REQUIRED, LIST (Ja|Nee)]  Inclusief verf
                    │   └── [Thema bouwpakket, REQUIRED, LIST (Militaire voertuigen|Vliegtuigen|Auto's|Gebouwen|Schepen|Helikopters|Sciencefiction|Diorama|Ruimtevaart|Dieren|Vrachtwagens|Motoren|Treinen|Hijskranen)]  Thema bouwpakket
                    └── Children
                        └── [179700]  Bouwpakket

```